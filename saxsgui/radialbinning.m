function [phi,radialave,radialaveerr,radialaveerr2]=radialbinning(DATA,qmin,qmax,phipoint,qtype,phimin,phimax,ycen,xcen,ycalib,xcalib,wavelength)
persistent ycenold xcenold ycalibold xcalibold wavelengthold sizedataold Q PHI

%this routine calculates the azimuthally averaged data  (I vs. phi) given a 2D image and
%some boundary values.
% it returns 
% q the x-array into which the data has been binned
% radialave the averaged data
% radialaveerr  the relative error on the average (assuming counting
% statistics of gas-detectors)
% radialaverr2  the relative erron on the average where each pixel value is
% used as measurement and the error is thus taking on these values...this
% is usefull for CCD's, IP's. This second approach breaks down for images with lot's 
% of 0, 1, and 2 ..in short for Poisson statistics 

%no need to recreate rphi if it's already been created for the same
%parameters
% This routine has issues with discretization close to the beamstop and is 
% therefore not used in SAXSGUI
wavelengthtemp=wavelength;
if  isempty(ycenold) 
    [Q,PHI]=create_qphi_gen(DATA,ycen,xcen,ycalib,xcalib,wavelength) ; %creates 2 matrices having the q-value and phi-values for each pixel
else
 if isempty(wavelength), wavelengthtemp=0; end
 if ~(ycenold==ycen && xcenold==xcen && ycalibold==ycalib && xcalibold==xcalib && wavelengthold==wavelength && isequal(sizedataold,size(DATA),1))
        [Q,PHI]=create_qphi_gen(DATA,ycen,xcen,ycalib,xcalib,wavelength) ; %creates 2 matrices having the q-value and phi-values for each pixel
 end
end

%default is linear binning
phistep=(phimax-phimin)/(phipoint-1);
phi=[phimin+0.5*phistep:phistep:phimax+0.5*phistep];
DATAbin=floor(((PHI-phimin)/phistep)+1).*(Q>=qmin&Q<=qmax);

sumcount=zeros(phipoint,1);
sumnumpixels=zeros(phipoint,1);
sumsqcount=zeros(phipoint,1);
datasize=size(DATA);
tic
for jj=1:datasize(2)
    for ii=1:datasize(1)
        if ~isnan(DATAbin(ii,jj)) && ~isinf(DATAbin(ii,jj)) && DATAbin(ii,jj)>0 && DATAbin(ii,jj)<=phipoint
            sumcount(DATAbin(ii,jj))=sumcount(DATAbin(ii,jj))+DATA(ii,jj);
            sumnumpixels(DATAbin(ii,jj))=sumnumpixels(DATAbin(ii,jj))+1;
            sumsqcount(DATAbin(ii,jj))=sumsqcount(DATAbin(ii,jj))+DATA(ii,jj)^2;
        end
    end
end
toc
warning('off','MATLAB:divideByZero')
radialave=sumcount./sumnumpixels;
radialaveerr=sqrt(sumcount)./sumcount; % this is a relative error
radialaveerr2=sqrt((sumsqcount-sumnumpixels.*radialave.^2)./(sumnumpixels-1))./radialave;
warning('on','MATLAB:divideByZero')

%updating old for next time the function is called
ycenold=ycen;
xcenold=xcen;
ycalibold=ycalib;
xcalibold=xcalib;
wavelengthold=wavelengthtemp;
sizedataold=size(DATA);
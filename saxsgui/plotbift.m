function plotbift(datain,Full)
%   PLOTBIFT creates a plotwindow for plotting and starts
%   Bayesian IFT process 
%   PLOTbift(data,Full)
%   H = PLOTX(data,Full)
%
%   

biftfig = figure;  % always in a new window
%hplot = plot(S, varargin{:});  % using SAXS/PLOT
%haxes = get(hplot, 'Parent');  % remember the axes object


refinemenu = uimenu('Label', 'Refine');
changeparametersmenu=uimenu(refinemenu, 'Label', 'Redo IFT', 'Callback', ...
    {@ChangeParam, biftfig,datain,Full});
advancedparametersmenu=uimenu(refinemenu, 'Label', 'Advanced', 'Visible','Off','Callback', ...
    {@AdvancedParam, biftfig});

outputmenu = uimenu('Label', 'Output');
figmenu=uimenu(outputmenu, 'Label', 'As Matlab figure', 'Callback', ...
    {@SaveFig, biftfig});
mfilemenu=uimenu(outputmenu, 'Label', 'As M-file', 'Callback', ...
    {@SaveMfile, biftfig});
mfilemenu=uimenu(outputmenu, 'Label', 'As Ascii-file', 'Callback', ...
    {@SaveAscii, biftfig});

datain(:,3)=datain(:,3);
xtemp=datain(:,1)';
ytemp=datain(:,2)';
sdtemp=datain(:,3)';

errorbar(xtemp,ytemp,sdtemp)
title('Data with errorbars')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Intensity a.u.')
if max(ytemp)/min(ytemp)>100 
    set(gca,'Yscale','Log')
end

results=biftcalc(datain,Full,0,biftfig); %calls Bayesian IFT with full error analysis and user interaction
h=figstatus('Done with calculations. Now plotting');
biftfig=plotbiftresults(results,biftfig);
set(biftfig,'Name','Bayesian IFT results');
delete(h)

if nargout
    hfig_out = hfig;
end

%*************************************
function ChangeParam(hmenu, eventdata, biftfig,datain,Full)
% Menu callback.  Allow another calculation...at a later time we may make
% this more user friendly but for now it simply allows a recalculation

plotbift(datain,Full);

%*************************************
function alphaChangeParam(hmenu, eventdata, biftfig)
% Menu callback.  Guide user through exporting data as text.

% Select line.
hline = whichline(haxes);
if isempty(hline)
    return
end
% Name file.
[file dir] = uiputfile('*.csv', 'Export to text file');
% Write file.
if file
    csvwriteline(hline, [dir file])
end
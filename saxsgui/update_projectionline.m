function update_projectionline
global SaxsguiWindowName
obj=findobj(get(0,'children'), 'Name', SaxsguiWindowName);
linehandle=findobj(obj,'Type','line');
linehandle=linehandle(1);

figurehandle=findobj(get(0,'children'), 'Name', 'Projection Line Graph');
if isempty(figurehandle);  % find existing figures with
    figurehandle=figure;
    set(figurehandle,'Name','Projection Line Graph')
    plot(1,1);
end
projectionlinehandle=findobj(figurehandle,'Type','axes');
gui=guidata(obj);
x=get(linehandle,'Xdata');
y=get(linehandle,'Ydata');
img=gui.img.xy;
a=bresenham(img,(pixelfrac(img,[x ; y]))',0);
plot(projectionlinehandle,a,'r');



%An idea for later could be to calculate the line profile - using multiple
%Bresenhams
function hline_out = circle(radius, varargin)
%CIRCLE Draw a circle.
%   CIRCLE(R) draws a circle of radius R, centered at the axes origin.
%
%   CIRCLE(R, ...) accepts property name/value pairs in the same way as
%   the LINE command.
%
%   H = CIRCLE(R, ...) returns the handle to the line object that CIRCLE
%   creates.
%
%   See also:  LINE.

if nargin == 0
    error('Supply a radius for the circle.')
end

if ~ (isnumeric(radius) && length(radius) == 1)
    error('Radius must be a scalar number.')
end

circx = [-radius : radius / 50 : radius];
circy = sqrt(radius ^ 2 - circx .^ 2);

circx = [circx, fliplr(circx)];
circy = [circy, fliplr(-circy)];

hline = line(circx, circy, varargin{:});

if nargout
    hline_out = hline;
end

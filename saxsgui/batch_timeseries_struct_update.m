function    batch_timeseries_struct_update(handles)
global batch_timeseries_temp
% this routine updates the structure based on the changes that have gone on in 
% in the GUI. 
%
% hObject is the handle for where the changes are taking place
%
% this routine only changes the structure  it does not change GUI 

rdt=batch_timeseries_temp; %reduc_par_temp was too long to write

rdt.UseIncrementalTime=get(handles.UseIncrementalTime,'Value');
rdt.UseFiledSavedTime=get(handles.UseFiledSavedTime,'Value');
rdt.UseTimeFromHeader=get(handles.UseTimeFromHeader,'Value');

rdt.UseMetaData=get(handles.UseMetaData,'Value');

rdt.sample_trans=str2num(get(handles.sample_trans,'String'));
if isempty(rdt.sample_trans), rdt.sample_trans=1; end
if rdt.sample_trans<=0, warndlg('Negative Transmission is garbage', 'Reduction Parameter Warning'); end
if rdt.sample_trans>1, warndlg('Transmission > 1 is non-physical', 'Reduction Parameter Warning'); end

rdt.Sample_thickness=str2num(get(handles.Sample_thickness,'String'));
if isempty(rdt.Sample_thickness), rdt.Sample_thickness=1; end
if rdt.Sample_thickness<=0, warndlg('Negative Sample Thickness is garbage', 'Reduction Parameter Warning'); end


rdt.EvalExpr1=get(handles.EvalExpr1,'Value');
rdt.EvalExpr1String=get(handles.EvalExpr1String,'String');
rdt.EvalExpr2=get(handles.EvalExpr2,'Value');
rdt.EvalExpr2String=get(handles.EvalExpr2String,'String');
rdt.EvalExpr3=get(handles.EvalExpr3,'Value');
rdt.EvalExpr3String=get(handles.EvalExpr3String,'String');
rdt.EvalExpr4=get(handles.EvalExpr4,'Value');
rdt.EvalExpr4String=get(handles.EvalExpr4String,'String');
rdt.EvalExpr5=get(handles.EvalExpr5,'Value');
rdt.EvalExpr5String=get(handles.EvalExpr5String,'String');


rdt.Save2DImages=get(handles.Save2DImages,'Value');
rdt.Save1DFigures=get(handles.Save1DFigures,'Value');

rdt.ReducedDataToSeparateFiles=get(handles.ReducedDataToSeparateFiles,'Value');
rdt.ReducedDataToSameFile=get(handles.ReducedDataToSameFile,'Value');
rdt.xyformat=get(handles.xyformat,'Value');
rdt.xydeltayformat=get(handles.xydeltayformat,'Value');
FileFormatIndex=get(handles.FileFormat,'Value');
FileFormatList=get(handles.FileFormat,'String');
rdt.FileFormat=FileFormatList{FileFormatIndex};
if strcmp(rdt.FileFormat,'RAD') || strcmp(rdt.FileFormat,'PHD')
    rdt.xyformat=0;
    rdt.xydeltayformat=1;
end

rdt.DescriptiveTagString=get(handles.DescriptiveTagString,'String');


batch_timeseries_temp=rdt;
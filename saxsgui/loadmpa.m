function img = loadmpa(filename)
%LOADMPA Load x-ray diffraction image data from an MPA file.
%   The MPA file format is from FAST ComTec, maker of the
%   multi-parameter, multi-channel analyzer called the MPA3.
%
%   IMG = LOADMPA('filename') loads image array data from an 
%   MPA file and assigns it to variable IMG.  LOADMPA reports
%   the number of data points and array dimensions in the
%   command window.

% Try to open the file for reading.
file = fopen(filename, 'rt');
if file == -1
    error('Could not open file.')
end

% Extract array x dimension.

line = findline(file, '[ADC1]');
if isempty(line)
    fclose(file);
    error('MPA file error - no [ADC1] tag.')
end

line = findline(file, 'range=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no range= after [ADC1]')
end

xdim = str2double(line(length('range=')+1:end));

% Extract array y dimension.

line = findline(file, '[ADC2]');
if isempty(line)
    fclose(file);
    error('MPA file error - no [ADC2] tag.')
end

line = findline(file, 'range=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no range= after [ADC2]')
end

ydim = str2double(line(length('range=')+1:end));

% Now find the array

line = findline(file, '[CDAT0,');
if isempty(line)
    fclose(file);
    error('MPA file error - no [CDAT0, tag.')
end

% Read data points till there ain't no more.
data = fscanf(file, '%d');
fclose(file);

fprintf(1, 'Read %d data points, %d x %d.\n', length(data), xdim, ydim);
if length(data) ~= xdim * ydim
    warning('Number of data points do not match dimensions.')
end

% Reshape to a square array; get x and y right.
img = reshape(data, xdim, ydim)';


% ---------------------------------------------------------
function line = findline(file, findstr, varargin)
%FINDLINE Look for line in file starting with string.
%
%  line = FINDLINE(file, findstr)
%    Look forward in file (handle) for a line starting with 
%    the characters in findstr.
%    If not found before end of file, return an empty array.
%    Otherwise return the line found as a character array.
%
%  line = FINDLINE(file, findstr, stopstr)
%    Same but stop if we find a line starting with stopstr,
%    returning an empty array.  An enhancement
%    would be to begin the next search at that line.  But
%    for now a subsequent search would start at the line after
%    the stop line.  So far, we're going to bail out if one
%    of our FINDLINE's fails, so we don't need subsequent 
%    FINDLINE's.

lenfind = length(findstr);
stopstr = [];
lenstop = 0;
if nargin == 3
    stopstr = varargin{1};
    lenstop = length(stopstr);
end

while ~ feof(file)
    line = fgetl(file);
    if strncmp(line, findstr, lenfind)
        break
    end
    if strncmp(line, stopstr, lenstop)  % returns false if lenstop is 0
        break
    end
end

if ~ strncmp(line, findstr, lenfind)
    line = [];  % string not found
end
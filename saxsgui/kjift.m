%building up the IFT of Glatter
% the KDJ basis function based on a gaussian with very low values at 0 and
% Dmax. Area of each basis function is 1

NBasis=40;
Rmax=200;
GaussWidth=10*10*NBasis/Rmax;
Nx=100;
xB=Rmax/NBasis:Rmax/NBasis:Rmax-Rmax/NBasis;
xR=Rmax/Nx:Rmax/Nx:Rmax;
k=Rmax/GaussWidth;
%sigma=k*(1-abs(1-(xB*2)/Rmax));
sigma=k+0*xB;
B=ones(Nx,NBasis-1);
for ii=1:NBasis-1
    B(:,ii)=(normfun(xR,xB(ii),sigma(ii))/sigma(ii))';
end

%so no C is the basis matrix containing all the basis vectors 
% and B x wf = p(r)

%now making a first weighting function and the firste pr
wf=normfun(xB,Rmax/2,Rmax/6)';
%wf=ones(NBasis-1,1);
pr=B*wf;

%now for q
qmin=1e-3;
qmax=0.2;
Nq=201;
q=qmin:(qmax-qmin)/(Nq-2):qmax;
I=q*0;
for ii=1:max(size(q))
    qi=q(ii);
    I(ii)=4*pi*((sin(qi*xR)./(qi*xR))*pr);
end
figure(1)
plot(wf)
figure(2)
plot(xR,pr)
figure(3)
plot(q,I)

function saxsobj=dtrekimgread(pathname,silent)

fileroot=pathname(1:max(find(pathname=='.')-1));

%read in data from the fuji inf fil
try  % reading image-header from the inf-file
    fid=fopen([fileroot,'.img'],'r');
    s=fread(fid,'uchar');
    fclose(fid);
    
    %search for header length
    headerlengthpos=strfind(char(s'),'HEADER_BYTES');
    size1pos=strfind(char(s'),'SIZE1');
    size2pos=strfind(char(s'),'SIZE2');
    filedatepos=strfind(char(s'),'DTREK_DATE_TIME');


    headerlength=sscanf(char(s(headerlengthpos:end))','HEADER_BYTES=%i'); 
    size1=sscanf(char(s(size1pos:end)'),'SIZE1=%i');
    size2=sscanf(char(s(size2pos:end)'),'SIZE2=%i');
    filedate=sscanf(char(s(size2pos:end)'),'SIZE2=%s');
catch
    caught = lasterr;  % error message
    error(['Error for dTrek data', pathname, ':  ', caught])
end
%reopen the file
fid=fopen([fileroot,'.img'],'r');
%read the header
s=fread(fid,headerlength,'uchar');
%now read actual data
imray = rot90(fread(fid,[size1,size2],'int32'),-1);
im=imray';
fclose(fid);

saxsobj = saxs(im, ['imgread file ''', pathname, ''' ', datestr(now)]);
if ~silent
    %input exposure time in dialogbox
    prompt={'Enter the normalization factor (time or monitor counter) for the img-file (in seconds):'};
    def={'900'};
    dlgTitle='Input for Exposure Time';
    lineNo=1;
    answer=inputdlg(prompt,dlgTitle,lineNo,def);

    livetime1=str2num(char(answer));
    if isempty(livetime1)
        livetime1=1;
    end
else
    livetime1=1;
end
saxsobj = type(saxsobj, 'xy');
saxsobj = realtime(saxsobj, max(0,livetime1));
saxsobj = livetime(saxsobj, max(0,livetime1));
saxsobj = raw_filename(saxsobj, pathname);
saxsobj = raw_date(saxsobj,filedate);
saxsobj = datatype(saxsobj,'img');
saxsobj = detectortype(saxsobj,'d*Trek');

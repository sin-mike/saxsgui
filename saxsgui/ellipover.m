function ellipover(eta, xc, yc, r, rstep, thetastep, linestyle)
%ELLIPOVER Overlay an image with an elliptical grid.
%   ELLIPOVER(ETA, XC, YC, R, RSTEP, THETASTEP, LINESTYLE) overlays the current axes with an
%   elliptical coordinates grid based on the image center
%   XC,YC and the apparent ellipticity ETA.  The grid extends
%   out from the center to a minimum radius R.  Note ELLIPOVER
%   assumes the elliptical coordinates are not rotated with
%   respect to the current axes.
%
%   RSTEP and THETASTEP control the spacing of elliptical grid lines
%   along the radius and angle of rotation, in pixels and degrees,
%   respectively.  Defaults are R/10 pixels and 10 degrees.
%
%   LINESTYLE is a MATLAB line style defaulting to '-y'.

if nargin < 7
    linestyle = '-y';
end
if nargin < 6
    thetastep = 10;
end
if nargin < 4
    r = 400;
end
if nargin < 5
    rstep = 20;
end
if nargin < 2
    xc = 0;
    yc = 0;
end

hold on
[X Y] = meshgrid(1:rstep:r, [0:thetastep:360]*2*pi/360);
[XU YU] = ellip2cart2(X, Y, eta);
XU = XU + xc;
YU = YU + yc;
plot(XU, YU, linestyle)
plot(XU', YU', linestyle)
hold off
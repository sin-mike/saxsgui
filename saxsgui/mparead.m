function s = mparead(pathname)
%MPAREAD Read an image from an MPA file, returning a saxs image object.
%   SAXS_OBJECT = MPAREAD('pathname'); reads the file 'pathname' and attempts
%   to load a SAXS image, returning a saxs image object.
%
%   See also SAXS/SAXS, GETSAXS, SAXS/DISPLAY.
% Feb 25 ,2006 added option for reading binary MPA files

% Try to open the file for reading.
file = fopen(pathname, 'rt');
if file == -1
    error('Could not open file.')
end

% find out whether it data is asc or binary
line = findline(file, 'mpafmt');
if isempty(line)
    fclose(file);
    error('MPA file error - no [ADC1] tag.')
end
mpafmt = line(length('mpafmt=')+1:end);

% Extract array x dimension.

line = findline(file, '[ADC1]');
if isempty(line)
    fclose(file);
    error('MPA file error - no [ADC1] tag.')
end

line = findline(file, 'range=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no range= after [ADC1]')
end

xdim = str2double(line(length('range=')+1:end));

% Extract real time.

line = findline(file, 'realtime=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no realtime= after [ADC1]')
end

realtime1 = str2double(line(length('realtime=')+1:end));

% Extract live time.

line = findline(file, 'livetime=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no livetime= after [ADC1]')
end

livetime1 = str2double(line(length('livetime=')+1:end));

% Extract starting date from header

line = findline(file, 'cmline0=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no date= after [ADC1]')
end

filedate = (line(length('cmline0=')+1:end));

% Extract Transmission Monitor Value (only for PhotoDiode) from header

% line = findline(file, 'cmline9=', '[');
% if isempty(line)
%     fclose(file);
%     error('MPA file error - no PhotoDiode= after [ADC1]')
% end
% try
%     transmon = abs(str2num((line(length('cmline9=PhotoDiode=')+1:end))));
% catch
%     transmon=[];
% end

% Extract array y dimension.

line = findline(file, '[ADC2]');
if isempty(line)
    fclose(file);
    error('MPA file error - no [ADC2] tag.')
end

line = findline(file, 'range=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no range= after [ADC2]')
end

ydim = str2double(line(length('range=')+1:end));

% Extract live time.

line = findline(file, 'livetime=', '[');
if isempty(line)
    fclose(file);
    error('MPA file error - no livetime= after [ADC2]')
end

livetime2 = str2double(line(length('livetime=')+1:end));

if strcmp(mpafmt,'asc')% Now find the array
    line = findline(file, '[CDAT');
    if isempty(line)
        fclose(file);
        error('MPA file error - no [CDAT, tag.')
    end
    % Read data points till there ain't no more.
    data = fscanf(file, '%d');
else
   fclose(file);
   file=fopen(pathname,'r');
   databin=fread(file);
   ii=strfind(char(databin)','[CDAT0,');
   iii=strfind(char(databin(ii:end)'),']');
   a=databin(ii+iii(1)+2:ii+iii(1)+2+4*xdim*ydim-1);
  
   b=reshape(a,4,length(a)/4);
   data=b(4,:)*256^3+b(3,:)*256^2+b(2,:)*256+b(1,:);
end
fclose('all');
if length(data) ~= xdim * ydim
    warning('Number of data points do not match dimensions.')
end

% Reshape to a square array; get x and y right.
pixels = reshape(data, xdim, ydim)';
if strcmp(mpafmt,'dat')
    pixels(ydim,:)=pixels(xdim,:)*0;
    pixels(:,xdim)=pixels(:,ydim)*0;
end

%Some mpa detectors have spurrious counts recorded in the (1,:) and (:,1).
%Since these are never desired we replace this data with 0's

pixels(:,1)=pixels(:,1)*0;
pixels(1,:)=pixels(1,:)*0;
pixels(:,end)=pixels(:,end)*0;
pixels(end,:)=pixels(end,:)*0;

    
% Return a saxs object.
s = saxs(pixels, ['mparead file ''', pathname, ''' ', datestr(now)]);
s = type(s, 'xy');
s = realtime(s, realtime1);
s = livetime(s, min(livetime1, livetime2));
s = datatype(s,'mpa');
s = detectortype(s,'MolMet');
s = raw_filename(s, pathname);
s = raw_date(s,filedate);
s;

% ---------------------------------------------------------
function line = findline(file, findstr, varargin)
%FINDLINE Look for line in file starting with string.
%  line = FINDLINE(file, findstr)
%    Look forward in file (handle) for a line starting with 
%    the characters in findstr.
%    If not found before end of file, return an empty array.
%    Otherwise return the line found as a character array.
%
%  line = FINDLINE(file, findstr, stopstr)
%    Same but stop if we find a line starting with stopstr,
%    returning an empty array.  An enhancement
%    would be to begin the next search at that line.  But
%    for now a subsequent search would start at the line after
%    the stop line.  So far, we're going to bail out if one
%    of our FINDLINE's fails, so we don't need subsequent 
%    FINDLINE's.

lenfind = length(findstr);
stopstr = [];
lenstop = 0;
if nargin == 3
    stopstr = varargin{1};
    lenstop = length(stopstr);
end

while ~ feof(file)
    line = fgetl(file);
    if strncmp(line, findstr, lenfind)
        break
    end
    if strncmp(line, stopstr, lenstop)  % returns false if lenstop is 0
        break
    end
end

if ~ strncmp(line, findstr, lenfind)
    line = [];  % string not found
end

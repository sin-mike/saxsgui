function filename=find_filename(s)

%this program assumes a standard SAXSGUI structure with a "mpa read" in the
%first part of the history
try 
b=findstr('''',s.history(1,:));
filename=s.history(1,b(1)+1:b(2)-1);
catch
    filename='No filename';
end
function [got, gotname, fromdir] = getsaxs(specifiedpath, title)
%GETSAXS Get SAXS image from file.
%   GETSAXS displays a dialog window to open a SAXS image file.  If
%   GETSAXS reads a SAXS image from the file, it returns the SAXS image
%   object.  Otherwise it returns an empty array.
%
%   GETSAXS can read SAXS images from MPA detector files (see MPAREAD), MAT
%   files saved from MATLAB (see SAVE or SAXSGUI), and gray-scale TIFF
%   files.  If a MAT file contains more than one SAXS object, GETSAXS
%   displays a second dialog window to select an image.  [Or it might
%   someday.  Right now it just takes the first SAXS image it finds.]
%
%   GETSAXS('directory') starts in the specified directory.
%
%   GETSAXS('directory', 'title') uses the specified title for the dialog
%   window.  Default is 'Open SAXS image file'.  To specify a title but let
%   GETSAXS use the last directory it worked in, specify an empty string
%   for 'directory'.
%
%   GETSAXS('file') where 'file' is not a directory, attempts to read a
%   SAXS image from the file without displaying the usual dialog window.
%
%   [S NAME] = GETSAXS(...) returns saxs object S and the file name NAME,
%   sans directory path.
%
%   [S NAME DIR] = GETSAXS(...) returns saxs object S, file name NAME, and
%   directory path DIR.
%
%   See also MPAREAD, SAXS, SAXS/IMAGE, LOAD, SAVE.

persistent rememberdir  % remember where we've been
file = [];  % no file specified yet

if nargin > 0 && ~ isempty(specifiedpath)
    % We have either a directory to start in or a file to open.
    if isdir(specifiedpath)
        rememberdir = fullpath(specifiedpath);
    else
        [dir file ext] = fileparts(specifiedpath);
        file = [file, ext];
        if isempty(dir)
            dir = '.';
        end
        dir = fullpath(dir);
        clear ext 
    end
end
if nargin > 1 && ischar(title)
    dlgtitle = title;
else
    dlgtitle = 'Open SAX image file';
end
if isempty(rememberdir)
    rememberdir = '';  % avoid warning from FULLFILE
end
got = [];  % we haven't read a SAXS object yet

if isempty(file)
        [file dir] = uigetfile([fullfile(rememberdir, '*.mpa'), ...
        ';*.mat;*.tif;*.tiff;*.img;*.inf;*.gfrm;*.unw;*.cmb;*.edf;*.lst'], dlgtitle);
end

if file  % we have a file path
    path = fullfile(dir, file);
    rememberdir = dir;  % look in this directory first next time
    switch filetype(file)
        case 'mpa'
            got = trympa(path);
        case 'mat'
            got = trymat(path);
        case 'tiff'
            got = trytiff(path);
        case 'tif'
            got = trytiff(path);
        case ('img')
            got = tryimg(path);
        case 'bruker'
            got = trybruker(path);
        case 'cmb'
            got = trycmb(path);
        case 'edf'
            got = tryedf(path);
        case 'lst'
            got = trympalst(path);
        otherwise
            errordlg('I can only read MPA,MAT,TIFF,IMG,GFRM,CMB,EDF, and LST files.', ...
                'Wrong file type', 'modal')
            got = [];
    end
end

% Take care of optional output arguments.
if isempty(got)  % we didn't read a SAXS object
    if nargout > 1
        gotname = [];
        if nargout > 2
            fromdir = [];
        end
    end
else  % we did read a SAXS object
    if nargout > 1
        gotname = file;
        if nargout > 2
            fromdir = dir;
        end
    end
    % let's just put a comment in the history about how many counts
    totalint=num2str(sum(sum(got.raw_pixels)));
    got=history(got,['Total Intensity ',totalint]);
end
fclose('all'); % this is to fix a strange fopen leak that appeared after 
              %saxsgui version 2.0.4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trympa(path)
% Try reading an MPA file.
% Return a SAXS object or report an error and return empty.
try
    S = mparead(path);
catch
    caught = lasterr;
    errordlg(caught, 'Error reading MPA file', 'modal')
    S = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trympalst(path)
global liststruct
% Try reading an MPA LST filefile.
% Return a SAXS object or report an error and return empty.
try
    liststruct=mpalistread(path);
    S = list2saxs(liststruct);
catch
    caught = lasterr;
    errordlg(caught, 'Error reading MPA lst-file', 'modal')
    S = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = tryedf(path)
% Try reading an EDF file.
% Return a SAXS object or report an error and return empty.
try
    S = edfread(path);
catch
    caught = lasterr;
    errordlg(caught, 'Error reading EDF file', 'modal')
    S = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trymat(path)
% Try reading a MAT file.
% Return a SAXS object or report an error and return empty.
% A MAT file may contain 0, 1, or more SAXS objects.

% A SAXS object may be in an old form, not matching the current SAXS field
% structure.  In that case MATLAB issues a warning that the fields do not
% match and returns a structure with the existing fields from the object.
% We can use the SAXS function on the structure to update it to a current
% SAXS object.

S = [];  % return empty by default

% We shall be brave and load the entire MAT file.  This could take up an
% arbitrary amount of space and time.  Ignore warnings to avoid the MATLAB
% warning when old SAXS objects are converted to structures.  (That warning
% has no message ID so we can't suppress it individually.)

restore_warnings = warning('query', 'all');  % save warning state
warning('off', 'all')  % disable all warnings
try
    loaded = load('-mat', path);
catch
    warning(restore_warnings)
    errordlg(lasterr, 'Error reading MAT file', 'modal')
    return
end
warning(restore_warnings)
% loaded is a structure containing variables read from the file.

% Take the first SAXS variable we find.  Someday, select one from a list.
list = {};
vars = struct2cell(loaded);
for ivar = 1 : length(vars)
    var = vars{ivar};
    if isa(var, 'saxs')
        list = horzcat(list, {var});
    end
end

% If no SAXS variables, look for converted structures (old SAXS variables).
% The vars variable is still loaded.
if length(list) == 0
    for ivar = 1 : length(vars)
        var = vars{ivar};
        if isstruct(var)
            if isfield(var, 'pixels')  % if pixels here, assume it's an old SAXS
                list = horzcat(list, {saxs(var)});
            end
        end
    end    
end

switch length(list)
    case 0
        errordlg('There are no SAXS variables in that file.', ...
            'No SAXS variables', 'modal')
        return
    otherwise
        S = list{1};
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trytiff(path)
% Return a SAXS object or report an error and return empty.
global multiple_file_run
try
    S = tiffread(path,multiple_file_run); %1 means silent
catch
    caught = lasterr;
    errordlg(lasterr, 'Error reading TIFF file', 'modal')
    S = [];
    return
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = tryimg(path)
% This looks at img files....there are presently 3 versions supported.
% 1) A fuji img file (which requires an .inf file as well with meta data)
% 2) An Rigaku Raxia file (which will be recognized by the fact the file
% starts with an R)
% 3) An Rigaku dTrek file (which will be recognized by the fact the file
% starts with an R)
% Returns a SAXS object or report an error and return empty.
global multiple_file_run

S=[];
fileroot=path(1:max(find(path=='.')-1));
imgfuji=0;
imgraxia=0;
imgdtrek=0;
%try to decide which filetype it is
% first we try to open the fuji .inf-file
fid=fopen([fileroot,'.inf'],'r');
if fid>0
    imgfuji=1;
    fclose(fid)
end
if imgfuji==0
    fid=fopen(path,'r');
    firstcharacter=fread(fid,1,'char');
    if strcmp(char(firstcharacter),'R')
        imgraxia=1;
    end
    if strcmp(char(firstcharacter),'{')
        imgdtrek=1;
    end
    fclose(fid)
end

if imgfuji
    try
        S = fujiimgread(path,multiple_file_run);
    catch
        caught = lasterr;
        errordlg(lasterr, 'Error reading Fuji img file', 'modal')
        S = [];
        return
    end
end
if imgraxia
    try
        S = raxiaimgread(path,multiple_file_run);
    catch
        caught = lasterr;
        errordlg(lasterr, 'Error reading RAXIA img file', 'modal')
        S = [];
        return
    end
end
if imgdtrek
    try
        S = dtrekimgread(path,multiple_file_run);
    catch
        caught = lasterr;
        errordlg(lasterr, 'Error reading d*Trek img file', 'modal')
        S = [];
        return
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trybruker(path)
% Try reading an Bruker GFRM file.
% Return a SAXS object or report an error and return empty.
try
    S = brukerread(path);
catch
    caught = lasterr;
    errordlg(caught, 'Error reading Bruker File', 'modal')
    S = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trycmb(path)
% Try reading an hecus CMB file.
% Return a SAXS object or report an error and return empty.
global multiple_file_run
try
    S = cmbread(path,multiple_file_run);
catch
    caught = lasterr;
    errordlg(caught, 'Error reading CMB file', 'modal')
    S = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function type = filetype(path)
% Return file type string, or 0 if we don't recognize the file.
if regexpi(path, '\.mpa$')
    type = 'mpa';
elseif regexpi(path, '\.mat$')
    type = 'mat';
elseif ~ isempty(regexpi(path, '\.tiff$'))
    type = 'tiff';
elseif ~ isempty(regexpi(path, '\.tif$'))
    type = 'tif';
elseif ~ isempty(regexpi(path, '\.img$')) || ~ isempty(regexpi(path, '\.inf$'))
    type = 'img';
elseif ~ isempty(regexpi(path, '\.gfrm$')) || ~ isempty(regexpi(path, '\.unw$'))
    type = 'bruker';
elseif ~ isempty(regexpi(path, '\.cmb$'))
    type = 'cmb';
elseif ~ isempty(regexpi(path, '\.lst$'))
    type = 'lst';
elseif ~ isempty(regexpi(path, '\.edf$'))
    type = 'edf';
else
    type = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function    watercalib_par_GUIupdate(handles)
global watercalib_par_temp
% this routine updates the window where Reduction parameters can be entered
% to reflect the changes that are made interactively

% hObject is the handle for the figure window
% handles are all the handles referred to by name f.eks. handle.sample
% watercalib_par_temp is a structure that holds all the relevant values
% 
% this routine only changes the GUI it does not change any underlying
% parameter values in the structure
rdt=watercalib_par_temp; %

if (rdt.sample_check==1)
    set(handles.sample_check,'Value',1)
    set(handles.text9,'Enable','On')
    set(handles.sample_trans,'Enable','On')
else
    set(handles.sample_check,'Value',0)
    set(handles.text9,'Enable','Off')
    set(handles.sample_trans,'Enable','Off')
end
set(handles.sample_trans,'String',num2str(rdt.sample_trans))

if (rdt.mask_check==1)
    set(handles.mask_check,'Value',1)
    set(handles.text12,'Enable','On')
    set(handles.maskname,'Enable','On')
    set(handles.maskfile_get,'Enable','On')
else
    set(handles.mask_check,'Value',0)
    set(handles.text12,'Enable','Off')
    set(handles.maskname,'Enable','Off')
    set(handles.maskfile_get,'Enable','Off')
end
set(handles.maskname,'UserData',rdt.maskname)
[PATHSTR,NAME,EXT] = fileparts(rdt.maskname);
set(handles.maskname,'String',NAME)


if (rdt.flatfield_check==1)
    set(handles.flatfield_check,'Value',1)
    set(handles.text13,'Enable','On')
    set(handles.flatfieldname,'Enable','On')
    set(handles.flatfieldfile_get,'Enable','On')
else
    set(handles.flatfield_check,'Value',0)
    set(handles.text13,'Enable','Off')
    set(handles.flatfieldname,'Enable','Off')
    set(handles.flatfieldfile_get,'Enable','Off')
end
set(handles.flatfieldname,'UserData',rdt.flatfieldname)
[PATHSTR,NAME,EXT] = fileparts(rdt.flatfieldname);
set(handles.flatfieldname,'String',NAME)

if (rdt.empty_check==1)
    set(handles.empty_check,'Value',1)
    set(handles.text17,'Enable','On')
    set(handles.empty_trans,'Enable','On')
    set(handles.text14,'Enable','On')
    set(handles.emptyname,'Enable','On')
    set(handles.emptyfile_get,'Enable','On')
else
    set(handles.empty_check,'Value',0)
    set(handles.text17,'Enable','Off')
    set(handles.empty_trans,'Enable','Off')
    set(handles.text14,'Enable','Off')
    set(handles.emptyname,'Enable','Off')
    set(handles.emptyfile_get,'Enable','Off')
end
set(handles.emptyname,'UserData',rdt.emptyname)
[PATHSTR,NAME,EXT] = fileparts(rdt.emptyname);
set(handles.emptyname,'String',NAME)
set(handles.empty_trans,'String',num2str(rdt.empty_trans))

if (rdt.dark_check==1)
    set(handles.dark_check,'Value',1)
    set(handles.text15,'Enable','On')
    set(handles.darkcurrentname,'Enable','On')
    set(handles.darkcurrentfile_get,'Enable','On')
else
    set(handles.dark_check,'Value',0)
    set(handles.text15,'Enable','Off')
    set(handles.darkcurrentname,'Enable','Off')
    set(handles.darkcurrentfile_get,'Enable','Off')
end
set(handles.darkcurrentname,'UserData',rdt.darkcurrentname)
[PATHSTR,NAME,EXT] = fileparts(rdt.darkcurrentname);
set(handles.darkcurrentname,'String',NAME)

if (rdt.readout_check==1)
    set(handles.readout_check,'Value',1)
    set(handles.text40,'Enable','On')
    set(handles.readoutnoisename,'Enable','On')
    set(handles.readoutnoisefile_get,'Enable','On')
else
    set(handles.readout_check,'Value',0)
    set(handles.text40,'Enable','Off')
    set(handles.readoutnoisename,'Enable','Off')
    set(handles.readoutnoisefile_get,'Enable','Off')
end
set(handles.readoutnoisename,'UserData',rdt.readoutnoisename)
[PATHSTR,NAME,EXT] = fileparts(rdt.readoutnoisename);
set(handles.readoutnoisename,'String',NAME)

if (rdt.zinger_check==1)
    set(handles.zinger_check,'Value',1)
else
    set(handles.zinger_check,'Value',0)
end

for ii=1:5
    eval(['set(handles.description',num2str(ii),',''String'',rdt.saved(ii).description);'])
    if (rdt.saved(ii).saved==0)
        eval(['set(handles.description',num2str(ii),',''Enable'',''Off'');'])
        eval(['set(handles.load_config',num2str(ii),',''Enable'',''Off'');'])
    else
        eval(['set(handles.description',num2str(ii),',''Enable'',''On'');'])
        eval(['set(handles.load_config',num2str(ii),',''Enable'',''On'');'])
    end
end

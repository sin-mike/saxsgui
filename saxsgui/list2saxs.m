function S = list2saxs(liststruct,timestart,timeend)
%function S = list2saxs(liststruct,timestart,timeend)
%LIST2SAXS Create SAXS object from a list structure (from MPA List file) 
% take the photons arriving in the timeinterval time-˝timewindow to
% time-˝timewindow

if (nargin==1)
    time=(liststruct.timestart+liststruct.timeend)/2;
    timewindow=time*2;
else
    timestart=max(liststruct.timestart,timestart);
    timeend=min(liststruct.timeend,timeend);
    time=(timestart+timeend)/2;
    timewindow=(timeend-timestart);
end
time=1000/liststruct.timerreduce;
timewindow=timewindow*1000/liststruct.timerreduce;
%Creating the image
[X,Y]=meshgrid(1:liststruct.xdim,1:liststruct.ydim);
img=0*X;
iistart=find(liststruct.list(1,:)>=(time-timewindow/2),1);
iiend=find((liststruct.list(1,end)-liststruct.list(1,:))<=(liststruct.list(1,end)-(time+timewindow/2)),1);
if isempty(iiend)
    iiend=max(liststruct.list(1,:));
end
for ii=iistart:iiend
    img(liststruct.list(2,ii),liststruct.list(3,ii))=img(liststruct.list(2,ii),liststruct.list(3,ii))+1;
end
S=saxs(img,'Created From MPA listfile');
S=livetime(S,timewindow*liststruct.timerreduce/1000); %converting to seconds
S=realtime(S,timewindow*liststruct.timerreduce/1000); %converting to seconds
S=datatype(S,'lst');
S=detectortype(S,'MolMet');
S = raw_filename(S,liststruct.pathname);
%S = raw_date(S,liststruct.filedate); does not work : no filedate in the
%liststruct.





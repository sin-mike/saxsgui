function remove_figstatus(fig)
%FIGSTATUS remove all status line messages .
%   H = REMOVE_FIGSTATUS removes all the displayed yellow bars at the bottom of the
%   current figure with 'message' inscribed.  REMOVE FIGSTATUS returns the handle
%   H of the figure
%
%   REMOVE FIGSTATUS replaces any newlines in the message with three blank characters,
%   '   '.
%
%
%   H = FIGSTATUS() removes all the yellow bars in all windows 
%   handle is FIG.

if nargin < 1
    statuslines=findobj(0,'Tag','statusline');
    delete(statuslines)
else
    if ~ ishandle(fig) || (~ isequal(get(fig, 'Type'), 'figure'))
        error('1st argument must be a figure handle.')
    end
    statuslines=findobj(fig,'Tag','statusline');
    delete(statuslines)
end
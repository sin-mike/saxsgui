function Icalc=GaussPeakSlope(q,I,varargin)
% Description
%     ICALC=GaussPeakSlope(q,I,varargin)
% This function calculates 1D data for a gaussian peak with a sloped
% background
% Description end
% Number of Parameters: 5
% parameter 1: Centre : Centre of Gaussian 
% parameter 2: Amp : Amplitude of Gaussian
% parameter 3: Width : Width (std. dev) of Gaussian (=FWHM/2.35)
% parameter 4: Backg : Background of Gaussian
% parameter 5: Slope : Slope of the background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% GaussPeakSlope
% This is a simple gaussian function with a sloped background
% It was adapted from the matlab program MFIT where it was
% contributed by M. Zinkin
global fitpar
values=varargin{1};
Centre=values(1);
Amp=values(2);
Width=values(3);
Backg=values(4);
Slope=values(5);


Icalc=Backg+Slope.*(q-Centre)+Amp.*exp(-0.5*((q-Centre)/Width).^2);
%--------------------------------------------------------------------------

function saxsgui_installer()
%an installation function for the SAXSGUI. 
%installerchoice is defined as:
%option 1:'saxsgui base'
%option 2:'saxsgui documentation'
%option 3:'saxsgui tutorials'
%option 4:'BIFT'
%option 5:'maskmaker'
%option 6:'mpalisttool'
%option 7:'sample database'
%option 8:'beta features'


saxs_ver='1_0'; %SET THIS MANUALLY FOR EACH NEW VERSION!

display('Welcome to the SAXSGUI installer');
%determine some initial directories
installerfile=which('saxsgui_installer.m'); % in case it was not in a local directory. must be in the path...
[installerdir,d_a,d_b,d_c]=fileparts(installerfile);
kansas=pwd; %a place to return to
source=installerdir;
toolboxsource=source;

%give the user a choice
choice=menu('What type of installation do you want?','Easy installation','Easy full installation','Installation with choices');
pause(.5);
switch choice
    case 0
        error('Nothing chosen, exiting...')
    case 1
        destination=sprintf('%s%s%s%s%s%s',matlabroot,filesep,'work',filesep,'saxsgui_version_',saxs_ver);
        toolboxdestination=sprintf('%s%s%s',toolboxdir(''),filesep,'saxsgui_tools');
        installerchoice=[1 1 0 1 1 1 1 0];%standard installation set
    case 2
        destination=sprintf('%s%s%s%s%s%s',matlabroot,filesep,'work',filesep,'saxsgui_version_',saxs_ver);
        toolboxdestination=sprintf('%s%s%s',toolboxdir(''),filesep,'saxsgui_tools');
        installerchoice=[1 1 1 1 1 1 1 1];%standard installation set
    case 3
        display('Choose the destination for the SAXSGUI program files');
        destination=uigetdir(matlabroot,'Choose the destination for the SAXSGUI program files');
        display('Choose the destination for the SAXSGUI toolbox files');
        toolboxdestination=uigetdir(toolboxdir(''),'Choose the destination for the SAXSGUI toolbox files');
        choicelist={'saxsgui base','saxsgui documentation','saxsgui tutorials',...
            'BIFT','maskmaker','mpalisttool',...
            'sample database','beta features'};
        [installerchoice_unproc,ok]=listdlg('ListString',choicelist,'InitialValue',[1 2 4 5 6 7],'Name','Choose options','OKString','Accept','CancelString','Quit installer (last chance)');
        %process the installer choices
            for instchoiceind=1:max(size(choicelist))
               if ~isempty(find(installerchoice_unproc==instchoiceind))
                   installerchoice(instchoiceind)=1;
               else
                   installerchoice(instchoiceind)=0;
               end
            end
        if ok==0
            return
        end
        %      docdestination=uigetdir(kansas,'Choose the destination for the SAXSGUI documentation and tutorial files');
end

%additional questions:
%wavelength standard
%spec-related stuff

%prepare the destination
mkdir(destination);
destination=sprintf('%s%s',destination,filesep);
mkdir(toolboxdestination);
toolboxdestination=sprintf('%s%s',toolboxdestination,filesep);

%------------------------------------
%process the options
display('Copying the files');
if installerchoice(1)==1
    copyfile(sprintf('%s%s%s',source,filesep,'saxsgui'),sprintf('%s%s',destination,'saxsgui')); %copy application files
    copyfile(sprintf('%s%s%s',source,filesep,'fminsearchbnd'),sprintf('%s%s',toolboxdestination,'fminsearchbnd')); %copy toolboxes

    if exist(toolboxdestination,'dir')~=1 %do not overwrite existing toolboxes.
        copyfile(sprintf('%s%s%s',toolboxsource,filesep,'saxsgui_ff_1d'),sprintf('%s%s',toolboxdestination,'saxsgui_ff_1d')); %copy toolboxes
        copyfile(sprintf('%s%s%s',toolboxsource,filesep,'saxsgui_ff_2d'),sprintf('%s%s',toolboxdestination,'saxsgui_ff_2d')); %copy toolboxes
    end
end
if installerchoice(2)==1
    copyfile(sprintf('%s%s%s',source,filesep,'saxsgui_documentation'),sprintf('%s%s',destination,'saxsgui_documentation')); %copy application files
end
if installerchoice(3)==1
    copyfile(sprintf('%s%s%s',source,filesep,'tutorial_data'),sprintf('%s%s',destination,'tutorial_data')); %copy application files
end
if installerchoice(4)==1
    copyfile(sprintf('%s%s%s',source,filesep,'BIFT'),sprintf('%s%s',destination,'BIFT')); %copy application files
end
if installerchoice(5)==1
    copyfile(sprintf('%s%s%s',source,filesep,'maskmaker'),sprintf('%s%s',destination,'maskmaker')); %copy application files
end
if installerchoice(6)==1
    copyfile(sprintf('%s%s%s',source,filesep,'mpalisttool'),sprintf('%s%s',destination,'mpalisttool')); %copy application files
end
if installerchoice(7)==1
    copyfile(sprintf('%s%s%s',source,filesep,'sampledb'),sprintf('%s%s',destination,'sampledb')); %copy application files
end
if installerchoice(8)==1
    beta_enable=1;
else
    beta_enable=0;
end

display('Saving the path to the files');
%save the path
addpath(genpath(toolboxdestination));
addpath(genpath(destination));
eflag=savepath;
if eflag==1 %the path cannot be saved
    error('There was an error saving the search path. Consult the author for more help on this');
end

%edit the settings file. Actually, edit a preformatted settigns file, one
%without the two last lines defining the fitfunction positions. These will
%be added here.
display('Editing the settings file');
def_wavelength=1.5418;
wavelength=inputdlg('Enter radiation wavelength in Angstroms','Wavelength Setting',1,{num2str(def_wavelength)});
if isempty(wavelength)
    wavelength=def_wavelength;
end
wavelength=str2num(wavelength{1})%*1E-10; %store in meters.

choice=menu('Use SPEC features?','Use SPEC features','Do not use SPEC features');
switch choice
    case 0
        use_spec='yes';
    case 1
        use_spec='yes';
    case 2
        use_spec='no';
end

[fid_read,readmessage]=fopen(sprintf('%s%s%s%s%s',source,filesep,'saxsgui',filesep,'SAXSGUI_settings.m'),'rt');
[fid_write,writemessage]=fopen(sprintf('%s%s%s%s',destination,'saxsgui',filesep,'SAXSGUI_settings.m'),'wt');
%error checking
if fid_read==-1
    error('The settings file could not be opened for reading, consult your dealer. Errormessage: %s',readerror)
elseif fid_write==-1
    error('The settings file could not be written, please check whether the directory %s is writeable by you. Errormessage: %s',...
        sprintf('%s%s%s',destination,filesep,'saxsgui'),writemessage);
end
    

tline = fgetl(fid_read);
while 1

    if isempty(strfind(tline,'fitfunction_dir_1D='))~=1 %we have found the line with the 1D fitfunctions
        fprintf(fid_write,'%s',sprintf('fitfunction_dir_1D=''%s%s'';\r\n',toolboxdestination,'saxsgui_ff_2d')); %replace with this
    elseif isempty(strfind(tline,'fitfunction_dir_2D='))~=1 %we have found the line with the 2D fitfunctions
        fprintf(fid_write,'%s',sprintf('fitfunction_dir_2D=''%s%s'';\r\n',toolboxdestination,'saxsgui_ff_2d')); %replace with this
    elseif isempty(strfind(tline,'wavelength_default='))~=1 %we have found the line with the wavelength
        fprintf(fid_write,'%s',sprintf('wavelength_default=%d \r\n',wavelength)); %replace with this
    elseif isempty(strfind(tline,'spec_installation='))~=1 %we have found the line with the spec installation
        fprintf(fid_write,'%s',sprintf('spec_installation=''%s'' \r\n',use_spec)); %replace with this %spec is either 'yes' or 'no'
    elseif isempty(strfind(tline,'allow_SAXSGUI_beta='))~=1 %we have found the line with the spec installation
        fprintf(fid_write,'%s',sprintf('allow_SAXSGUI_beta=%i \r\n',beta_enable)); %replace with this %spec is either 'yes' or 'no'
    else %nothing to do
        fprintf(fid_write,'%s \r\n',tline); % do not change anything, copy 1 to 1
    end
    tline = fgetl(fid_read); %read the next line
    if ~ischar(tline), break, end
end

fclose(fid_write);
fclose(fid_read);

display('Done! Enjoy the SAXSGUI!')
cd(kansas); %there is no place like home

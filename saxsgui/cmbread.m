function s = cmbread(pathname,silent)
%CMBREAD Read an image from a CMB file, returning a saxs image object.
%   SAXS_OBJECT = CMBREAD('pathname'); reads the file 'pathname' and attempts
%   to load a SAXS image, returning a saxs image object.
%
% CMB files are file formats used at the Ris� SAXS scattering facility.
% This is supposedly a Hecus format
% Brian Pauw added functionality of reading the image exposure time.
%
%   See also SAXS/SAXS, GETSAXS, SAXS/DISPLAY.
%  I have not yet solved the starting time 


% Try to open the file for reading.
file = fopen(pathname, 'rt');
if file == -1
    error('Could not open file.')
end
% Extract starting date from header

line = findline(file, 'Start:');
if isempty(line)
    fclose(file);
    error('MPA file error - no starting date')
end

filedate = (line(length('Start: ')+1:end));

% Extract array x dimension.
line = findline(file, 'Channels X =');
if isempty(line)
    fclose(file);
    error('MPA file error - no X range')
end
xdim = str2double(line(length('Channels X =')+1:end));

% Extract array y dimension.
line = findline(file, 'Channels Y =');
if isempty(line)
    fclose(file);
    error('MPA file error - no Y range')
end
ydim = str2double(line(length('Channels Y =')+1:end));

line = findline(file, 'No.', '[');
fseek(file,4*xdim*ydim,'cof');
% Extract live time.

startline = findline(file, '#sdt');
startpos=strfind(startline,'Epoch time:')+11;
stopline = findline(file, '#edt');
stoppos=strfind(startline,'Epoch time:')+11;

if isempty(startpos)==0 && isempty(stoppos)==0
    livetime1 = str2num(stopline(stoppos:stoppos+10))-str2num(startline(startpos:startpos+10));
    % sometimes there is a small overshoot of 1 that has to do with the
    % computer clock time... so let's check for that and round off in case of
    % that.
    livetime_roundtest=10*round(livetime1/10);
    if abs(livetime_roundtest-livetime1)==1
        livetime1=livetime_roundtest;
    end

    %if no livetime is found in the footer of the cmb file (for example in case
    %of an old file, ask for the exposure time in dialogbox
else
    if ~silent
        %input exposure time in dialogbox
        prompt={'Enter the normalization factor (time or monitor counter) for the cmb-file (in seconds):'};
        def={'900'};
        dlgTitle='Input for Exposure Time';
        lineNo=1;
        answer=inputdlg(prompt,dlgTitle,lineNo,def);

        livetime1=str2num(char(answer));
        if isempty(livetime1)
            livetime1=1;
        end
    else
        livetime1=1;
    end
end



% now read binary data.
fclose(file);
file=fopen(pathname,'r');
databin=fread(file);

a=databin(513:513+4*xdim*ydim-1);

b=reshape(a,4,length(a)/4);
data=b(4,:)*256^3+b(3,:)*256^2+b(2,:)*256+b(1,:);

   
fclose(file);
if length(data) ~= xdim * ydim
    warning('Number of data points do not match dimensions.')
end

% Reshape to a square array; get x and y right.
pixels = reshape(data, xdim, ydim)';


% Return a saxs object.
s = saxs(pixels, ['cmbread file ''', pathname, ''' ', datestr(now)]);
s = type(s, 'xy');
s = realtime(s, max(0,livetime1));
s = livetime(s, livetime1);
s = datatype(s,'cmb');
s = detectortype(s,'Gabriel');
s = raw_filename(s, pathname);
s = raw_date(s,filedate);

% ---------------------------------------------------------
function line = findline(file, findstr, varargin)
%FINDLINE Look for line in file starting with string.
%  line = FINDLINE(file, findstr)
%    Look forward in file (handle) for a line starting with 
%    the characters in findstr.
%    If not found before end of file, return an empty array.
%    Otherwise return the line found as a character array.
%
%  line = FINDLINE(file, findstr, stopstr)
%    Same but stop if we find a line starting with stopstr,
%    returning an empty array.  An enhancement
%    would be to begin the next search at that line.  But
%    for now a subsequent search would start at the line after
%    the stop line.  So far, we're going to bail out if one
%    of our FINDLINE's fails, so we don't need subsequent 
%    FINDLINE's.

lenfind = length(findstr);
stopstr = [];
lenstop = 0;
if nargin == 3
    stopstr = varargin{1};
    lenstop = length(stopstr);
end

while ~ feof(file)
    line = fgetl(file);
    if strncmp(line, findstr, lenfind)
        break
    end
    if strncmp(line, stopstr, lenstop)  % returns false if lenstop is 0
        break
    end
end

if ~ strncmp(line, findstr, lenfind)
    line = [];  % string not found
end

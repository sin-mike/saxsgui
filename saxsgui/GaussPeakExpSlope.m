function Icalc=GaussPeakExpSlope(q,I,varargin)
% Description
% This function calculates 1D data for a gaussian peak with a sloped
% background (exponential function of q)
% Description end
% Number of Parameters: 5
% parameter 1: Centre : Centre of Gaussian 
% parameter 2: Amp : Amplitude of Gaussian
% parameter 3: Width : Width (std. dev) of Gaussian (FWHM/2.35)
% parameter 4: Backg : Background of Gaussian
% parameter 5: LogSlope : LogSlope of the background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% GaussPeakExpSlope
% This is a simple gaussian function with a sloped background
% The background is defined as an exponential background

global fitpar
values=varargin{1};
Centre=values(1);
Amp=values(2);
Width=values(3);
Backg=values(4);
LogSlope=values(5);


Icalc=(Backg*exp(LogSlope.*(log(q)-log(Centre))))+Amp.*exp(-0.5*((q-Centre)/Width).^2);
%--------------------------------------------------------------------------

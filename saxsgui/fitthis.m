function [gui,Fitparam,qs,phis,I]=fitthis(gui,varargin)

%this function will minimize the error of fit for any function func. for
%use with the SAXSGUI program by Karsten Joensen.
%Written by Brian R. Pauw, October 2006
%the function func should have all parameters contained in a single vector.
% the function should first link all variables cells to guess values.
%input: "function, variable, valuematrix", variable is a cell array of strings 

switch nargin;
    case 1;
        warndlg('no function given!');
        func=inputdlg('please type the function name to be fitted','name',1);
        variables=inputdlg('please type the names of the variables in a cell array format');
    case 2;
        func=varargin(1);
        variables=[];
        initguess=[];
    case 3
        func=varargin(1);
        variables=varargin{2};
        title='initial guesses of function variables';
        %this can not be passed on because of the
%          lack of variable sizing of the "def" variable. def should be a
%          cell array of empty strings of similar shape as the "variables"
%          array.
          argument.Resize='on'; 
          argument.WindowStyle='normal';
          argument.Interpreter='tex';
         def={'180' '50' '100' '0.1' '40'};
        iguess=inputdlg(variables,title,1,def,argument);
        for i=1:size(iguess)
            initguess(i)=str2double(iguess(i));
        end
    case 4
        func=varargin(1);
        variables=varargin{2};
        initguess=varargin(3);
        if size(variables,2)~=size(initguess)
            error('the size of the variables cell array is not similar to the size of the initguess array');
        end
    case 5
        error('Too many input parameters.\n Variables and initial guesses should be matrices or cell arrays.');
end

        
        %     otherwise;
%         func=varargin(1);
%         if nargin>4 && ischar(varargin(4))
%             pair=0;            
%         end
%         if nargin<4 || pair==0
%             for i=2:nargin
%                 variables{i}=varargin(i) %the cell array does not get passed on properly to the functions. It needs to be solved.
%             end
%         end
%         display(variables{:})
%         title={'initial guesses of function variables'};
% %         argument.Resize='on';
% %         argument.WindowStyle='normal';
% %         argument.Interpreter='tex';
% %         def{1:size(variables,2)}='';
%         initguess=inputdlg(variables,title,1);
% %     case 4;
% %         func=varargin(1);
% %         variables=varargin(2);
% %         initguess=varargin(3);
% %         if size(variables,2)~=size(initguess)
% %             error('the size of the variables cell array is not similar to the size of the initguess array');
% %         end
% %     case 5;
% %         error('Too many input parameters.\n Variables and initial guesses should be matrices or cell arrays.');


%convert data to angle phi and angle q
[q,phi]=create_qphi_gen(gui.img.xy.pixels,gui.img.xy.raw_center(2),gui.img.xy.raw_center(1),gui.img.xy.kcal/gui.img.xy.pixelcal(2),gui.img.xy.kcal/gui.img.xy.pixelcal(1),gui.img.xy.wavelength);
%the above function does not generate the right values for q, so we're
%going to grab them from the axes :). 
%pixelsize=[((gui.img.xy.xaxis(2)-gui.img.xy.xaxis(1))/size(gui.img.xy.pixels,1)) ((gui.img.xy.yaxis(2)-gui.img.xy.yaxis(1))/size(gui.img.xy.pixels,1))];

%Alternative q calculation algo.
%too lazy to figure out how to do this fast
% h = waitbar(0,'Please wait...');
%let's preallocate for speed
% q=zeros(size(gui.img.xy.pixels,1),size(gui.img.xy.pixels,2));
% for i=1:size(gui.img.xy.pixels,1)
%     q_x(i)=(gui.img.xy.xaxis(1)+pixelsize(1)*i);
% end
% for j=1:size(gui.img.xy.pixels,2)
%     q_y(i)=(gui.img.xy.yaxis(1)+pixelsize(2)*j);
% end
% % for i=1:size(gui.img.xy.pixels,1)
%     for j=1:size(gui.img.xy.pixels,2)
%         q(:,j)=sqrt(q_x.^2.+q_y(j)^2);
%     end
% %     if i/100==round(i/100)
% %         waitbar(i/size(gui.img.xy.pixels,1),h);
% %     end
% % end
% % close(h)

%ask for fit parameters
options={'Enter lower fitting limit on q (A^{-1})'; 'Enter upper fitting limit on q (A^{-1})'; 'Enter lower fitting limit on \phi (deg.)'; 'Enter upper fitting limit on \phi (deg.)'};
title='Fitting limits';
def={'0.1','2','90','270'};
argument.Resize='on';
argument.WindowStyle='normal';
argument.Interpreter='tex';
lts=inputdlg(options,title,1,def,argument);
limits=zeros(size(lts,1)); %preallocating for speed.
for i=1:size(lts,1)
 limits(i)=str2double(lts(i));
end

%determine fitting relevant data
relevant_data_indices_qphi=find(limits(2) > q && q > limits(1) && limits(3) < phi && phi < limits(4));
%relevant_data_indices_qphi=find(limits(3)< phi (relevant_data_indices_q) && phi(relevant_data_indices_q)<limits(4));

%this should make things more clear:
I=gui.img.xy.pixels(relevant_data_indices_qphi);
qs=q(relevant_data_indices_qphi);
phis=phi(relevant_data_indices_qphi);

%how to fix the variables and initguess here?
%[Fitparam,Fitval,exitflag,output]=fminsearch(@(initguess)errorfun(str2func(func),I,qs,phis,variables,initguess),optimset('MaxFunEvals',10000,'MaxIter',10000));%optimset('Tolx',0.0000001)
[Fitparam,Fitval,exitflag,output]=fminsearch(@(initguess)errorfun(func,I,qs,phis,variables,initguess),initguess,optimset('MaxFunEvals',10000,'MaxIter',10000));%optimset('Tolx',0.0000001)

%if it's no good...
if exitflag==0
    warndlg('maximum number of iterations reached');
    display(output.message);
end

%and here we define the function that needs to be minimized, basically an
%error function.

function err=errorfun(func,I,qs,phis,varargin)
warning off all;
func=str2func(func);
warning on all;
Icalc=func(qs,phis,varargin);
err=norm(Icalc-I);


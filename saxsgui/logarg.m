function log = logarg(prefix, arg, name)
%LOGARG Make up text to log an argument in a saxs history.
%   LOG = LOGARG(PREFIX, ARG, NAME) creates a 
%   log entry for an argument.  For example,
%   'plus scalar 20 (x)', for a scalar argument valued 20 and
%   named x.  PREFIX is 'plus', 'minus', or the like.
%
%   If ARG is a SAXS object, its history is appended
%   on subsequent rows of LOG, prefixed by '>'.

log = prefix;

if isa(arg, 'saxs')
    log = [log, ' saxs'];
    if name
        log = [log, ' (', name, ')'];
    end
    log = strvcat(log, [repmat('>', size(arg.history, 1), 1), arg.history]);
else  % arg is either scalar or array, named variable or expression
    if length(arg) == 1  % scalar
        log = [log, ' scalar'];
    else  % array
        log = [log, ' array'];
    end
    if length(arg) == 1  % show value of scalar
        log = [log, ' ', num2str(arg)];
    end
    if name  % show variable name
        log = [log, ' (', name, ')'];
    end
end

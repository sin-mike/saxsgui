function [filepath,filenames]=getfiles(description_text)
% GETFILES A program that allows you to choose a number of files from the same
% directory.
%     [path,files]=getfiles(description_text)
% results in a window popping up (with title description text) where one
% can either choose individual files (Windows) or a whole directory of
% files (Linux). The output parameter path is a single string and files contains the list of
% file names.
%
% Important note:
% In later versions (7.x) of matlab there exists a uigetfile option for multiple
% file entry...however there are serious bugs with it...so instead we use
% the uigetfiles.dll program which works for recent versions of matlab run
% on a PC (except the executables)
%
% This problem now seems fixed and we use the getfile with MultipleSelect opiton
% From 2007b and onwards

% New code starts here
persistent rememberdir
[filenames, filepath] = uigetfile([fullfile(rememberdir, '*.mpa'), ...
    ';*.mat;*.tif;*.tiff;*.img;*.inf;*.gfrm;*.unw;*.cmb;*.edf;*.lst'], ...
    description_text,'MultiSelect', 'on');
if filepath~=0
    rememberdir=filepath;
else
    filenames=[];
    filepath=[];
end
%if one file is chosen it does not come as a cell...but we have to have it
%come in a cell structure
if ~iscell(filenames)
   filenames={filenames}
end

% % old code starts here
% global executableversion
% persistent lastdir
% %We find the list of files to process
% if isempty(executableversion), executableversion=0; end
% if nargin==0, description_text='getfiles'; end
% filepath=[];
% ver=version('-release');
%
% if
% if ispc && (str2double(ver)>10 || str2double(ver(1:2))>10) && executableversion~=1
%     [filenames,filepath] = uigetfiles('*.*', description_text);
%     if filepath==0
%         warndlg('No files chosen.')
%         return
%     end
%     %filenames=[filenames(2:length(filenames)),filenames(1)]; % reorders to make it conform to the user file order
% else
%     %For Linux, early versions of Matlab, or the executable this is
%     %a quick and dirty substitution for uigetfile() with multiple file
%     %selection enabled equivalent to
%     %[FileName,PathName]=uigetfile('FilterSpec','DialogTitle','MultiSelection','on');
%     % Strong inspiration From Akiyuki Anzai on  comp.soft-sys.matlab
%     if ~isempty(lastdir)
%         questiondialogue=(['Which Directory? Same:',lastdir,' or New?']);
%         ButtonName=questdlg(questiondialogue, ...
%             'Choose directroy for Multiple file selection','Same','New Directory','yes');
%         switch ButtonName
%             case 'Same'
%                 filepath=lastdir;
%             case 'New Directory'
%                 filepath=strcat(uigetdir('Choose new directory'),filesep);
%                 if filepath==0
%                     warndlg('No files chosen. Batch activity exited.')
%                     return
%                 end
%         end
%         if isempty(filepath)
%             warndlg('No files chosen. Batch activity exited')
%         end
%     else
%         filepath=uigetdir('Choose a directory');
%         if filepath==0
%             warndlg('No files chosen. Batch activity exited.')
%             return
%         end
%     end
%
%     cd(filepath)
%     files=dir('*.*');
%     %remove "." and ".." which are always first 2 elements in the dir
%     files=files(3:end);
%     %now sorting by creation time
%     filesname={files.name};
%     filetimes=datenum({files.date});
%     % sort according to creating time
%     [Y,NDX]=sort(filetimes,1,'descend');
%     % now apply to filenames
%     sortedfiles.name=filesname(NDX);
%     [Selection,ok]=listdlg('Name',description_text,'ListString',sortedfiles.name);
%     filenames=fliplr(sortedfiles.name(Selection));
%     %modification data is given in files(Selection).date
%     if isempty(filenames)
%         %to make it consistent with uigetfile() return
%         filenames=0;
%         filepath=0;
%     else
%         filepath=strcat(pwd,filesep);
%     end
% end
% lastdir=filepath;
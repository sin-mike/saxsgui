function [photodiode,totrate]=readmpaheader(fdir,fname)
%
% read in data from the MPA created during LabView run
%
%     function [photodiode,totrate]=readmpaheader(fdir,fname)
%
% must include photodiode in cmline9 comment
bb=0;   %counter for breaking once we've found what we are looking for
%ii %counter for seeing how far weve read in the file

pathname=[fdir,char(fname)];
try  % reading data-header from the impa-file
    fid=fopen(pathname,'r');
catch
    caught = lasterr;  % error message
    fclose('all');
    error(['Error mpa-file configuration file', pathname, ':  ', caught])
end
for ii=1:80
    s=fgetl(fid);
    if strcmp(s(1:min(7,length(s))),'cmline9')
        photodiode=abs(str2num(s(20:length(s))));
        bb=bb+1;
    end
    if strcmp(s(1:min(8,length(s))),'TOTALSUM')
        totcount=str2num(s(10:length(s)));
        bb=bb+1;
    end
    if strcmp(s(1:min(9,length(s))),'livetime=')
        ltime=str2num(s(10:length(s)));
        bb=bb+1;
    end
    if (ii>80 || bb==3)
        fclose(fid);
        break;
    end
end
if (bb<3 || bb>3)
    photodiode=0;
    totrate=totcount/ltime;
    fclose(fid);
    error(['Error: The mpa file', pathname, ':  ', caught, 'was not created in Labview'])
end
totrate=totcount/ltime;
fclose('all');



function h = statusbox(message, title)
%STATUSBOX Display a one-line status message in a window.
%   H = STATUSBOX(MESSAGE, TITLE)
width = max(length(message), length(title));
width = width + 30;  % title bar decorations, white space
height = 3;
[rootwidth rootheight] = rootsizechars;
position(1) = (rootwidth - width) / 2;
position(2) = (rootheight - height) / 2;
position(3) = width;
position(4) = height;
color = get(0, 'defaultuicontrolbackgroundcolor');
fig = figure('Units', 'characters', 'Position', position, ...
    'MenuBar', 'none', 'Name', title, 'NumberTitle', 'off', ...
    'BackingStore', 'off', 'Color', color, 'Resize', 'off');
uicontrol(fig, 'Style', 'text', 'Units', 'characters', ...
    'Position', [0.5, (height - 1.1) / 2, width - 1, 1.1], ...
    'String', message)
drawnow
if nargout
    h = fig;
end

function [w, h] = rootsizechars
oldunits = get(0, 'Units');
set(0, 'Units', 'characters')
size = get(0, 'ScreenSize');
set(0, 'Units', oldunits)
w = size(3);
h = size(4);

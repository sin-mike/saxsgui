function ap_xy_scan(gui,tmppath,tmpfiles)
global ap_file ap_struct info_struct
global remember_fdir
global AP_batch AP_batch_filename


%Tell us how many rows and columns in the scan
% and tell us how far each step was
%rows=13;
%columns=13;
%rowstep=0.5; %values should be give in mm otherwise labels won't fit
%columnstep=0.5; %values should be given in mm otherwise labels won't fit
rows=ap_struct.xy_numy;
columns=ap_struct.xy_numx;
rowstep=ap_struct.xy_stepy;
columnstep=ap_struct.xy_stepx;
samplenamelist=tmpfiles;
sampledir=tmppath;
if ~(rows*columns == length(samplenamelist))
    disp ('rows and columns do not agree with number of files chosen')
    disp ('will pad the missing files with empty images')
    %rows=1;
    %columns=length(samplenamelist);
    %rowstep=1;
    %columnstep=1;
end

%do transmission map, absorption map, and intesity map by reading info and mpa files
%step through files
rowpos=((1:rows)-1)*rowstep;
columnpos=((1:columns)-1)*columnstep;
photodiode=zeros(1,length(samplenamelist));
totrate=zeros(1,length(samplenamelist));
for ii=1:length(samplenamelist)
    [photodiode(ii),totrate(ii)]=readmpaheader(sampledir,samplenamelist(ii));
end
transmap=reshape(photodiode,rows,columns);
ratemap=reshape(totrate,rows,columns);
if (ap_struct.doxy_transmap)
    figure
    imagesc(columnpos,rowpos,transmap)
    title([ap_struct.xy_name,': Transmission (Photodiode reading) vs Position'])
    xlabel('position in mm')
    ylabel('position in mm')
    colorbar
    set(gcf,'Name','XY Scan: Transmission')
end
if (ap_struct.doxy_intmap)
    figure
    imagesc(columnpos,rowpos,ratemap)
    colorbar
    title([ap_struct.xy_name,': Total Counts vs Position (in mm)'])
    xlabel('position in mm')
    ylabel('position in mm')
    set(gcf,'Name','XY Scan: Scattering Intensity')
end

if (ap_struct.doxy_intmap)
    figure
    
    imagesc(columnpos,rowpos,max(max(transmap))-transmap)
    colorbar
    title([ap_struct.xy_name,': Relative Absorption vs Position (in mm)'])
    xlabel('position in mm')
    ylabel('position in mm')
    set(gcf,'Name','XY Scan: Absorption')
end

figure(gui.figure)
if ap_struct.doxy_arrow_output
    if ap_struct.xy_arrow_background==3
        xy_background_map=ratemap;
    elseif ap_struct.xy_arrow_background==2
        xy_background_map=max(max(transmap))-transmap;
    else
        xy_background_map=transmap;
    end
    %now we start the processing of the files
    abackground=[];
    adirection=[];
    asize=[];
    for ii=1:length(tmpfiles)
        [a,b,c]=find_arrow_params(gui,sampledir,samplenamelist{ii});
        abackground(ii)=a;
        adirection(ii)=b;
        asize(ii)=c;
    end
    ASIZE=reshape(asize,rows,columns);
    ADIRECTION=reshape(adirection,rows,columns);
    figure
    imagesc(columnpos,rowpos,max(max(transmap))-transmap)
    colorbar
    title([ap_struct.xy_name])
    xlabel('position in mm')
    ylabel('position in mm')
    hold on
    [X,Y]=meshgrid(columnpos,rowpos);
    U=ASIZE.*cos(ADIRECTION);
    V=ASIZE.*sin(ADIRECTION);
    quiver('v6',X,Y,U,V,max(asize))
    set(gcf,'name','XY Scan')
    hold off

end

function   [abackground,adirection,asize]=find_arrow_params(gui,tmp_dir,tmp_file)
global ap_file ap_struct info_struct
global remember_fdir
global AP_batch AP_batch_filename

%Now load data and information in the data file and the 
%mpa-info file
im_file=fullfile(tmp_dir,tmp_file);
statusline = status(['Opening image file...',tmp_file], gui);
S=getsaxs(im_file); %loads parameters from the datafile itself and
delete(statusline);

statusline = status('Loading info file...', gui);
info_struct=mpainforead(im_file); %loads parameter from the info-dat file if it is there
delete(statusline);


%There may be conflicting/competing parameters in the various files
%so they need to ber merged.
%the basic rule is that anything in the info-file has
%preference,(info_struct)
%after which values from the AP configuration file 
%are used (ap_struct)
%pixel values, exposure time and date is always taken from the image file
%itself

statusline = status('Prioritizing parameters...and loading reduction files..', gui);
S=apinfo_merge(S,info_struct,ap_struct); 
delete(statusline);
abackground=0;
adirection=rand*2*pi;
asize=rand;

% %Start with Azimuthal scan
% %Averaging parameters 
% bbcor=0  %beamstop blocking correction not done for Azimuthal scan
% azimstart=0; %negative values not yet implemented!
% azimend=360;
% lambda=1.54;   %radiation wavelength in �
% qmin=0.015;
% qmax=0.93*2*pi/(1.54/(2*sin(atan(detectorsize/estimated_distance/2/2)))); 
% qpoint=100;
% phipoint=181; %used for I vs. Phi scans
% qtype='Lin';   %can be either 'Log' or 'Lin' indicating linear or logarithmic binning in X.
% 
% calc_center=0 %should center of calibration image be used 1=yes 0=No...If no enter values below:
% if ~(calc_center) 
%     measuredycenpeak=507.5 %this parameter is obtained from an actual images since AgBeh image was not take
%     measuredxecnpeak=515.6 %this parameter is obtained from an actual images since AgBeh image was not take
% end
% 
% %samplenamelist=samplenamelist(84)
% %now run batch process
% azimuthalavebatch
% 
% resfile=[samplefilebase,'phiresults.asc']
% save 'phi-eccentricity' phiresults
% 
% %Now for Radial Scan
% %Averaging parameters 
% bbcor=1  %beamstop blocking correction not done for Azimuthal scan
% azimstart=0; %negative values not yet implemented!
% azimend=270;
% lambda=1.54;   %radiation wavelength in �
% qmin=0.001;
% qmax=0.93*2*pi/(1.54/(2*sin(atan(detectorsize/estimated_distance/2/2)))); 
% qpoint=100; %used for I vs. q scans
% qtype='Log';   %can be either 'Log' or 'Lin' indicating linear or logarithmic binning in X.
% 
% calc_center=0 %should center of calibration image be used 1=yes 0=No...If no enter values below:
% if ~(calc_center) 
%     measuredycenpeak=507.5 %this parameter is obtained from an actual images since AgBeh image was not take
%     measuredxecnpeak=515.6 %this parameter is obtained from an actual images since AgBeh image was not take
% end
% 
% %now run batch process
% radialavebatch
% 
% resfile=[samplefilebase,'radialresults.asc']
% save 'radial_results' radialresults
% 
% % azimstart=0; %negative values not yet implemented!
% % azimend=360;
% % lambda=1.54;   %radiation wavelength in �
% % qmin=0.015;
% % qmax=0.93*2*pi/(1.54/(2*sin(atan(detectorsize/estimated_distance/2/2)))); 
% % phipoint=360; %used for I vs. Phi scans
% % qpoint=100; %used for I vs. q scans
% % qtype='Log';   %can be either 'Log' or 'Lin' indicating linear or logarithmic binning in X.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STATUS BAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h_out = status(message, gui, seconds)
% Display a status line message, returning its handle.
% If you the caller don't specify SECONDS, then you must delete the status
% text control via its handle.
if nargin == 2
    h = figstatus(message, gui.figure);
elseif nargin == 3
    h = figstatusbrief(message, seconds, gui.figure);
end
if nargout
    h_out = h;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


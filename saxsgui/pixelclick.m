function xy = pixelclick(ax)
%PIXELCLICK Return x,y pixel coordinates of pointer click.
%   PIXELCLICK returns x,y coordinates of the next mouse click in the
%   current axes. The axes must contain an image object.  The returned
%   value is a two-element vector [x, y] of pixel coordinates in the image.
%
%   PIXELCLICK(AXES) works in the axes specified by AXES, an axes handle.
%
%   See also:  XYCLICK.

if nargin
    if ~ (ishandle(ax) && isequal(get(ax, 'Type'), 'axes'))
        error('Argument must be a handle to an axes object.')
    end
else  % no argument
    ax = gca;  % use current axes
end
fig = get(ax, 'Parent');

ims = findobj(ax, 'Type', 'image');
if isempty(ims)
    error('I don''t see any images here.')
end
set(ims, 'ButtonDownFcn', @click)
old_pointer = get(fig, 'Pointer');  % restore later
set(fig, 'Pointer', 'fullcrosshair')
uiwait(fig)  % wait for click or figure deletion

if ishandle(ax)  % our axes is still around
    set(ims, 'ButtonDownFcn', [])
    set(fig, 'Pointer', old_pointer)
    xy = getappdata(ax, 'pixelclick');  % retrieve click coords
    rmappdata(ax, 'pixelclick')
else  % figure must've been deleted
    xy = [];
end


function click(im, event)
% Image ButtonDown callback.
ax = get(im, 'Parent');
xy = get(ax, 'CurrentPoint');
xy = [xy(1), xy(3)];  % two dimensions only, please
xy(1) = ax2imx(im, xy(1));  % image pixel coordinates
xy(2) = ax2imy(im, xy(2));
setappdata(ax, 'pixelclick', xy)  % save for main function
uiresume(get(ax, 'Parent'))  % restart main function


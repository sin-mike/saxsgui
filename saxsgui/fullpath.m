function path = fullpath(partpath)
%FULLPATH Return full directory path for a partial path.

% Bogus hack.

started_from = pwd;
if isdir(partpath)
    cd(partpath)
    path = pwd;
    cd(started_from)
else
    error(['Not a directory path: ', partpath])
end

function hgui = saxsgui(varargin)
%SAXSGUI View and manipulate SAXS images in a point-and-click style.
%   SAXSGUI helps find an image file, then opens the image in a window
%   providing a point-and-click interface to basic SAXS image operations.
%
%   SAXSGUI can open Many image types:  MPA files; MAT (MATLAB saved data)
%   files; and TIFF gray-scale image files, Bruker Hi-Star files, Fuji img
%   files, Risoe CMB files, EDF files from Pilatus detectors
%
%   See the SAXSGUI User's Guide for more SAXSGUI capabilities.
%
%   H = SAXSGUI returns a handle to the GUI figure window.
%
%   SAXSGUI(S) loads the image stored in SAXS variable S.  You may perform
%   arithmetic operations and cropping and centering operations on S before
%   loading it into SAXSGUI, but do not take a logarithm of it or convert
%   it to polar coordinates--that would confuse SAXSGUI.

%   SAXSGUI(1, 'function', ...) calls subfunction 'function' with any
%   subsequent arguments.  Events in the figure window call us back with
%   this form.

% GUI data:
%
% gui.<tag>       - handles of most GUI controls, by tag name
% gui.img         - the image; multiple views and properties
%     img.xy      - xy (left-hand) image (saxs object)
%     img.rth     - rth (right-hand) image (saxs object)
%     img.name    - overall image name = file name if from file
%     img.xy_unsmoothed  - present when xy is smoothed (for undo)
%     img.xy_prereduction_pixels- hold the data as it was before reduction
%                   (can be smoothed)
%
% Option settings are recorded in the respective menu item's Checked property.
%
% NB:   Any subfunction that is not a callback and that changes some field
%       in the gui structure must return gui to the caller so we don't get
%       gui values out of sync at different function levels.
%
%       Likewise only the main function or callback subfunctions should
%       save gui data (via guidata(gui.figure, gui)).
%
% SIMPLICITY suggests we only save images as linear, not log.  If we try to
% use a log image, we can't go back to linear.  Also no need to save polar
% images, just centered rectangular-coordinate images.
global SaxsguiWindowName fitfunctions  data_path 
global allow_SAXSGUI_beta

% Reading SAXSGUI_setting.m for global variables describing local settings
SAXSGUI_settings
saxs_preferences_read

% saxsgui version and date
saxsgui_version='v2.07.02';
if allow_SAXSGUI_beta==1
    saxsgui_version=[saxsgui_version,' including beta-features'];
end
saxsgui_version_date='March 26, 2011';
SaxsguiWindowName=['saxsgui: ',saxsgui_version];

% Check MATLAB version.
matlabrelease = version('-release');
if (str2double(matlabrelease)<13 || str2double(matlabrelease(1:2))<13)
    error('SAXSGUI needs MATLAB release 13 or later.')
end

% Attempt to process the arguments with which we are invoked.
if ~ nargin  % no input arguments
    %first see if we have any other SAXSGUI windows open
    if ~ isempty(findobj(get(0,'children'), 'Name', SaxsguiWindowName));  % find existing figures with
        errordlg('One SAXSGUI window already opened. Use that one')
        error('Only one SAXSGUI at a time. Use the one already open!')
    end
    %now we will look for fitting functions..
    %for the non-executable the fitting function library is dynamic:
    %building a cell array to describe which fitting functions are available
    
    %this may take some time so therefore we decide to "warn the user"
    disp(['Initializing SAXSGUI ',saxsgui_version])

    %locate_fitfunctions
    if executableversion==1
        try
            load standard_fitfunctions
            invoke_standard_fitfunction;
        catch
            errordlg('Not able to find standard fitfunction - FullFit will not function correctly')
        end
        invoke_maskmaker_function;
        invoke_quikfit_functions;
    else
        try
            fitfunctions.oneD=find_fitfunctions(1);
        catch
            fitfunctions.oneD=[];
        end
        try
            fitfunctions.twoD=find_fitfunctions(2);
        catch
            fitfunctions.twoD=[];
        end
    end
    disp(['Done initializing...'])
    reduct_par_init
    % Now to the real thing
    % Find an image file and open it.
    
    img = empty_img; % cd(work_path); Milos
    [img(1).xy, img(1).name] = getsaxs; %getsaxs(data_path);
    if ~ isempty(img.xy)  % we have an image structure
        % Guess if image has been centered.
        if ~ any(isnan(pixelfrac(img.xy, [0 0])))
            status_window = statusbox('Computing image in polar coordinates...', 'Busy');
            img.rth = polar(img.xy);
            delete(status_window)
        end
        gui = makegui(img);  % open our figure window
        % now saves this xy data in an "untouched" area that can be used
        % for repeating use of reductions
        gui.img.xy_prereduction_pixels=gui.img.xy.pixels; 
        guidata(gui.figure, gui)  % save data with figure window
        set(gui.figure,'Name',SaxsguiWindowName)
        if allow_SAXSGUI_beta==0
            set(gui.MenuImageAnalysis2D,'Visible','off')
            set(gui.MenuOpenDB,'Visible','off')
        else
            set(gui.MenuImageAnalysis2D,'Visible','on')
            set(gui.MenuOpenDB,'Visible','on')
        end
    end
else  % parse input arguments
    if isequal(varargin{1}, 1)  % this is a gui callback
        feval(varargin{2:end});
    elseif nargin == 1 && isa(varargin{1}, 'saxs')
        if ~ isequal(type(varargin{1}), 'xy')
            error('I can only start with an x,y image.')
        end
        img = empty_img;
        img(1).xy = varargin{1};
        img.name = inputname(1);
        if ~ any(isnan(pixelfrac(img.xy, [0 0])))
            status_window = statusbox('Computing image in polar coordinates...', 'Busy');
            img.rth = polar(img.xy);
            delete(status_window)
        end
        gui = makegui(img);  % open our figure window
        set(gui.figure,'Name',SaxsguiWindowName) % make sure the window has the right name
        guidata(gui.figure, gui)  % save data with figure window
        if allow_SAXSGUI_beta==0
            set(gui.MenuImageAnalysis2D,'Visible','off')
            set(gui.MenuOpenDB,'Visible','off')
        else
            set(gui.MenuImageAnalysis2D,'Visible','on')
            set(gui.MenuOpenDB,'Visible','on')
        end
    else
        error('I don''t understand the command arguments.')
    end
end
% Return figure handle if requested.
if nargout
    try
        hgui = gui.figure;
    catch
        hgui = [];
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function img = empty_img
img = struct('xy', {}, 'rth', {}, 'name', {});
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = makegui(img)
% Make a GUI figure window using image data from img.
SAXSGUI_settings
warning off all
gui = guihandles(openfig('saxsgui.fig', 'new'));
if strcmp(spec_installation,'no')
    set(gui.MenuFileOpenSpec,'Visible','off')
else
    set(gui.MenuFileOpenSpec,'Visible','on')
end
warning on all
set(gui.figure, 'Color', get(0, 'DefaultUicontrolBackgroundColor'))  % system default background color
set(gui.MenuRotateCompute, 'Enable', 'off')  % disable 'Compute rotation' command
gui.img = img;
init_colorscale(gui)
gui = images(gui);  % draw our images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function init_colorscale(gui)
% Set up color scale controls for a new image.
top = max(gui.img.xy(:)+2);  % top of color scale
set(gui.top_text, 'String', num2str(top))
set(gui.top_slider, 'Min', 1, 'Max', top, 'Value', top)
set(gui.top_slider, 'SliderStep', [1 / (top - 1), 20 / (top - 1)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = images(gui_in)

% Draw images, including the color bar.
% Avoid depending on 'current' axes, since the user could be clicking
% around.

remove_projection_line

gui = gui_in;
guimage(gui, gui.xy_axes, gui.img.xy);
set(get(gui.xy_axes, 'Title'), 'String', gui.img.name, 'Interpreter', 'none')
guimage(gui, gui.rth_axes, gui.img.rth)
set(get(gui.rth_axes, 'Title'), 'String', 'Polar Transformation')
if ischecked(gui.MenuViewZoom)
    set(gui.MenuViewZoom, 'Checked', 'off')
end
% If we're looking at log display, hide the color scale controls.
colorcontrols = [gui.top_label, gui.top_text, gui.top_slider];
if ischecked(gui.MenuViewLog)
    set(colorcontrols, 'Visible', 'off')
else
    set(colorcontrols, 'Visible', 'on')
end
set(gui.rth_axes, 'CLim', get(gui.xy_axes, 'CLim'))  % use one color scale (xy)
gui = update_colorbar(gui);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function guimage(gui, haxes, sax, varargin)
% Put up an image of saxs object s in an axes.
% If sax, the saxs object, is empty, hide the axes.
% We take the log of the image or not, then pass arguments on to
% saxs/image.
% Keep current with View menu options (Grid, etc.) and color scale.
if isempty(sax)
    set(haxes, 'Visible', 'off')
else
    set(haxes, 'Visible', 'on')
    if isequal(get(gui.MenuViewLog, 'Checked'), 'on')
        hstatus = status('Plotting log image...', gui);
        himage = image(log10((sax)), 'Parent', haxes, varargin{1:end});
        delete(hstatus)
    else
        ctop = get(gui.top_slider, 'Value');
        himage = image(sax, 'Parent', haxes, varargin{:}, [0 ctop]);
        % Work around stupid bug where CLim specifier is ignored.
        set(haxes, 'CLim', [0 ctop]);
    end
    if ischecked(gui.MenuViewGrid)
        grid on
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function packmemory(gui)
cwd = pwd;
cd(tempdir);
statusbar = status('Packing memory...', gui);
pack
delete(statusbar)
cd(cwd)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function trackpointer(onoff, gui)
switch onoff
    case 'on'
        set([gui.xy_track, gui.rth_track], 'Visible', 'on')
        set(gui.figure, 'WindowButtonMotionFcn', ...
            {@display_currentpoint, guidata(gcbo)})
    case 'off'
        set(gui.figure, 'WindowButtonMotionFcn', [])
        set([gui.xy_track, gui.rth_track], 'Visible', 'off')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function display_currentpoint(fig, event, gui)
xy_point = get(gui.xy_axes, 'CurrentPoint');
xy_point = [xy_point(1) xy_point(3)];  % just 2-D x,y
xy_text = [num2str(xy_point(1),'%4.2f'), ',', num2str(xy_point(2),'%4.2f')];
set(gui.xy_track, 'String', xy_text)
rth_point = get(gui.rth_axes, 'CurrentPoint');
rth_point = [rth_point(1) rth_point(3)];  % just 2-D x,y
rth_text = [num2str(rth_point(1),'%4.2f'), ',', num2str(rth_point(2),'%4.2f')];
set(gui.rth_track, 'String', rth_text)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STATUS BAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h_out = status(message, gui, seconds)
% Display a status line message, returning its handle.
% If you the caller don't specify SECONDS, then you must delete the status
% text control via its handle.
if nargin == 2
    h = figstatus(message, gui.figure);
elseif nargin == 3
    h = figstatusbrief(message, seconds, gui.figure);
end
if nargout
    h_out = h;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLOR SCALE CONTROL CALLBACKS AND UTILITIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_slider(slider, gui)
% Callback for slider controlling top of color scale.
% top_text calls this too.
top = fix(get(slider, 'Value'));
set(slider, 'Value', top)
set(gui.top_text, 'String', num2str(top))
set([gui.xy_axes, gui.rth_axes], 'CLim', [0 top])
gui = update_colorbar(gui);
guidata(gui.figure, gui)  % save gui data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_text(edit, gui)
% Callback for text edit box controlling top of color scale.
num = str2double(get(edit, 'String'));
if ~ isnan(num)  % we have a number
    slider = gui.top_slider;
    if num >= get(slider, 'Min') && num <= get(slider, 'Max')
        set(slider, 'Value', num)
    end
    top_slider(slider, gui)  % update everything
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = update_colorbar(gui_in)
% Because of technical difficulties with colorbar, it seems we must delete
% and recreate it.
gui = gui_in;
haxes=guihandles(gui.figure);
if ~isfield(haxes,'Colorbar')
    colorbar(gui.colorbar_axes, 'peer', gui.xy_axes)
    haxes=guihandles(gui.figure);
end
%cbpos = get(gui.colorbar_axes, 'Position');
%delete(gui.colorbar_axes)
%gui.colorbar_axes = axes('Position', cbpos);
%guidata(gui.figure, gui) % save info
%colorbar(gui.colorbar_axes, 'peer', gui.xy_axes)
colorlim=get(gui.xy_axes,'Clim');
set(haxes.Colorbar,'Xlim',colorlim);
set(haxes.TMW_COLORBAR,'XData',colorlim);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MENU CALLBACKS AND UTILITIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileOpen(menu, gui)
global old_saxs_obj
global multiple_file_run
global last_open_how
% Callback for File menu Open command.
% Open .mpa or .mat files.  Verify .mat file is one of our saves.
% What state to preserve, what to reset?  Mostly reset.
% Jun 1, 2006, added old_saxs_obj to allow quik processing
remove_figstatus % this line deletes all the yellow "statuslines"
% and let's just make sure the next and previous buttons are now visible
haxes=guihandles(gui.figure);
set(haxes.ButtonOpen,'Visible','on')
set(haxes.NextFile,'Visible','on')
% finished
multiple_file_run=0;
if ~isempty(gui.img.rth)
    old_saxs_obj=gui.img.rth;
end
img = empty_img;
statusline = status('Opening image file...', gui);
[img(1).xy, img(1).name] = getsaxs;
delete(statusline)
if ~ isempty(img.xy)  % we have an image structure
    % Guess if image has been centered.
    if ~ any(isnan(pixelfrac(img.xy, [0 0])))
        statusline = status('Computing image in polar coordinates...', gui);
        img.rth = polar(img.xy);
        delete(statusline)
    end
    gui = newimage(img, gui);
    guidata(gui.figure, gui)
    last_open_how='normal';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileOpenUsingMetaData(menu, gui)
global old_saxs_obj
global multiple_file_run
global k pixelcal koffset wavelength pixelsize detector_dist calib_type
global wavelength_default
global last_open_how
% Callback for File menu Open command using header information
remove_figstatus % this line deletes all the yellow "statuslines"
% and let's just make sure the next and previous buttons are now visible
haxes=guihandles(gui.figure);
set(haxes.ButtonOpen,'Visible','on')
set(haxes.NextFile,'Visible','on')
% finished

multiple_file_run=0;
if ~isempty(gui.img.rth)
    old_saxs_obj=gui.img.rth;
end
img = empty_img;
statusline = status('Opening image file...', gui);
[img(1).xy, img(1).name] = getsaxs;
delete(statusline)
if ~ isempty(img.xy)  % we have an image structure
    %load all header parameters to the saxsobjet
    img.xy=fill_from_saxslab_header(img.xy);
    
    % Guess if image has been centered.
    if ~ any(isnan(pixelfrac(img.xy, [0 0])))
        statusline = status('Computing image in polar coordinates...', gui);
        img.rth = polar(img.xy);
        delete(statusline)
    end
    gui = newimage(img, gui);
    guidata(gui.figure, gui)
    %saving for calibration
    k = gui.img.xy.kcal;
    pixelcal = gui.img.xy.pixelcal;
    koffset = gui.img.xy.koffset;
    wavelength = gui.img.xy.wavelength;
    pixelsize = gui.img.xy.pixelsize;
    detector_dist = gui.img.xy.detector_dist;
    last_open_how='meta';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileOpenSpec(menu, gui) 
% Callback for File menu Open from SPEC file command.
% Allows user to choose an image that was taken as part of a spec scan
% The image can be any of the recognized formats...but to get there
% the user needs to choose which spec log file, which scan in the log file,
% and which point in the scan.
global old_saxs_obj
global last_open_how
remove_figstatus % this line deletes all the yellow "statuslines"
% and let's just make sure the next and previous buttons are now visible
haxes=guihandles(gui.figure);
set(haxes.ButtonOpen,'Visible','off')
set(haxes.NextFile,'Visible','off')
% finished

if ~isempty(gui.img.rth)
    old_saxs_obj=gui.img.rth;
end

img = empty_img;
statusline = status('Getting image file from SPEC-file...', gui);
[img(1).xy, img(1).name] = getspecsaxs; 
delete(statusline)
if ~ isempty(img.xy)  % we have an image structure
    % Guess if image has been centered.
    if ~ any(isnan(pixelfrac(img.xy, [0 0])))
        statusline = status('Computing image in polar coordinates...', gui);
        img.rth = polar(img.xy);
        delete(statusline)
    end
    gui = newimage(img, gui);
    guidata(gui.figure, gui)
    last_open_how='spec';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileOpenDB(menu, gui) 
% Callback for File menu Open from Database file 
% Allows user to choose an image that has previously been entered into the
% generic database

global old_saxs_obj
global last_open_how
if ~isempty(gui.img.rth)
    old_saxs_obj=gui.img.rth;
end
remove_figstatus % this line deletes all the yellow "statuslines"
% and let's just make sure the next and previous buttons are now visible
haxes=guihandles(gui.figure);
set(haxes.ButtonOpen,'Visible','off')
set(haxes.NextFile,'Visible','off')
% finished

img = empty_img;
statusline = status('Getting image file from DataBase...', gui);
img(1).xy = sampledb2_beta;
if ~isempty(img(1).xy)
    img(1).name = img(1).xy.raw_filename;

    delete(statusline)
    if ~ isempty(img.xy)  % we have an image structure
        % Guess if image has been centered.
        if ~ any(isnan(pixelfrac(img.xy, [0 0])))
            statusline = status('Computing image in polar coordinates...', gui);
            img.rth = polar(img.xy);
            delete(statusline)
        end
        gui = newimage(img, gui);
        guidata(gui.figure, gui)
        last_open_how='DB';
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileOpen_and_add(menu, gui)
% Callback for File menu "Open and add" command, which import multiple
% files and adds them all up.
global old_saxs_obj
global last_open_how
remove_figstatus % this line deletes all the yellow "statuslines"
% and let's just make sure the next and previous buttons are now visible
haxes=guihandles(gui.figure);
set(haxes.ButtonOpen,'Visible','off')
set(haxes.NextFile,'Visible','off')
% finished
if ~isempty(gui.img.rth)
    old_saxs_obj=gui.img.rth;
end
img = empty_img;
Ssum=0;
[filepath,files]=getfiles('Choose all the files you want to add together');
if ~isempty(files) % this fails if we choose nothing or cancel 
    for ii=1:length(files)
        filename=[filepath files{ii}];    
        hstatus = status(['Loading and adding ',files{ii}],gui);  % tell user what is going on in a status bar
        [S Name]=getsaxs(filename);
        Ssum=plus(Ssum,S);
        delete(hstatus)
    end
    Ssum=raw_filename(Ssum,'Sum of many');
    img(1).xy= Ssum;
    img(1).name='Sum of many';
    if ~ isempty(img.xy)  % we have an image structure
        % Guess if image has been centered.
        if ~ any(isnan(pixelfrac(img.xy, [0 0])))
            statusline = status('Computing image in polar coordinates...', gui);
            img.rth = polar(img.xy);
            delete(statusline)
        end
        gui = newimage(img, gui);
        guidata(gui.figure, gui)
        last_open_how='add';
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ButtonOpen(gui,filenum_increment)
% Callback for Button Click on NextImage button
global old_saxs_obj
global last_open_how
if ~isempty(gui.img.rth)
    old_saxs_obj=gui.img.rth;
end
img = empty_img;

%find filenames with extension same as the present
[dirname,~,tmp,~]=fileparts(gui.img.xy.raw_filename);
D=dir([dirname,filesep,'*',tmp]);
D1=struct2cell(D);
filenum=find(strcmp(gui.img.name,D1(1,:)));
try
    newfilename=D1(1,filenum+filenum_increment);
catch
    if filenum_increment>0
        errordlg('There is no next file')
    else
        errordlg('There is no previous file')
    end
    if exist(statusline) 
        delete(statusline)
    end
    return
end
img = empty_img;
statusline = status('Opening image file...', gui);
[img(1).xy, img(1).name] = getsaxs([dirname,filesep,newfilename{1}]);
delete(statusline)
if ~ isempty(img.xy)  % we have an image structure
    %load all header parameters to the saxsobjet
    if strcmp('meta',last_open_how)
        img.xy=fill_from_saxslab_header(img.xy);
    end
    
    % Guess if image has been centered.
    if ~ any(isnan(pixelfrac(img.xy, [0 0])))
        statusline = status('Computing image in polar coordinates...', gui);
        img.rth = polar(img.xy);
        delete(statusline)
    end
    gui = newimage(img, gui);
    guidata(gui.figure, gui)
    %saving for calibration
    if strcmp('meta',last_open_how)
        k = gui.img.xy.kcal;
        pixelcal = gui.img.xy.pixelcal;
        koffset = gui.img.xy.koffset;
        wavelength = gui.img.xy.wavelength;
        pixelsize = gui.img.xy.pixelsize;
        detector_dist = gui.img.xy.detector_dist;
        last_open_how='meta';
    end
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = newimage(img, gui_in)
% img should have fields xy, rth, and name.  See empty_img.
gui = gui_in;
if ~ isempty(img)
    gui.img = img;
    delete(findobj(gui.figure, 'Type', 'image','Tag',''))  % delete all our images
    init_colorscale(gui)
    
    gui = images(gui);  % draw our images
    gui.img.xy_prereduction_pixels=gui.img.xy.pixels; % this saves the xy data in an "untouched"
    guidata(gui.figure, gui)
    %packmemory(gui)  % try to compact variables not possible in later
    %matlab versions
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileName(gui)
newname = inputdlg('Image name:', 'Change image name', 1, {gui.img.name});
if ~ isempty(newname)
    gui.img.name = newname{1};
    set(get(gui.xy_axes, 'Title'), 'String', gui.img.name)
end
guidata(gui.figure, gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileClose(menu, gui)
% Callback for File menu Close command.
% Don't even ask if they want to save anything.  Sheesh.
close(gui.figure)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileSave(gui)
% Callback for File menu Save command.
% Save our linear xy saxs object and some display options.
default_name = gui.img.name;
[a,b,c]=fileparts(default_name);
default_matname=[b,'.mat'];
[filename filedir] = uiputfile(default_matname, 'Save image in .mat file');
if filename
    xy = gui.img.xy;
    name = gui.img.name;
    ctop = get(gui.top_slider, 'Value');
    log = ischecked(gui.MenuViewLog);
    try
        save([filedir filename], 'xy', 'name', 'ctop', 'log')
    catch
        errordlg(lasterr, 'Error during file save', 'modal')
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileSaveTiff(gui,bitsize)
% Callback for File menu Save Tiff command.
% Save our linear xy saxs object as tiff file
[filedir filename ext] = fileparts(gui.img.name);
default_name=fullfile(filedir,filename);
default_name = [default_name, '.tif'];

[filename filedir] = uiputfile(default_name, 'Save image in .tiff file');
if filename
    try
        saxs2tiff(gui.img.xy,[filedir filename],bitsize);
    catch
        errordlg(lasterr, 'Error during tiff-file save', 'modal')
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileExportTextXY(gui)
saxsgui_export(gui.img.xy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileExportTextPolar(gui)
if ~ isempty(gui.img.rth)
    saxsgui_export(gui.img.rth)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileExportJPEG(gui)
[fname fdir] = uiputfile('*.jpg', 'Save saxsgui window to JPEG file');
set(gcf,'PaperPositionMode','auto')
if fname
    print(gui.figure, '-djpeg', [fdir fname])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileExportTIFF(gui)
[fname fdir] = uiputfile('*.tiff', 'Save saxsgui window to TIFF file');
set(gcf,'PaperPositionMode','auto')
if fname
    print(gui.figure, '-dtiff', [fdir fname])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileExportPDF(gui)
[fname fdir] = uiputfile('*.pdf', 'Save saxsgui window to PDF file');
set(gcf,'PaperPositionMode','auto')
if fname
    print(gui.figure, '-dpdf', [fdir fname])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileExportMatFig(gui)
[fname fdir] = uiputfile('*.fig', 'Save saxsgui window to Matlab Figure file');
if fname
    saveas(gui.figure,[fdir fname], 'fig' )
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileSet_Saxsgui_Preferences(menu,gui)
global water_abs_int glassy_carbon_abs_int water_attenuation_length
global cmask_radius smooth_size smooth_std
global fatpix_fontsize
global zinger_params
global averagex_prefs
global fujiimgrot90 fujiimgflip
name='Input SAXSGUI Preferences';
numlines=1;
prompts={'Absolute Scattering Crossection of water at 25 degrees (units: 1/cm):',...
         'Glassy Carbon Standard Crossection (Calibrated)',...
         'Water Attenuation Length (function of x-ray wavelength) (units: cm:',...
         'Auto-Centering Mask Radius (pixels)',...
         'Image smoothing (size of convolution box)',...
         'Image smoothing (width of smoothing function)',...
         'Fatpix Fontsize',...
         'Zinger: Size of smoothing box',...
         'Zinger: Zinger detection threshold (Zinger Int./Average Int.)',...
         'Zinger: Zinger mask (radius of mask around zinger)',...
         'Averaging: default number of points in azimutal average (I vs. q))',...
         'Averaging: default number of points in radial average(I vs. phi))',...
         'Fuji ImagePlate: Flip Fuji image',...
         'Fuji ImagePlate: Number of times to rotate Fuji image by 90 degrees'};
 default_ans= {num2str(water_abs_int), num2str(glassy_carbon_abs_int), num2str(water_attenuation_length),...
            num2str(cmask_radius), num2str(smooth_size), num2str(smooth_std), num2str(fatpix_fontsize), ...
            num2str(zinger_params.smoothingsize), num2str(zinger_params.threshold),...
            num2str(zinger_params.zinger_masksize),num2str(averagex_prefs.aznumpoints),...
            num2str(averagex_prefs.monumpoints),num2str(fujiimgflip),num2str(fujiimgrot90) };
answer=inputdlg(prompts,name,numlines,default_ans);
if ~isempty(answer)
    water_abs_int=str2num(answer{1});
    glassy_carbon_abs_int=str2num(answer{2});
    water_attenuation_length=str2num(answer{3});
    cmask_radius=str2num(answer{4});
    smooth_size=str2num(answer{5});
    smooth_std=str2num(answer{6});
    fatpix_fontsize=str2num(answer{7});
    zinger_params.smoothingsize=str2num(answer{8});
    zinger_params.threshold=str2num(answer{9});
    zinger_params.zinger_masksize=str2num(answer{10});
    averagex_prefs.aznumpoints=str2num(answer{11});
    averagex_prefs.monumpoints=str2num(answer{12});
    fujiimgflip=str2num(answer{13});
    fujiimgrot90=str2num(answer{14});
    saxs_preferences_write
else
    errordlg('Enter all parameters please, New preferences not saved')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuFileSet_Default_Saxsgui_Preferences(menu,gui)

sp_file=which('saxs_preferences.txt');
sp_def_file=which('saxs_preferences_default.txt');
copyfile(sp_def_file,sp_file); 
rehash toolboxcache

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewLog(menu, gui)
% Callback for View menu Log Intensity command.
togglemenu(menu)
gui = images(gui);  % draw images
guidata(gui.figure, gui)  % save data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewGrid(menu, gui)
% Callback for View menu Grid command.
togglemenu(menu)
state = get(menu, 'Checked');
ax = [gui.xy_axes, gui.rth_axes];
set(ax, 'XGrid', state, 'YGrid', state)
% Mark the prominent angles in the polar image.
%%% This repercusses when we open a new image or redraw images.
%%% Maybe some other time...
% if isequal(state, 'on')
%     gui.ygridspecial = hlines(gui.rth_axes, [90 180 270], 'yellow', ':');
% else
%     delete(gui.ygridspecial)
% end
% guidata(gui.figure, gui)  % save data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function hout = hlines(hax, posn, color, style)
% % Plot one or more horizontal lines
% % Return handles to the line objects.
% % NB: No argument checking here.
% Y = posn(:)';  % make sure it's a row vector
% Y = [Y; Y];  % endpoints
% xlim = get(hax, 'XLim');  % y limits
% X = repmat(xlim', 1, length(posn));
% h = line(X, Y, 'Color', color, 'LineStyle', style, 'Parent', hax);
% if nargout
%     hout = h;
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewCrosshairs(menu, gui)
togglemenu(menu)
if ischecked(menu)
    set(gui.figure, 'Pointer', 'fullcrosshair')
    trackpointer('on', gui)
else
    set(gui.figure, 'Pointer', 'default')
    trackpointer('off', gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewProjectionAlongLine(menu, gui)
persistent linehandle linecoords linewidth
if isempty(linehandle) || ~ishandle(linehandle)
    linecoords=[sum(gui.img.xy.xaxis)/2 sum(gui.img.xy.yaxis)/2 ;(gui.img.xy.xaxis(2)-sum(gui.img.xy.xaxis)/2)/2 (gui.img.xy.yaxis(2)-sum(gui.img.xy.yaxis)/2)/2];
    linewidth=1;
    linehandle=ProjectionLineDraw(gui,linecoords,linewidth);   
end

togglemenu(menu)
if ischecked(menu)
    %undisplay line
    set(linehandle,'Visible','on')
    %make line edittable
    editableline(linehandle)
    update_projectionline
else
    %undisplay line
    set(linehandle,'Visible','off')
    %make line uneditable
    editableline(linehandle,'off')
end

function remove_projection_line
global linehandle
%erase projection line
delete(linehandle)
linehandle=[];
menu=findobj(get(1,'Children'),'Tag','MenuViewProjectionAlongLine');
if ischecked(menu), togglemenu(menu); end


function handle=ProjectionLineDraw(gui,linecoords,linewidth)
x=linecoords(:,1);
y=linecoords(:,2);
hold(gui.xy_axes)
axis manual
handle=plot(gui.xy_axes,x,y,'r-','linewidth',linewidth);
hold(gui.xy_axes)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewZoom(menu, gui)
% Callback for View menu Zoom command.
togglemenu(menu)
state = get(menu, 'Checked');
zoom(gui.figure, state)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewColormap(menu, gui)
maps = {'jet', 'hsv', 'gray', 'bone', 'pink', 'copper', ...
        'hot', 'cool', 'spring', 'summer', 'autumn', 'winter', 'prism', 'colorcube'};
[imap ok] = listdlg('ListString', maps, 'SelectionMode', 'single', ...
    'name', 'Color Map');
if ok
    colormap(maps{imap})
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewFatpix(gui)
if ischecked(gui.MenuViewLog)
    errordlg('Fatpix works only on linear intensity images.  Turn log intensity off.', ...
        'fatpix', 'modal')
    return
end
im = findobj(gui.xy_axes, 'Type', 'image');
fatpix(im)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewHistory(gui)
height = size(gui.img.xy.history, 1) * 2;
width = size(gui.img.xy.history, 2);
fig = figure('MenuBar', 'none', 'Units', 'characters', ...
    'Position', [10, 20, width, height], ...
    'NumberTitle', 'off', 'Name', 'image history');
uicontrol(fig, 'Style', 'edit',  'Max', 2, 'Min', 0, ...
    'Units', 'normalized','Position', [0 0 1 1], ...
    'HorizontalAlignment', 'left', ...
    'String', gui.img.xy.history)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuViewMATLAB(menu, gui)
global separator  % between our menus and MATLAB menus
togglemenu(menu)
if ischecked(menu)
    separator = uimenu('Label', '|', 'Enable', 'off');
    set(gui.figure, 'MenuBar', 'figure')
else
    set(gui.figure, 'MenuBar', 'none')
    delete(separator)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAlterRemoveBadPixels(gui)
global zinger_params
if ~isempty(gui.img.xy.reduction_state)
      helpdlg([{'SAXSGUI cannot presently perform bad pixel removal on '},...
          {'2D reduced data, please clear the edit parameters before'},...
          {'before removing bad pixels.'}],'Bad Pixel removal on 2D reduced data')
      return
end
if strcmp(get(gui.MenuViewZoom,'Checked'),'on') %rurning off zoom if activated
    CenterClickChanged=1;
    MenuViewZoom(gui.MenuViewZoom,gui)
else
    CenterClickChanged=0;
end

zinger_params_old= zinger_params;
% We use the zinger removal program.
% Parameteters of 2, 10, 1 
% 
zinger_params.smoothingsize=3;
zinger_params.threshold=5;
zinger_params.zinger_masksize=1;

tmpmask=gui.img.xy.pixels*0+1;
status_bar = status('Finding bad pixels...', gui);

pixels = zinger_removal(gui.img.xy.pixels,tmpmask);
zinger_params=zinger_params_old;

gui.img.xy=insert_saxs_pixels(gui.img.xy,pixels);
gui.img.xy=insert_raw_saxs_pixels(gui.img.xy,gui.img.xy.pixels);
delete(status_bar)
if ~ isempty(gui.img.rth)
    gui = makepolar(gui);  % reproduce polar image
end
init_colorscale(gui)
gui = images(gui);  % draw images
guidata(gui.figure, gui)
if CenterClickChanged==1 %rurning on zoom if it was deactivated
    MenuViewZoom(gui.MenuViewZoom,gui)
end

% we could ask for maximum number of bad pixels but we won't we'll
% just assume that it is 50 at most. The criteria will be that a bad pixel
% will have an intensity that is more than  20 times the
% surrounding 100th pixel intensity AND that is more than 1000 times the
% average of its neighboring pixels



% sortedpixels=sort(gui.img.xy.pixels(~isnan(gui.img.xy.pixels)),'descend');
% topsortedpixels=sortedpixels(1:100);
% status_bar = status('Finding bad pixels...', gui);
% % now establish lists of potentially bad pixels
% % out of the 50 highest intensities
% 
% [ybadpix,xbadpix]=find(gui.img.xy.pixels>topsortedpixels(50));
% for ii=1:length(ybadpix)
%     local_average(ii)=...
%         (gui.img.xy.pixels(ybadpix(ii),min(end,xbadpix(ii)+1))+...
%         gui.img.xy.pixels(ybadpix(ii),max(1,xbadpix(ii)-1))+...
%         gui.img.xy.pixels(min(ybadpix(ii)+1,end),xbadpix(ii))+...
%         gui.img.xy.pixels(max(1,ybadpix(ii)+1),xbadpix(ii)))/4;
%     
%     if gui.img.xy.pixels(ybadpix(ii),xbadpix(ii))>(100*local_average(ii)) && ...
%             gui.img.xy.pixels(ybadpix(ii),xbadpix(ii))>(10*topsortedpixels(100))
%         disp(['Found bad pixel ',num2str(xbadpix(ii)),' ',num2str(ybadpix(ii))])
%         gui.img.xy=insert_saxs_pixelval(gui.img.xy,ybadpix(ii),xbadpix(ii),local_average(ii));
%     end
% end
% gui.img.xy=insert_raw_saxs_pixels(gui.img.xy,gui.img.xy.pixels);
% 
% delete(status_bar)
% if ~ isempty(gui.img.rth)
%     gui = makepolar(gui);  % reproduce polar image
% end
% init_colorscale(gui)
% gui = images(gui);  % draw images
% guidata(gui.figure, gui)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAlterZingerRemoval(gui)
global   cmask_radius
if ~isempty(gui.img.xy.reduction_state)
      helpdlg([{'SAXSGUI cannot presently perform zinger removal on '},...
          {'2D reduced data, please clear the edit parameters before'},...
          {'before attempting to remove.'}],'Zinger removal on 2D reduced data')
      return
end
if strcmp(get(gui.MenuViewZoom,'Checked'),'on') %rurning off zoom if activated
    CenterClickChanged=1;
    MenuViewZoom(gui.MenuViewZoom,gui)
else
    CenterClickChanged=0;
end
% We use the unzing program.
% Parameteters of 9, 10, 3 
% 
tmpmask=gui.img.xy.pixels*0+1;
status_bar = status('Finding zingers...', gui);

pixels = zinger_removal(gui.img.xy.pixels,tmpmask);
%if we have a center don't change those that are within
%maskradius of the center
if ~ isempty(gui.img.rth) %means a center has been selected
    p1=max(1,floor(gui.img.xy.raw_center(2)-cmask_radius));
    p2=min(floor(gui.img.xy.raw_center(2)+cmask_radius),size(gui.img.xy.pixels,1));
    p3=max(1,floor(gui.img.xy.raw_center(1)-cmask_radius));
    p4=min(floor(gui.img.xy.raw_center(1)+cmask_radius),size(gui.img.xy.pixels,2));
    pixels(p1:p2,p3:p4)=gui.img.xy.pixels(p1:p2,p3:p4);
end
gui.img.xy=insert_saxs_pixels(gui.img.xy,pixels);
gui.img.xy=insert_raw_saxs_pixels(gui.img.xy,gui.img.xy.pixels);
delete(status_bar)
if ~ isempty(gui.img.rth)
    gui = makepolar(gui);  % reproduce polar image
end
init_colorscale(gui)
gui = images(gui);  % draw images
guidata(gui.figure, gui)
if CenterClickChanged==1 %rurning on zoom if it was deactivated
    MenuViewZoom(gui.MenuViewZoom,gui)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MenuAlterList(gui)
global time_begin time_end listfigid
global liststruct

remove_figstatus % this line deletes all the yellow "statuslines"

if ~strcmp(gui.img.xy.datatype,'lst')
      helpdlg([{'The loaded image must come from an mpa-lst file '}])
      return
end
% we do not read the file any longer since it has already been read once
%a=mpalistread(gui.img.xy.raw_filename);

if ~ isempty(findobj(get(0,'children'), 'Tag', 'list mode window'))
    listfigid=findobj(get(0,'children'), 'Tag', 'list mode window');
    figure(listfigid)
else
    listfigid=figure;
    set(listfigid,'Tag','list mode window')
end
plot([0:length(liststruct.rate)-1]*liststruct.timerreduce/1000,liststruct.rate')
drawnow

param = inputdlg({'Starting time (seconds):',...
    'End time (seconds):'}, 'Input for list subsection', ...
    1, {num2str(max(liststruct.timestart,time_begin)), num2str(min(time_end,liststruct.timeend))});
if ~ isempty(param)  % dialog not cancelled
    % Hang onto these values for next time.
    time_begin = max(liststruct.timestart,str2double(param{1}));
    time_end = min(liststruct.timeend,str2double(param{2}));
end

img.xy=list2saxs(liststruct,time_begin,time_end);
[PATHSTR,NAME,EXT] = fileparts(img.xy.raw_filename);
img.name=[NAME,EXT,'[',num2str(time_begin),':',num2str(time_end),']'];
img.rth=[];
if ~ isempty(img.xy)  % we have an image structure
    % Guess if image has been centered.
    if ~ any(isnan(pixelfrac(img.xy, [0 0])))
        statusline = status('Computing image in polar coordinates...', gui);
        img.rth = polar(img.xy);
        delete(statusline)
    end
    gui = newimage(img, gui);
    guidata(gui.figure, gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuSmoothGaussian(gui)
global smooth_size smooth_std
if ~isempty(gui.img.xy.reduction_state)
      helpdlg([{'SAXSGUI cannot presently perform smoothing on '},...
          {'2D reduced data, please clear the edit parameters before'},...
          {'before smoothing.'},...
          {'It is, however, possible to reduce smoothed data'}],'Smoothing on 2D reduced data')
      return
end
if isempty(smooth_size)
    saxs_preferences_read  % get smooth_size, smooth_std
    initialized = 1;
end

param = inputdlg({'Size of Convolution Box (pixels):',...
    'Width of Smoothing Function (st.dev in pixels):'}, 'Gaussian impulse filter', ...
    1, {num2str(smooth_size), num2str(smooth_std)});
if ~ isempty(param)  % dialog not cancelled
    % Hang onto these values for next time.
    smooth_size = str2double(param{1});
    smooth_std = str2double(param{2});
    if isfield(gui.img, 'xy_unsmoothed')
        gui.img.xy = gui.img.xy_unsmoothed;  % start from unsmoothed image
    else
        gui.img.xy_unsmoothed = gui.img.xy;  % save original for undo
    end
    status_bar = status('Smoothing image...', gui);
    gui.img.xy = smooth(gui.img.xy, smooth_size, smooth_std);
    gui.img.xy_prereduction_pixels=gui.img.xy.pixels;
    
    delete(status_bar)
    if ~ isempty(gui.img.rth)
        gui = makepolar(gui);  % reproduce polar image
    end
    gui = images(gui);  % draw images
    guidata(gui.figure, gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuSmoothUndo(gui)
if isfield(gui.img, 'xy_unsmoothed')
    if ~isempty(gui.img.xy.reduction_state)
      helpdlg([{'2D reductions will also be undone'}],'Undo Smoothing Note')
    end
    gui.img.xy= insert_saxs_pixels(gui.img.xy,gui.img.xy_unsmoothed.pixels);
    gui.img.xy=clear_reduction_parameters(gui.img.xy);
    gui.img.xy_prereduction_pixels=gui.img.xy.pixels;
    gui.img = rmfield(gui.img, 'xy_unsmoothed');
    %delete(findobj(gui.figure, 'Type', 'image','Tag',''))  % delete all our images
    init_colorscale(gui)
    %try, pack, catch, end  % consolidate memory blocks if we can
    if ~ isempty(gui.img.rth)
        gui = makepolar(gui);  % reproduce polar image
    end
    gui = images(gui);  % draw images
    guidata(gui.figure, gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuBkgSubtract(gui)
% Get a background image from a file and interactively subtract from our
% image (including a saved unsmoothed image).  Recompute polar image if
% necessary.
bkg = getsaxs('', 'Open background image file');
if ~ isempty(bkg)
    if isfield(gui, 'kim') && isfield(gui, 'kbkg')
        [diff gui.kim gui.kbkg] = isubtract(gui.img.xy, bkg, gui.kim, gui.kbkg);
    else
        [diff gui.kim gui.kbkg] = isubtract(gui.img.xy, bkg);
    end
    if ~ isempty(diff)
        gui.img.xy = diff;
        if isfield(gui.img, 'xy_unsmoothed')  % subtract from unsmoothed too
            gui.img.xy_unsmoothed = gui.img.xy_unsmoothed * gui.kim ...
                - bkg * gui.kbkg;
        end
        if ~ isempty(gui.img.rth)
            gui = makepolar(gui);  % reproduce polar image
        end
        gui = images(gui);
        guidata(gui.figure, gui)
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuBkgFlood(gui)
flood = getsaxs('', 'Open flood field image');
if ~ isempty(flood)
    fig = figure('MenuBar', 'none', 'Name', 'Flood field image', ...
        'NumberTitle', 'off', 'WindowStyle', 'modal');
    ax = axes('Parent', fig, ...
        'Units', 'normalized', 'Position', [0.1 0.2 0.8 0.7]);
    image(flood, 'Parent', ax)
    strApply = 'Apply flood field';
    uicontrol('Parent', fig, 'Style', 'pushbutton', ...
        'Units', 'characters', ...
        'Position', [2, 1, length(strApply) + 4, 2], ...
        'String', strApply, 'Callback', {@applyflood, flood, gui})
    % NOTE:  APPLYFLOOD may change GUI structure values and save them to
    % the image.
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function applyflood(button, event, flood, gui)
% NOTE:  APPLYFLOOD may change GUI structure values and save them to the
% image.
flood = normalize(flood);
gui.img.xy = gui.img.xy ./ flood;
if isfield(gui.img, 'xy_unsmoothed')
    gui.img.xy_unsmoothed = gui.img.xy_unsmoothed ./ flood;
end
if ~ isempty(gui.img.rth)
    gui = makepolar(gui);
end
close(get(button, 'Parent'))  % close flood field window
gui = images(gui);
guidata(gui.figure, gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function im = normalize(im0)
im = im0;
maxflatfield=ffvalue(im);%ffvalue establishes the pixelvalue in the image that should be normalized to 1
% Avoid dividing by zero.
im(im.pixels == 0) = 1;  % assuming we're dealing with integer counts
im = history(im, 'Replaced 0s with 1s.');
% Normalize according to definition in ffvalue
im = im ./maxflatfield; 
im = history(im, 'Normalized flatfield');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuMaskSameAsLast(gui)
global oldmask
if isempty(gui.img.rth)
    uiwait(msgbox('Choose Center first before loading a mask'))
    return
end
if ~isempty(oldmask)
    gui.img.rth=assignmask(gui.img.rth,oldmask.mask_pixels,oldmask.mask_filename,1);
    guidata(gui.figure, gui)% save data
else
    uiwait(msgbox('No Mask File Loaded','Error','modal'))
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuMaskLoadExisting(gui)
global oldmask
if isempty(gui.img.rth)
    uiwait(msgbox('Choose Center first before loading a mask'))
    return
end
[filename, pathname] = uigetfile('*.mat', 'Pick a mask-file');
fullfilename=fullfile(pathname,filename);
if exist(fullfilename)
    statusline= status('Loading mask file...',gui);
    a=load(fullfilename); %mask file contains 1's and 0's
    if ~isfield(a,'mask')
        errordlg(['mask file:', fullfilename,' does not contain variable "mask". Please make a new mask file'])
        delete(statusline)
        return
    end
    if ~(size(a.mask)==size(gui.img.xy.raw_pixels)  )
        errordlg(['The size of the mask is different from the size of the image. Please choose a different file'])
        delete(statusline)
        return
    end
    if ~islogical(a.mask)
        errordlg(['mask is not a logical file (0''s and 1''s). Make new mask file '])
        delete(statusline)
        return
    end
else
    errordlg(['mask file:', fullfilename,' does not exist. Please correct'])
    return
end
warning off MATLAB:divideByZero
gui.img.rth=assignmask(gui.img.rth,double(a.mask)./double(a.mask),fullfilename,1);
oldmask.mask_pixels=gui.img.rth.mask_pixels;
oldmask.mask_filename=gui.img.rth.mask_filename;
%gui.img.xy=assignmask(gui.img.xy,double(a.mask)./double(a.mask),fullfilename,1)
warning on MATLAB:divideByZero
guidata(gui.figure, gui)% save data
delete(statusline)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuMaskCreate(gui)

maskmakergui(gui.img.xy.pixels)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCenterClick(menu, gui)
global oldc
if strcmp(get(gui.MenuViewZoom,'Checked'),'on') %rurning off zoom if activated
    CenterClickChanged=1;
    MenuViewZoom(gui.MenuViewZoom,gui)
else
    CenterClickChanged=0;
end
gui.img.xy = center(gui.img.xy, pixelclick(gui.xy_axes));
gui = update_unsmoothed(gui);
gui = makepolar(gui);
gui = images(gui);
guidata(gui.figure, gui)% save data
oldc = pixelfrac(gui.img.xy, [0 0]);
if CenterClickChanged==1 %rurning on zoom if it was deactivated
    MenuViewZoom(gui.MenuViewZoom,gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCenterMask(menu, gui)
% Callback for Center menu Mask command.
oldcmask = gui.img.xy.cmask_radius;
newcmaskstr = inputdlg('Center Mask radius:', 'CMask', 1, {num2str(oldcmask)});
if isempty(newcmaskstr)  % user cancelled the dialog
    return  % do nothing
end
newcmask = str2double(newcmaskstr);
if isnan(newcmask)  % not a number
    errordlg(['You must enter a number for center mask radius.  The center mask radius is still ', ...
            num2str(oldcmask), '.'], 'Invalid center mask radius', 'modal')
else  % ok
    gui.img.xy = cmask(gui.img.xy, newcmask);
    if ~ isempty(gui.img.rth)
        gui.img.rth = cmask(gui.img.rth, newcmask);  % necessary?  always regenerated?
    end
    guidata(gui.figure, gui)  % save new mask values
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCenterCompute(menu, gui)
global oldc
% Callback for Center menu Compute command
hstatus = status('Computing symmetric center...', gui);
gui.img.xy = center(gui.img.xy, 'nostatus');
gui = update_unsmoothed(gui);
delete(hstatus)
gui = makepolar(gui);
gui = images(gui);
guidata(gui.figure, gui)  % save data
oldc = pixelfrac(gui.img.xy, [0 0]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MenuCenterComputeByRing(menu, gui)
global oldc

clicks=zeros(3:2);

% first bring back to uncalibrated
gui = update_unsmoothed(gui);

guidata(gui.figure, gui);
% bring focus back to xy
set(get(gui.xy_axes, 'Title'), 'String', gui.img.name, 'Interpreter', 'none')

%then ask for user input
uiwait(helpdlg('Click on 3 approx. locations of centering ring. The program will calculate more accurate positions, and then calculate the center','Centering by a full ring'))
statusbar = status('Click on 1st point ...', gui);
clicks=zeros(3,2);
clicks(1,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on 2nd point ...', gui);
clicks(2,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on last point ...', gui);
clicks(3,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
clicks=pixel(gui.img.xy,clicks(:,:));
[pixelcal,pixelcen] = findkcal(gui.img.xy,clicks);
if ~isempty(pixelcen)
    gui.img.xy = setcenter(gui.img.xy,pixelcen);
    gui = update_unsmoothed(gui);
    gui = makepolar(gui);
    gui = images(gui);% draw images
    guidata(gui.figure, gui);
    oldc=pixelcen;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MenuCenterComputeByPartialRing(menu, gui)
global oldc

clicks=zeros(3:2);

% first bring back to uncalibrated
gui = update_unsmoothed(gui);

guidata(gui.figure, gui);
% bring focus back to xy
set(get(gui.xy_axes, 'Title'), 'String', gui.img.name, 'Interpreter', 'none')

%then ask for user input
uiwait(helpdlg('Click on 3 approx. locations of partial centering ring. The program will calculate more accurate positions','Centering by a Partial ring'))
statusbar = status('Click on 1st point ...', gui);
clicks=zeros(3,2);
clicks(1,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on 2nd point ...', gui);
clicks(2,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on last point ...', gui);
clicks(3,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
clicks=pixel(gui.img.xy,clicks(:,:));

[pixelcal,pixelcen] = findcenterbypartialring(gui.img.xy,clicks);
if ~isempty(pixelcen)
    gui.img.xy = setcenter(gui.img.xy,pixelcen);
    gui = update_unsmoothed(gui);
    gui = makepolar(gui);
    gui = images(gui);% draw images
    guidata(gui.figure, gui);
    oldc=pixelcen;
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCenterSave(gui)
[name, path] = uiputfile('*.mat', 'Save center coordinates');
if name
    savecenter(gui.img.xy, [path, name])
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCenterLoad(gui)
global oldc
[name, path] = uigetfile('*.mat', 'Load center coordinates');
if name
    gui.img.xy = loadcenter(gui.img.xy, [path, name]);
    gui = update_unsmoothed(gui);
    gui = makepolar(gui);
    gui = images(gui);
    guidata(gui.figure, gui)
    oldc = pixelfrac(gui.img.xy, [0 0]);
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCenterCoordinates(gui)
global oldc

% this little addition ensures that the last entered center is always
% displayed and that if this is the first time then the center of image is
% displayed
presentc = pixelfrac(gui.img.xy, [0 0]);
if isnan(presentc) 
    if isempty(oldc)
        dispc=size(gui.img.xy.pixels)/2;
    else
        dispc=oldc;
    end
else
    dispc=presentc;
end   
dispc = round(100 .* dispc) ./ 100;  % round to hundredth of pixel
%
newcstr = inputdlg({'x:', 'y:'}, 'Image center in raw pixel coordinates', 1, ...
    {num2str(dispc(1)), num2str(dispc(2))});
if ~ isempty(newcstr)  % user didn't cancel
    newc = str2double(newcstr)';
    if max(abs(newc-presentc))>0.011 || max(isnan(presentc)) % user changed values
        gui.img.xy = center(gui.img.xy, newc);
        gui = update_unsmoothed(gui);
        gui = makepolar(gui);
        gui = images(gui);
        guidata(gui.figure, gui)  % save data
    end
    oldc=newc;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui=MenuCenterSame(gui)
global oldc

% this is adapted from MenuCenterCoordinates 
% displayed
presentc = pixelfrac(gui.img.xy, [0 0]);
if isnan(presentc) 
    if isempty(oldc)
        dispc=size(gui.img.xy.pixels)/2;
        oldc=dispc;
    else
        dispc=oldc;
    end
else
    dispc=presentc;
end   

if norm(oldc-presentc)>0.011 || isequal([1 1],isnan(presentc)) % user changed values
    gui.img.xy = center(gui.img.xy, oldc);
    gui = update_unsmoothed(gui);
    gui = makepolar(gui);
    gui = images(gui);
    guidata(gui.figure, gui)  % save data
end
oldc=dispc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCenterDirectBeam(menu, gui)
%Pearson VII function for determining the peak center position in an empty beam
%measuremnt. Contributed by Brian Pauw

hstatus=status('Fitting a 2D peak to the center',gui);
[maxxval,maxxx]=max(sum(gui.img.xy.pixels,1));%the sum ensures a better center guess
[maxyval,maxxy]=max(sum(gui.img.xy.pixels,2));

maxave=(max(max(gui.img.xy.pixels)));
fwhm_1=sum(min(sum(gui.img.xy.pixels>maxave/2,1),1));%a line with 1's to determine length of pixels
fwhm_2=sum(min(sum(gui.img.xy.pixels>maxave/2,2),1));%a line with 1's to determine length of pixels
fwhmave=(fwhm_1+fwhm_2)/2;

ikx=1:size(gui.img.xy.pixels,1);
ikx=repmat(ikx,size(gui.img.xy.pixels,2),1);
iky=1:size(gui.img.xy.pixels,1);
iky=repmat(iky,size(gui.img.xy.pixels,1),1);
iky=permute(iky,[2 1]);

x0=[maxave fwhmave 1 maxxx maxxy];
xl=[maxave/10 fwhmave/10 0 maxxx-50 maxxy-50]; %min 50 pixels off
xu=[maxave*10 fwhmave*10 Inf maxxx+50 maxxy+50]; %max 50 pixels off

meritfunc=@(x) chisqr_peakxy(gui.img.xy.pixels(maxxx-100:maxxx+100,maxxy-100:maxxy+100),ikx(maxxx-100:maxxx+100,maxxy-100:maxxy+100),iky(maxxx-100:maxxx+100,maxxy-100:maxxy+100),x) ; %MOD smears added
[Fitparam,Fitval,exitflag,output]=...
    fminsearchbnd(meritfunc,x0,xl,xu,optimset('MaxFunEvals',1000,'MaxIter',1000,'Tolx',1e-2));

%check for bad fit here!
delete(hstatus)
%and center the plot.
gui.img.xy = center(gui.img.xy, [Fitparam(4) Fitparam(5)]);
gui = update_unsmoothed(gui);
gui = makepolar(gui);
gui = images(gui);
guidata(gui.figure, gui)  % save data

clear Fitparam Fitval Exitflag output x0 x1 xu ikx iky

function cs=chisqr_peakxy(I,ikx,iky,fitpar)
%for use in the aforementioned MenuCenterFile computation

Amp=fitpar(1);
fwhm=fitpar(2);
M=fitpar(3);
cx=fitpar(4);
cy=fitpar(5);

Icalc=Amp./(1+4.*(((ikx-cx).^2+(iky-cy).^2)./fwhm.^2).^2.*(2.^(1./M)-1)).^(M); %a pearson VII function with x^2+y^2


cs=sqrt(sum(sum((Icalc-I).^2))/(length(I)-length(fitpar)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = update_unsmoothed(gui_in)
gui = gui_in;
if isfield(gui.img, 'xy_unsmoothed')
    if ~isnan(centerof(gui.img.xy))
        gui.img.xy_unsmoothed = center(gui.img.xy_unsmoothed, centerof(gui.img.xy));
    end
    gui.img.xy_unsmoothed = calibrate(gui.img.xy_unsmoothed, ...
        gui.img.xy.kcal, gui.img.xy.pixelcal, gui.img.xy.koffset,...
        gui.img.xy.wavelength, gui.img.xy.pixelsize, gui.img.xy.detector_dist, gui.img.xy.calibrationtype); 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = makepolar(gui_in)
gui = gui_in;
hstatus = status('Transforming to polar image...', gui);
gui.img.rth = polar(gui.img.xy);
gui.img.xy=raw_center(gui.img.xy,gui.img.rth.raw_center);
delete(hstatus)
guidata(gui.figure, gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuRotateCompute(gui)
statusbar = status('Computing rotation...', gui);
rot = findrot(gui.img.rth);
delete(statusbar)
msgbox(['Rotation:  ', num2str(rot), ' degrees'])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuRotateSpecify(gui)
if isempty(gui.img.rth)
    msgbox(['Please define a center of the image first'])
    return
end
rotby = inputdlgnum('Enter degrees by which to rotate the image. Positive is counter-clockwise', 'Rotation');
if rotby  % some number other than zero
    statusbar = status('Rotating...', gui);
    gui.img.xy = rotatexy(gui.img.xy, rotby);
    gui.img.rth = rotaterth(gui.img.rth, rotby);
    % Don't forget to rotate the unsmoothed image!
    if isfield(gui.img, 'xy_unsmoothed')
        set(statusbar, 'String', 'Rotating unsmoothed image...')
        gui.img.xy_unsmoothed = rotatexy(gui.img.xy_unsmoothed, rotby);
    end
    delete(statusbar)
    gui = images(gui);  % redraw images
    guidata(gui.figure, gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function num = inputdlgnum(msg, title)
% Obtain a number from the user via an input dialog box.
% Arguments are the prompt message and an optional title.
% Always return a number.  If the user's input cannot be converted to a
% number, return 0.  Return 0 even if the user cancels the dialog.
if nargin < 2
    title = '';
end
answer = inputdlg(msg, title);  % answer is a cell array
if isempty(answer)
    num = 0;
else
    num = str2double(answer{1});
    if isnan(num)
        num = 0;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAutoKCalibrate_AgBeh(gui)
global k pixelcal koffset wavelength pixelsize detector_dist calib_type
global wavelength_default

AgBeh=zeros(3:2);
% asking to calibrate AgBeh 1st order
k=0.1076; %q-value of first order AgBeh reflection in 1/A

% first bring back to uncalibrated
gui.img.xy = uncalibrate(gui.img.xy);
gui = update_unsmoothed(gui);
% if ~ isempty(gui.img.rth)
%     gui = makepolar(gui);
% end
gui = images(gui);  % draw images
guidata(gui.figure, gui)
% bring focus back to xy
set(get(gui.xy_axes, 'Title'), 'String', gui.img.name, 'Interpreter', 'none')

%then ask for user input
uiwait(helpdlg('Click on 3 approx. locations on Silver Behenates first order ring. The program will calculate more accurate positions','Auto Calibrating with Silver Behenate'))
statusbar = status('Click on 1st point ...', gui);
AgBehP=zeros(3,2);
AgBehP(1,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on 2nd point ...', gui);
AgBehP(2,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on last point ...', gui);
AgBehP(3,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
AgBehP=pixel(gui.img.xy,AgBehP(:,:));
pixelcal = findkcal(gui.img.xy,AgBehP);
% also need to enter the wavelength
prompt={'Enter the Wavelength A:'};
name='Input for Wavelength';
numlines=1;
if isempty(wavelength)
    defaultanswer={num2str(wavelength_default)};
else
    defaultanswer={num2str(wavelength)};
end

answer=inputdlg(prompt,name,numlines,defaultanswer);
if ~isempty(answer)
    if ~isempty(str2num(answer{1}))
        wavelength=str2num(answer{1});
    end
end

koffset=0;

if ~ (isempty(k) || isempty(pixelcal) || isempty(koffset))
    gui.img.xy = calibrate(gui.img.xy, k, pixelcal, koffset, wavelength, [], [],'std');
    gui = update_unsmoothed(gui);
    if ~ isempty(gui.img.rth)
        gui = makepolar(gui);
    end
    gui = images(gui);% draw images
end
guidata(gui.figure, gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MenuAutoKCalibrate_AgBeh_ByPartialRing(gui)
global k pixelcal koffset wavelength pixelsize detector_dist calib_type
global wavelength_default
global oldc

clicks=zeros(3:2);
% asking to calibrate AgBeh 1st order
k=0.1076; %q-value of first order AgBeh reflection in 1/A

% first bring back to uncalibrated
gui = update_unsmoothed(gui);

guidata(gui.figure, gui);
% bring focus back to xy
set(get(gui.xy_axes, 'Title'), 'String', gui.img.name, 'Interpreter', 'none')

%then ask for user input
uiwait(helpdlg('Click on 3 approx. locations of partial calibration ring. The program will calculate more accurate ring positions','Calibrating by a partial ring'))
statusbar = status('Click on 1st point ...', gui);
clicks=zeros(3,2);
clicks(1,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on 2nd point ...', gui);
clicks(2,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
statusbar = status('Click on last point ...', gui);
clicks(3,:)=ginput(1); % should do some error checking on the input...for later
delete(statusbar)
clicks=pixel(gui.img.xy,clicks(:,:));

[pixelcal,pixelcen] = findcenterbypartialring(gui.img.xy,clicks);

% also need to enter the wavelength
prompt={'Enter the Wavelength A:'};
name='Input for Wavelength';
numlines=1;
if isempty(wavelength)
    defaultanswer={num2str(wavelength_default)};
else
    defaultanswer={num2str(wavelength)};
end

answer=inputdlg(prompt,name,numlines,defaultanswer);
if ~isempty(answer)
    if ~isempty(str2num(answer{1}))
        wavelength=str2num(answer{1});
    end
end

koffset=0;

if ~ (isempty(k) || isempty(pixelcal) || isempty(koffset))
    gui.img.xy = calibrate(gui.img.xy, k, pixelcal, koffset, wavelength, [], [],'std');
    gui = update_unsmoothed(gui);
    if ~ isempty(gui.img.rth)
        gui = makepolar(gui);
    end
    gui = images(gui);% draw images
end
guidata(gui.figure, gui)




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuKMysaCalibrator(gui)
% The Mysa Calibrator was contributed by Milos Steinhart, from the 
% Macromolecular Institute in Prague, The Czech republic. 
% It is especially useful for calibration where AgBehanate cannot be used
global k pixelcal koffset wavelength pixelsize detector_dist calib_type
global wavelength_default
global mysaS
if isempty(koffset)
    if isempty(gui.img.xy.kcal)
        koffset = 0;  % default value (others empty)
    else  % get calibration data from image
        k = gui.img.xy.kcal;
        pixelcal = gui.img.xy.pixelcal;
        koffset = gui.img.xy.koffset;
        wavelength = gui.img.xy.wavelength;
        pixelsize = gui.img.xy.pixelsize;
        detector_dist = gui.img.xy.detector_dist;
    end
end
if isempty(wavelength), wavelength=wavelength_default; end
mysaS=gui.img.xy;
fig = onew10();

if (strcmp(calib_type,'std') && ~ (isempty(k) || isempty(pixelcal) || isempty(koffset) || isempty(wavelength) )) || ...
        (strcmp(calib_type,'geom') && ~(isempty(wavelength) || isempty(pixelsize) || isempty(detector_dist)))
    gui.img.xy = calibrate(gui.img.xy, k, pixelcal, koffset, wavelength, pixelsize, detector_dist,calib_type);
    gui = update_unsmoothed(gui);
    if ~ isempty(gui.img.rth)
        gui = makepolar(gui);
    end
    gui = images(gui);  % draw images
end
guidata(gui.figure, gui)
clear mysaS

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuKCalibrate(gui)
% Be careful.  kcal is an external function.  k is the local variable.
% June 3, 2006: Extended to also included calibration by geometry
global k pixelcal koffset wavelength pixelsize detector_dist calib_type
global wavelength_default

if isempty(koffset)
    if isempty(gui.img.xy.kcal)
        koffset = 0;  % default value (others empty)
    else  % get calibration data from image
        k = gui.img.xy.kcal;
        pixelcal = gui.img.xy.pixelcal;
        koffset = gui.img.xy.koffset;
        wavelength = gui.img.xy.wavelength;
        pixelsize = gui.img.xy.pixelsize;
        detector_dist = gui.img.xy.detector_dist;
    end
end
if isempty(wavelength), wavelength=wavelength_default; end
[k, pixelcal, koffset, wavelength, pixelsize, detector_dist, calib_type] = kcal(k, pixelcal, koffset, wavelength, pixelsize, detector_dist,calib_type);
if (strcmp(calib_type,'std') && ~ (isempty(k) || isempty(pixelcal) || isempty(koffset) || isempty(wavelength) )) || ...
        (strcmp(calib_type,'geom') && ~ (isempty(wavelength) || isempty(pixelsize) || isempty(detector_dist)))
    gui.img.xy = calibrate(gui.img.xy, k, pixelcal, koffset, wavelength, pixelsize, detector_dist,calib_type);
    gui = update_unsmoothed(gui);
    if ~ isempty(gui.img.rth)
        gui = makepolar(gui);
    end
    gui = images(gui);  % draw images
end
guidata(gui.figure, gui)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui=MenuKSame(gui)
% Be careful.  kcal is an external function.  k is the local variable.
global k pixelcal koffset wavelength pixelsize detector_dist calib_type

if isempty(koffset)
    if isempty(gui.img.xy.kcal)
        koffset = 0;  % default value (others empty)
    else  % get calibration data from image
        k = gui.img.xy.kcal;
        pixelcal = gui.img.xy.pixelcal;
        koffset = gui.img.xy.koffset;
    end
end
if ~ (isempty(k) || isempty(pixelcal) || isempty(koffset)) 
    gui.img.xy = calibrate(gui.img.xy, k, pixelcal, koffset, wavelength, pixelsize, detector_dist, calib_type);
    gui = update_unsmoothed(gui);
    if ~ isempty(gui.img.rth)
        gui = makepolar(gui);
    end
    gui = images(gui);  % draw images
end
guidata(gui.figure, gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuKUncalibrate(gui)
gui.img.xy = uncalibrate(gui.img.xy);
gui = update_unsmoothed(gui);
if ~ isempty(gui.img.rth)
    gui = makepolar(gui);
end
gui = images(gui);  % draw images
guidata(gui.figure, gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuKSave(gui)
global k pixelcal koffset wavelength pixelsize detector_dist calib_type

[name, path] = uiputfile('*.mat', 'Save q calibration');
if name
    if isempty(koffset)
        if isempty(gui.img.xy.kcal)
            k=[];
            pixelcal=[];
            koffset=[];
            wavelength=[];
            pixelsize=[];
            detector_dist=[];
            % default value (others empty)
        else  % get calibration data from image
            k = gui.img.xy.kcal;
            pixelcal = gui.img.xy.pixelcal;
            koffset = gui.img.xy.koffset;
            wavelength= gui.img.xy.wavelength;
            pixelsize=gui.img.xy.pixelsize;
            detector_dist=gui.img.xy.detector_dist;
            calib_type=gui.img.xy.calibrationtype;
        end
    end
    save([path,name],'k','pixelcal','koffset','wavelength','pixelsize','detector_dist','calib_type')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuKLoad(gui)
global k pixelcal koffset wavelength pixelsize detector_dist calib_type

[name, path] = uigetfile('*.mat', 'Load q calibration');
if name
    load([path name])
    if ~ (isempty(k) || isempty(pixelcal) || isempty(koffset))
        gui.img.xy = calibrate(gui.img.xy, k, pixelcal, koffset,wavelength,pixelsize,detector_dist,calib_type);
        gui = update_unsmoothed(gui);
        if ~ isempty(gui.img.rth)
            gui = makepolar(gui);
        end
        gui = images(gui);  % draw images
    end
guidata(gui.figure, gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuReductionParametersEdit(type,gui)
global old_saxs_obj
%type has been introduced as a flag for which type of reduction AvePix (type=1) or
%pix2pix (type=2)

if ~isempty(gui.img.rth)
    reduction_parameters(gui.img.rth)
    old_saxs_obj=gui.img.rth;
else
    errordlg('Please choose a center before editing reduction parameters')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuReductionParametersClear(gui)

if ~isempty(gui.img.rth)
    gui.img.rth=clear_reduction_parameters(gui.img.rth);
end
gui.img.xy=clear_reduction_parameters(gui.img.xy);

%if the "interactive reduction" is checked we must apply the clearing
%of the displayed data
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
    gui.img.xy=insert_saxs_pixels(gui.img.xy,gui.img.xy_prereduction_pixels);
end
% and send it back to the original window.
guidata(gui.figure, gui)
% since this does not automatically update the image, we also need to do
% that specifically from here.
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
    init_colorscale(gui)
    gui = makepolar(gui);
    images(gui);
    guidata(gui.figure, gui)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui=MenuReductionParametersSame(gui) 
global old_saxs_obj

if isempty(old_saxs_obj)
    errordlg('No "last" reduction parameters available')
    return
end
if ~isempty(gui.img.rth)
    gui.img.rth=copy_reduction_parameters(gui.img.rth,old_saxs_obj);
end
gui.img.xy=copy_reduction_parameters(gui.img.xy,old_saxs_obj);
%if the "interactive reduction" is checked we must apply the reductions
%of the displayed data
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
    gui.img.xy=pix2pix_red(gui.img.xy);
end
% and send it back to the original window.
guidata(gui.figure, gui)
% since this does not automatically update the image, we also need to do
% that specifically from here.
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
    init_colorscale(gui)
    gui = makepolar(gui);
    images(gui);
    guidata(gui.figure, gui)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuReduction1D(gui) 
global reduction_type

if isequal(get(gui.MenuReduction1D, 'Checked'), 'off')
    reduction_type='1D';
    set(gui.MenuReduction1D,'Checked','on')
    set(gui.MenuReduction2D,'Checked','off')
else
    reduction_type='1D';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuReduction2D(gui) 
global reduction_type

if isequal(get(gui.MenuReduction2D, 'Checked'), 'off')
    reduction_type='2D';
    set(gui.MenuReduction2D,'Checked','on')
    set(gui.MenuReduction1D,'Checked','off')
else
    reduction_type='2D';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAveraging(gui)

% June 1, 2006 added a check to see whether we had a polar plot

% if no polar plot has been made and therefore no center chosen return with
% error
remove_figstatus
if isempty(gui.img.rth) 
    errordlg('There is no center defined for this image. Use "center"-menu')
    return
end
remove_figstatus % this line deletes all the yellow "statuslines"
% if image is log then we communicate this to the averaging window by the
%parameter displog
if ischecked(gui.MenuViewLog)
    displog=1;
else 
    displog=0;
end
clim = get(gui.rth_axes, 'CLim');
ctop = clim(2);
cmap = get(gui.figure, 'ColorMap');
averagex(gui.img.rth, ctop, cmap, displog)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuImageAnalysis2D(gui)

% if no polar plot has been made and therefore no center chosen return with
% error
if isempty(gui.img.rth) 
    errordlg('There is no center defined for this image. Use "center"-menu')
    return
end
if isempty(gui.img.rth.pixelcal)
    errordlg('Fitting usually requires calibration of data  - please calibrate the data to proceed')
    return
end
% if image is log then we communicate this to the averaging window by the
%parameter displog
if ischecked(gui.MenuViewLog)
    displog=1;
else 
    displog=0;
end
clim = get(gui.rth_axes, 'CLim');
ctop = clim(2);
cmap = get(gui.figure, 'ColorMap');
Fitting2D(gui.img.rth,gui.img.xy, ctop, cmap,displog)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function checked = ischecked(menu)
% True if menu item is checked.
checked = isequal(get(menu, 'Checked'), 'on');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function togglemenu(menu)
switch get(menu, 'Checked');
    case 'on'
        checked = 'off';
    case 'off'
        checked = 'on';
end
set(menu, 'Checked', checked)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MenuQuikLast(menu,gui) 
% this callback function simply calls
% the 4 last processes
% last error calibration
% last calibration
% last centering 
% last MenuReductionParameter

if ~isempty(gui.img.xy)
    gui=MenuKSame(gui);
    gui=MenuCenterSame(gui);
    gui=MenuErrorCalibSame(gui);
    gui=MenuAbsIntSame(gui);
    gui=MenuReductionParametersSame(gui);
else
    errordlg('Please load an image first')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAP_Setup(menu,gui) 
% here is where we load the autoprocessing configuration file 
global ap_file remember_apdir ap_struct
stat=0;
if isempty(remember_apdir)
    remember_apdir=pwd;
end
[file tmp_dir] = uigetfile([fullfile(remember_apdir, '*.m'), ...
    ';*.txt;'], 'Choose AutoProcessing Configuration File');
if file  % we have a file path
    tmp_file = fullfile(tmp_dir, file);
    stat=read_APstructfile(tmp_file);
end
if stat==0
    return
else
    remember_apdir = tmp_dir;  % look in this directory next time
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function stat=read_APstructfile(tmp_file)
global ap_file remember_apdir ap_struct

tmp_struct=autoprocfileread(tmp_file); %loads parameters from the autoprocess file
if isempty(tmp_struct)
    errordlg('Loading the Auto Processing Configuration file failed--aborting')
    stat=0;
    return
end
if ~isfield(tmp_struct,'filename') tmp_struct.filename=[]; end
ap_file=tmp_file; %save these values for later use
ap_struct=tmp_struct; %save these values for later use
stat=1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAP_SingleFile(menu,gui)
% This function handles all of the aspects of the single file
% auto processing 
global ap_file ap_struct info_struct
global remember_fdir
global AP_batch AP_batch_filename first_of_list

if isempty(remember_fdir)
    remember_fdir=pwd;
end
%if there is no ap_file indicated or no ap_struct defined
%go get them
if isempty(ap_file) || isempty(ap_struct)
    MenuAP_Setup(menu,gui)
    if isempty(ap_struct)
        errordlg('Get your AutoProcessing file in order first')
        return
    end
end
% always load the latest version
stat=read_APstructfile(ap_file);
if stat==0, return; end

if AP_batch
    ap_struct.filename=AP_batch_filename;
end
%if a good filename is given in the AP configuration use it
% otherwise prompt for it
if exist(ap_struct.filename)
    im_file=ap_struct.filename;
    [tmpdir fileroot ext]=fileparts(im_file);
    file=[fileroot ext];
else
    % if no file is given then prompt for one
    dlgtitle='Input data file to Auto Process';
    [file tmpdir] = uigetfile([fullfile(remember_fdir, '*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm;')], dlgtitle);
    if file  % we have a file path
        im_file = fullfile(tmpdir, file);
        remember_fdir = tmpdir;  % look in this directory next time
    else
        return
    end
end

%Now load data and information in the data file and the
%mpa-info file
statusline = status(['Opening image file...',im_file], gui);
S=getsaxs(im_file); %loads parameters from the datafile itself and
delete(statusline);

statusline = status('Loading info file...', gui);
info_struct=mpainforead(im_file); %loads parameter from the info-dat file if it is there
delete(statusline);

%There may be conflicting/competing parameters in the various files
%so they need to ber merged.
%the basic rule is that anything in the info-file has
%preference,(info_struct)
%after which values from the AP configuration file
%are used (ap_struct)
%pixel values, exposure time and date is always taken from the image file
%itself

statusline = status('Prioritizing parameters...and loading reduction files..', gui);
S=apinfo_merge(S,info_struct,ap_struct);
delete(statusline);

%now we could get this into the main saxsgui window
img.xy=S;
img.name=im_file;
img.xy = center(img.xy, img.xy.raw_center);
statusline = status('Calculating Polar coordinates...', gui);
img.rth = polar(img.xy);
gui = newimage(img, gui);
gui.img.xy = calibrate(gui.img.xy, S.kcal, S.pixelcal, S.koffset,S.wavelength,S.pixelsize,S.detector_dist,S.calibrationtype);
gui = update_unsmoothed(gui);
delete(statusline)
if ~isempty(gui.img.rth)
    gui.img.rth=copy_reduction_parameters(gui.img.rth,S);
end
gui.img.xy=copy_reduction_parameters(gui.img.xy,S);
gui = images(gui);
guidata(gui.figure, gui)% save data
S=gui.img.rth;


if ap_struct.do_average==0
    % get the image into the averagex panel correctly
    MenuAveraging(gui)
    % and do nothing else
else
    statusline = status('Calculating Average...', gui);
    %proceed to plot it
    az=1; %using averagex plot approach for now
    if ap_struct.I_vs_q==0, az=0; end
    bounds=[ap_struct.qstart ap_struct.phistart ap_struct.qend ap_struct.phiend];
    if strcmp(ap_struct.reduction_type,'p')
        S=reduction_type(S,'p');
    else
        S=reduction_type(S,'s');
    end
    S=reduction_state_from_files(S);
    npts=ap_struct.num_xpoints;
    if ap_struct.I_vs_q==1
        S=xaxisbintype(S,strcmp(ap_struct.xaxis_type,'log'));
        if strcmp(S.reduction_type,'s')
            profile = average_raw(S, 'x',bounds, npts);
        else
            %perform pixel-by-pixel averaging
            profile =average_p(S,'x',bounds,npts);
        end
        txt='averagex_plot';
    else
        S=xaxisbintype(S,0); %0 means it is linear
        if strcmp(S.reduction_type,'s')
            %perform spectra-by-spectra averaging
            profile = average_raw(S, 'y',bounds, npts);
        else
            %perform pixel-by-pixel averaging
            profile =average_p(S,'y',bounds,npts);
        end
        txt='averagey_plot';
    end
    delete(statusline)
    statusline = status('Creating Averaged plot', gui);

     %if 0==1 %(ap_struct.new==0) && (first_of_list==0)
     if (ap_struct.new==0) && (first_of_list==0)
        figs = get(0, 'Children');  % handles to all figure windows
        thefig = 0;  % figure to add to if nonzero
        if az
            for fig = figs'
                if isequal(get(fig, 'Tag'), 'averagex_plot')  % one of ours
                    thefig = fig;
                    break
                end
            end
        else
            for fig = figs'
                if isequal(get(fig, 'Tag'), 'averagey_plot')  % one of ours
                    thefig = fig;
                    break
                end
            end
        end
        if thefig  % if we found one
            figure(thefig)
            ax = findobj(get(thefig,'Children'),'Type','axes');
            if strcmp(get(ax(2),'Tag'),'legend')
                ax=ax(1);
            else
                ax = ax(2);  % first one found
            end
            % set(ax, 'NextPlot', 'add')
            set(ax, 'NextPlot', 'replacechildren');
            %     leg = get(ax, 'UserData');
            %     leg = [leg, {regionstr(handles)}];  % extend cell array
            %     set(ax, 'UserData', leg)
            % Cycle through color order.
            colororder = get(ax, 'ColorOrder');
            color = colororder(mod(nlines(ax)-1,7)+1, :);
            profile=strip_reduction_pixels(profile);
            set(ax,'YScale','lin')
            plot(profile, 'Color', color, 'Parent', ax)
            %     legend(leg{:})
            set(ax,'YScale','log')
            h=reduction_text(ax);
        end
    else
        profile=strip_reduction_pixels(profile);
        fig = plotx(profile);
        set(fig, 'Tag', txt)  % mark it so we can find it later
        lastline=findobj(gcf,'Type','line');  %This returns all the lines in the plot but last is always element number 1
        set(lastline(1),'UserData',profile); %saving saxsobj in data in the UserData variable
        set(lastline(1),'Tag','Data'); %so we can find it later when we want to change display
        plothandle=findobj(get(fig,'Children'),'Type','axes');
        set(plothandle,'Tag','plot');
        fig;
        if ~isempty(info_struct)
            tmptxt=[info_struct.Sample_Description,'; ',info_struct.Date];
        else
            tmptxt=[S.raw_filename,'; ',S.raw_date];
        end
        set(get(gca,'Title'),'String',tmptxt)
        if ap_struct.reduction_text==1
            h=reduction_text(plothandle);
        end
        set(plothandle,'YScale','log')
    end
end
delete(statusline)
if ap_struct.do_average==1 && ap_struct.do_output==1
    fig;
       
    savefile=[ap_struct.output_dir filesep file];
    
    display(savefile);
    
    [dir file ext]=fileparts(savefile);
%     doesn't work, don't know why. Mike
%     if ap_struct.output_dir == ''
%         print('ap_struct.output_dir == empty');
%         outfile = [fullfile(fileparts(savefile),file), '_', ap_struct.addon_name];
%     else
%         outfile = [fullfile(ap_struct.output_dir,file),'_',ap_struct.addon_name];
%     end

%   but this one works
    outfile = [fileparts(im_file) filesep file '_' ap_struct.addon_name];        
    display(outfile);
    
    if ap_struct.print==1
        print(fig,'-dwinc')
    end
    
    if ap_struct.print2D2jpeg==1
       
        %print(gui.figure,[outfile,'2D'],'-djpeg')
        print(fig,[outfile,'2D'],'-djpeg')
    end
    if ap_struct.print1D2jpeg==1
        saveas(fig,[outfile,'1D'],'jpg')
    end
    if ap_struct.save2fig==1
        saveas(fig,outfile,'fig')
    end

    X = profile.xaxisfull;
    Y = profile.pixels/profile.livetime;
    E=profile.relerr.*Y;
    if ap_struct.save2columns==1
        data = [X(:), Y(:)];
        dlmwrite([outfile,'xy.csv'], data, ',')
        
    end
    if ap_struct.save3columns==1
        data = [X(:), Y(:), E(:)];
        dlmwrite([outfile,'xye.csv'], data, ',')
    end
end
AP_batch=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function n = nlines(hax)
% Return number of average intensity lines in current plot.
n = 0;
childtypes = get(get(hax, 'Children'), 'Type');
% May have a one-line character array or a cell array.
if ischar(childtypes)
    childtypes = {childtypes};  % make it a cell just for uniformity
end
for itype = 1 : length(childtypes)
    if isequal(childtypes{itype}, 'line')
        n = n + 1;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAP_MultipleFiles(menu,gui)
% This function handles multiple files with the same 
% auto processing configuration file  
%
% It is not a very intelligent handling, since it does not allow to
% do any data analysis on the actual series...maybe something for later
% so it really only sets up a loop over MenuAP_SingleFile
% using the users input from getfiles
% Mysa 16. 02. 2007 file *.mys is accepted as a lits of data files
% corrected 22. 02 deblank
global ap_file ap_struct info_struct
global remember_fdir
global AP_batch AP_batch_filename first_of_list
global multiple_file_run

multiple_file_run=1;
olddir=pwd;
if ~isempty(remember_fdir) && exist(remember_fdir)==7
    cd(remember_fdir)
end
[tmppath,tmpfiles]=getfiles('Choose files for Auto-Processing');

for ii=1:length(tmpfiles)
    if ii==1, first_of_list=1;else; first_of_list=0; end
    AP_batch_filename=fullfile(tmppath,tmpfiles{ii});
    AP_batch=1;
    MenuAP_SingleFile(menu,gui)
end
AP_batch=0;
if tmppath~=0
    remember_fdir=tmppath;
end
first_of_list=[];
cd(olddir)
multiple_file_run=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAP_FilesFromFile(menu,gui) 
% This function gets the filenames and additional parameters from a comma
% separated variables
%
% The first line in the file must contain the name of the fields in the
% SAXS structure for example. The first line must always be the file name,
% inclduing pathe
% path/filename,transfact,sample_thickness
% C:/data/2001.mpa,0.15,0.106
%
global ap_file ap_struct info_struct
global remember_fdir
global AP_batch AP_batch_filename

olddir=pwd;
if ~isempty(remember_fdir) && exist(remember_fdir)==7
    cd(remember_fdir)
end
[tmppath,tmpfile]=uigetfile('Choose filelist input file for Auto-Processing');
% % from Milos
% if length(ttmpfiles)==1
%     ismy=strfind(char(ttmpfiles),'mys')
%     if isempty(ismy)
%         tmpfiles=ttmpfiles; % there should be perhaps 'return' here
%     else
%         tfile=fopen([tmppath,char(ttmpfiles)],'rt')
%         nls=0
%         while ~feof(tfile)
%             nls=nls+1;
%             tmpfiles{nls}=deblank(fgetl(tfile))
%         end
%         fclose(tfile);
%     end
% else
%     tmpfiles=ttmpfiles;
% end
% tmpfiles are either from getfile or from a list file ttmpfiles

%get file names


for ii=1:length(tmpfiles)
    IF_batch_filename=fullfile(tmppath,tmpfile{ii})
    AP_batch=1;
    IF_batch=1;
    MenuAP_SingleFile(menu,gui)
end
AP_batch=0;
if tmppath~=0
    remember_fdir=tmppath;
end
cd(olddir)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAP_XY_scan(menu,gui)
% This function handles XY scans using input from the  
% auto processing configuration file  
%
% It is not a very intelligent handling, since it does not allow to
% do any data analysis on the actual series...maybe something for later
% so it really only sets up a loop over MenuAP_SingleFile
% using the users input from getfiles
global ap_file ap_struct info_struct
global remember_fdir
global AP_batch AP_batch_filename
global multiple_file_run

multiple_file_run=1;

%if there is no ap_file indicated or no ap_struct defined
%go get them
if isempty(ap_file) || isempty(ap_struct)
    MenuAP_Setup(menu,gui)
    if isempty(ap_struct)
        errordlg('Get your configuration file in order first')
        return
    end
end
% always load the latest version
stat=read_APstructfile(ap_file);
if stat==0, return; end

if ap_struct.doxy_output
    olddir=pwd;
    if isstr(remember_fdir)
        if ~isempty(remember_fdir)&& exist(remember_fdir)
            cd(remember_fdir)
        end
    end
    choosetext=['Choose files from the XY scan. There should be ',num2str(ap_struct.xy_numx*ap_struct.xy_numy),' files in all'];
    [tmppath,tmpfiles]=getfiles(choosetext);
    %do reduction and analysis
    ap_xy_scan(gui,tmppath,tmpfiles)

    if tmppath~=0
        remember_fdir=tmppath;
    end
    cd(olddir)
end
    
multiple_file_run=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui=MenuErrorCalibSame(gui)
% 
global error_norm_fact

if isempty(error_norm_fact)
    if isempty(gui.img.xy.error_norm_fact)
        error_norm_fact = 1;  % default value 
    else  % get calibration data from image
        error_norm_fact = gui.img.xy.error_norm_fact;
    end
end
if ~isempty(error_norm_fact)
    gui.img.xy=errorcalibrate(gui.img.xy,error_norm_fact);
    if ~ isempty(gui.img.rth)
        gui.img.rth=errorcalibrate(gui.img.rth,error_norm_fact);
    end
end
guidata(gui.figure, gui)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MenuErrorCalibCompute(gui)
% 
global error_norm_fact

% first bring back to uncalibrated
%gui.img.xy = uncalibrate(gui.img.xy);
%gui = update_unsmoothed(gui);

% if ~ isempty(gui.img.rth)
%     gui = makepolar(gui);
% end
%gui = images(gui);  % draw images
%guidata(gui.figure, gui)
% bring focus back to xy
% bring focus back to xy 
set(get(gui.xy_axes, 'Title'), 'String', gui.img.name, 'Interpreter', 'none')

%then ask for user input
uiwait(helpdlg([{'Click on the first corner of a fairly flat rectangular region.'}...
    {'Then drag to the other corner'}],'Error Calibrating with a flatfield'))
statusbar = status('Click on 1st corner of the rectangle, and Drag', gui);
k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');    % button down detected
delete(statusbar);
statusbar = status('Drag to other corner', gui);
finalRect = rbbox;                   % return figure units
point2 = get(gca,'CurrentPoint');    % button up detected

delete(statusbar);
statusbar = status('Calculating Error Normalization Factor', gui);

point1 = point1(1,1:2);              % extract x and y
point2 = point2(1,1:2);
p1 = min(point1,point2);             % calculate locations
offset = abs(point1-point2);         % and dimensions
% now tranforming into indices
xysize=size(gui.img.xy.pixels); %number of pixels in image
p1pix(2)=floor((-gui.img.xy.yaxis(1)+p1(2))*xysize(1)/(gui.img.xy.yaxis(2)-gui.img.xy.yaxis(1)))+1;
p1pix(1)=floor((-gui.img.xy.xaxis(1)+p1(1))*xysize(2)/(gui.img.xy.xaxis(2)-gui.img.xy.xaxis(1)))+1;
offsetpix(2)=floor(offset(2)*xysize(1)/(gui.img.xy.yaxis(2)-gui.img.xy.yaxis(1)));
offsetpix(1)=floor(offset(1)*xysize(2)/(gui.img.xy.xaxis(2)-gui.img.xy.xaxis(1)));


ROI=[gui.img.xy.pixels(p1pix(2):(p1pix(2)+offsetpix(2)),p1pix(1):p1pix(1)+offsetpix(1))];
sizeROI=size(ROI);
ROI=reshape(ROI,1,sizeROI(1)*sizeROI(2));
average_value=mean(ROI);
std_value=std(ROI);
error_norm_fact=std_value/sqrt(average_value);

if isnan(error_norm_fact)
    error_norm_fact=1;
end
gui.img.xy=errorcalibrate(gui.img.xy,error_norm_fact);

if ~ isempty(gui.img.rth)
        gui.img.rth=errorcalibrate(gui.img.rth,error_norm_fact);
end


gui = images(gui);  % draw images

guidata(gui.figure, gui)
delete(statusbar);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuErrorCalibManual(gui)
% 
global error_norm_fact

if isempty(error_norm_fact)
    if isempty(gui.img.xy.error_norm_fact)
        error_norm_fact = 1;  % default value 
    else  % get calibration data from image
        error_norm_fact = gui.img.xy.error_norm_fact;
    end
end
prompt={'Enter the Error Normalization Factor:'};
name='Input for Error Calibration';
numlines=1;
defaultanswer={num2str(error_norm_fact)};
answer = inputdlg(prompt,name,numlines,defaultanswer);
if ~isempty(answer)
    error_norm_fact=str2num(answer{1});
    gui.img.xy=errorcalibrate(gui.img.xy,error_norm_fact);
else
    gui.img.xy=errorcalibrate(gui.img.xy,error_norm_fact);
end

if ~ isempty(gui.img.rth)
    gui.img.rth=errorcalibrate(gui.img.rth,error_norm_fact);
end

gui = images(gui);  % draw images

guidata(gui.figure, gui)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuErrorCalibUncalibrate(gui)
global error_norm_fact

error_norm_fact=1;
gui.img.xy=errorcalibrate(gui.img.xy,error_norm_fact);
guidata(gui.figure, gui)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuErrorCalibWhatIs(gui)
uiwait(msgbox([...
    {'Error-calibration is a calibration that allows the SAXGUI program'},...
    {'calculate correct error estimates for image data that follows a'},...
    {'gaussian distribution different from that expected for counting'},...
    {'statistics.'},...
    {' '},...
    {'So in short, Error Calibration is not needed for photon counting '},...
    {'detectors, but can be applied to data from image plates and CCD''s'},...
    {'(where fx. a value of 100 does NOT indicate a standard deviation of 10.'},...
    {' '},...
    {'More info can be found in the SAXSGUI manual.'}],'What is Error-Calib','modal'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuReductionParametersWhatIs(gui)
uiwait(msgbox([...
    {'Avepix Reduction allows you to enter the parameters required for accurate'},...
    {'reduction of your averaged data even if it the pixel-data is very noisy. '},...
    {'You can save the entered information, so you can apply it to other images.'},...
    {' '},...
    {'The entered data is used in the 1D averaging' },...
    {'in the Processing menu. '}],'What is Avepix Reduction','modal'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuQuikWhatIs(gui)
uiwait(msgbox([...
    {'Quik Enter/As last... automatically enters the last entered data for'},...
    {' ' },...
    {'1) Error-Calibration'},...
    {'2) q-Calibration'},...
    {'3) Centering  and '},...
    {'3) Absolute Intensity Factor  and '},...
    {'4) Reduction Panels' },...
    {' ' },...
    {' '}],'What is Quik-enter','modal'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuRedWhatIs(gui)
uiwait(msgbox([...
    {'Here you can specify reductions, that are then applied to the data.'},...
    {'You can specify the reduction parameters in a panel ("edit parameters")'},...
    {'You can clear them or use the same parameters as last time.'},...
    {'How these reductions are applied depends on which mode you have ticked:'},...
    {' '},...
    {'"Apply when averaging (1D)" is the default mode where you specify '}...
    {'the reductions, but wait until averaging the data to apply them. '},...
    {'This method can thus be used for noisy data (which most SAXS data is),'},...
    {'since the reductions are applied only after the data from a large '},...
    {'number of pixels hav been averaged. '},...
    {' '},...
    {'"Apply interactively (2D)" mode performs the reduction on a pixel-to-pixel'},...
    {'basis and should therefore only be used if the correction data has good '},...
    {'statistics OR if one needs the reduced 2D images. Using this method '},...
    {'one can also "see" what happens to the image, as the reductions are applied.'}]...
    ,'What is this Reductions','modal'));
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuProcessingWhatIs(gui)
uiwait(msgbox([...
    {'Here you can start the processing of your data to:'},...
    {' '},...
    {'1) Obtain averaged 1D spectra (azimuthal or radial averages)'},...
    {'2) Analyse your 2D images (beta version)'},...
    {'3) Auto Processing single or multiple files '},...
    {'   The parameters controlling the specific processing   '},...
    {'   (i.e the processing instruction) are given in a proces '},...
    {'   setup file. (Note: This feature is only implemented for MPA-files, '},...
    {' '}],'What is Processing','modal'));
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAbsIntCalibrationWhatIs(gui)
uiwait(msgbox([...
    {'Here you can enter the Absolute Intensity Calibration Factor or'},...
    {'start some tools to help you calculate the Abs. Int. Calibration Factor:'},...
    {' '},...
    {'1) Set the same value as last'},...
    {'2) Enter the value manually (can also be done from the Reductions Panel)'},...
    {'3) Start a tool to calculate the factor using data from water. (The water data should be loaded before running this) '},...
    {'4) Start a tool to calculate the factor using data from Glassy Carbon. (The data should be loaded before running this) '},...
    {'5) Save Calibration data to a file'},...
    {'6) Load Calibration data from a file'},...
    {'7) Delete the Absolute Intensity Calibration Factor '},...
    {' '}],'What is Absolute Intensity Calibration','modal'));
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui=MenuAbsIntSame(gui)

global abs_int_fact_last
if ~isempty(abs_int_fact_last)
    gui.img.xy=absintfact(gui.img.xy,abs_int_fact_last);
    if ~ isempty(gui.img.rth)
        gui.img.rth = absintfact(gui.img.rth,abs_int_fact_last);
    end
    guidata(gui.figure, gui)
    if (abs_int_fact_last~=1)
      msgbox(['The Absolute Intensity Factor is ', num2str(abs_int_fact_last)])
    end
end
%else
    %    msgbox('No Last Absolute Intensity Factor available')
%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAbsIntSave(gui)
global abs_int_fact_last
[name, path] = uiputfile('*.mat', 'Save Absolute Intensity Factor File');

if name
    if ~isempty(gui.img.xy.abs_int_fact)
        abs_int_fact=gui.img.xy.abs_int_fact;
    else
        if ~isempty(abs_int_fact_last)
            abs_int_fact=abs_int_fact_last;
        else  
            error('Absolute Intensity Factor has not been defined...and will not be saved')
        end
    end
    save([path,name],'abs_int_fact')
end
msgbox(['The Absolute Intensity Factor is saved as ', num2str(abs_int_fact)])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAbsIntLoad(gui)
global abs_int_fact_last
[name, path] = uigetfile('*.mat', 'Load Absolute Intensity Factor from File');
if name
    load([path name])
    if ~isempty(abs_int_fact)
        abs_int_fact_last=abs_int_fact;
        gui.img.xy = absintfact(gui.img.xy,abs_int_fact);
        if ~ isempty(gui.img.rth)
            gui.img.rth = absintfact(gui.img.rth,abs_int_fact);
        end
    else
        abs_int_fact_last=[];
    end
    guidata(gui.figure, gui)
end
msgbox(['The Absolute Intensity Factor is read as ', num2str(abs_int_fact)])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAbsIntUncalibrate(gui)
global abs_int_fact_last
abs_int_fact='';
gui.img.xy = absintfact(gui.img.xy,abs_int_fact);
if ~ isempty(gui.img.rth)
    gui.img.rth = absintfact(gui.img.rth,abs_int_fact);
end
abs_int_fact_last=abs_int_fact;
guidata(gui.figure, gui)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAbsIntByGC(gui)
global abs_int_fact_last
global glassy_carbon_abs_int


if isempty(gui.img.xy.kcal)
    errordlg('You must calibrate for q before the Absolute Intensity Factor can be calculated')
    error('You must calibrate for q before the Absolute Intensity Factor can be calculated') 
    return
end
    
if isempty(gui.img.rth)
    errordlg('You must choose a center before the Absolute Intensity Factor can be calculated')
    error('You must choose a center before the Absolute Intensity Factor can be calculated')
    return
else
    % make an azimuthal average with no subtractions
    % 
    bounds=[0.1*gui.img.rth.xaxis(2),gui.img.rth.yaxis(1),0.5*gui.img.rth.xaxis(2),gui.img.rth.yaxis(2)];
    npts=200;
    saxsin=reduction_state(gui.img.rth); % sets the reduction state to "no reduction"
    profile = average_raw(saxsin, 'x',bounds, npts); %return a r-ave saxsobject
    %now set the GC-"maximum" = maximum GC value minus 2*std
    GCInt=max(profile.pixels)*(1-2*profile.relerr(profile.pixels==max(profile.pixels))); 
    GCRate=GCInt/profile.livetime;
    abs_int_fact=glassy_carbon_abs_int/GCRate(1);
    abs_int_fact_last=abs_int_fact;
end
gui.img.xy = absintfact(gui.img.xy,abs_int_fact);
gui.img.rth = absintfact(gui.img.rth,abs_int_fact);
guidata(gui.figure, gui)
msgbox(['The Absolute Intensity Factor is calculated to be ', num2str(abs_int_fact)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAbsIntManual(gui)
global abs_int_fact_last

abs_int_fact_str = inputdlg({'Absolute Intensity Factor:'}, 'Enter Absolute Intensity Factor', 1, ...
    {num2str(abs_int_fact_last)});
if ~ isempty(abs_int_fact_str)  % user didn't cancel
    abs_int_fact_new = str2double(abs_int_fact_str)';
    if max(abs(abs_int_fact_new-abs_int_fact_last))>eps || max(isnan(abs_int_fact_last)) % user changed values
        gui.img.xy = absintfact(gui.img.xy, abs_int_fact_new);
        gui.img.rth = absintfact(gui.img.rth, abs_int_fact_new);
        guidata(gui.figure, gui)  % save data
    end
    abs_int_fact_last=abs_int_fact_new;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAbsIntByWater(gui)
global abs_int_fact_last
global water_abs_int

if isempty(gui.img.xy.kcal)
    errordlg('You must calibrate for q before the Absolute Intensity Factor can be calculated')
    error('You must calibrate for q before the Absolute Intensity Factor can be calculated') 
    return
end
if isempty(gui.img.rth)
    errordlg('You must choose a center before the Absolute Intensity Factor can be calculated')
    error('You must choose a center before the Absolute Intensity Factor can be calculated')
    return
else
    % make an azimuthal average with no subtractions
    watercalib_parameters(gui.img.rth)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuCalibsSame(gui)

gui=MenuKSame(gui);
gui=MenuCenterSame(gui);
gui=MenuErrorCalibSame(gui);
gui=MenuAbsIntSame(gui);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function write_list_of_fitfunctions
%this is a small routine that is only called from 
%the debugging line. It creaates a list of the functions
%available for Fullfit
try
    fitfunctions.oneD=find_fitfunctions(1);
catch
    fitfunctions.oneD=[];
end
try
    fitfunctions.twoD=find_fitfunctions(2);
catch
    fitfunctions.twoD=[];
end
%for the executable the fitting function library is dynamic:

save('standard_fitfunctions','fitfunctions')

function MenuFileOpenCont(~, gui)
% Callback for File menu "Open Continuously" command, which 
% reads the latest file in a directory and then displays it and 
% averages
global old_saxs_obj
if ~isempty(gui.img.rth)
    old_saxs_obj=gui.img.rth;
end
img = empty_img;
Ssum=0;
[filepath,files]=getfiles('Choose all the files you want to add together');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAP_SingleFileFromHeader(menu,gui)
% This function handles all of the aspects of the single file
% auto processing using info from the header (implemented for SAXSLAB)
global remember_fdir
global AP_batch AP_batch_filename AP_batch_ouput_dir first_of_list

remove_figstatus % this line deletes all the yellow "statuslines"


if isempty(remember_fdir)
    remember_fdir=pwd;
end

%Now load data and information in the data file and the
%mpa-info file
statusline = status(['Opening image file...',AP_batch_filename], gui);
S=getsaxs(AP_batch_filename); %loads parameters from the datafile itself and
delete(statusline);

statusline = status('Loading header info...', gui);
S=fill_from_saxslab_header(S);
delete(statusline);


%now we could get this into the main saxsgui window
img.xy=S;
img.name=AP_batch_filename;
img.xy = center(img.xy, img.xy.raw_center);
statusline = status('Calculating Polar coordinates...', gui);
img.rth = polar(img.xy);
gui = newimage(img, gui);
gui.img.xy = calibrate(gui.img.xy, S.kcal, S.pixelcal, S.koffset,S.wavelength,S.pixelsize,S.detector_dist,S.calibrationtype);
gui = update_unsmoothed(gui);
delete(statusline)
if ~isempty(gui.img.rth)
    gui.img.rth=copy_reduction_parameters(gui.img.rth,S);
end
gui.img.xy=copy_reduction_parameters(gui.img.xy,S);
gui = images(gui);
guidata(gui.figure, gui)% save data
S=gui.img.rth;


statusline = status('Calculating Average...', gui);
%proceed to plot it
az=1; %using averagex plot approach for now
bounds=[min(S.xaxis) 0 max(S.xaxis) 250];

S=reduction_type(S,'s');
S=reduction_state_from_files(S);
npts=400;
if (1==1)  %ap_struct.I_vs_q==1
    S=xaxisbintype(S,0);
    if strcmp(S.reduction_type,'s')
        profile = average_raw(S, 'x',bounds, npts);
    else
        %perform pixel-by-pixel averaging
        profile =average_p(S,'x',bounds,npts);
    end
    txt='averagex_plot';
else
    S=xaxisbintype(S,0); %0 means it is linear
    if strcmp(S.reduction_type,'s')
        %perform spectra-by-spectra averaging
        profile = average_raw(S, 'y',bounds, npts);
    else
        %perform pixel-by-pixel averaging
        profile =average_p(S,'y',bounds,npts);
    end
    txt='averagey_plot';
end
delete(statusline)
statusline = status('Creating Averaged plot', gui);

%if 0==1 %(ap_struct.new==0) && (first_of_list==0)
if (ap_struct.new==0) && (first_of_list==0)
    figs = get(0, 'Children');  % handles to all figure windows
    thefig = 0;  % figure to add to if nonzero
    if az
        for fig = figs'
            if isequal(get(fig, 'Tag'), 'averagex_plot')  % one of ours
                thefig = fig;
                break
            end
        end
    else
        for fig = figs'
            if isequal(get(fig, 'Tag'), 'averagey_plot')  % one of ours
                thefig = fig;
                break
            end
        end
    end
    if thefig  % if we found one
        figure(thefig)
        ax = findobj(get(thefig,'Children'),'Type','axes');
        if strcmp(get(ax(2),'Tag'),'legend')
            ax=ax(1);
        else
            ax = ax(2);  % first one found
        end
        set(ax, 'NextPlot', 'add')
        %     leg = get(ax, 'UserData');
        %     leg = [leg, {regionstr(handles)}];  % extend cell array
        %     set(ax, 'UserData', leg)
        % Cycle through color order.
        colororder = get(ax, 'ColorOrder');
        color = colororder(mod(nlines(ax)-1,7)+1, :);
        profile=strip_reduction_pixels(profile);
        set(ax,'XScale','lin')
        plot(profile, 'Color', color, 'Parent', ax)
        %     legend(leg{:})
        set(ax,'YScale','log')
        h=reduction_text(ax);
    end
else
    profile=strip_reduction_pixels(profile);
    fig = plotx(profile);
    set(fig, 'Tag', txt)  % mark it so we can find it later
    lastline=findobj(gcf,'Type','line');  %This returns all the lines in the plot but last is always element number 1
    set(lastline(1),'UserData',profile); %saving saxsobj in data in the UserData variable
    set(lastline(1),'Tag','Data'); %so we can find it later when we want to change display
    plothandle=findobj(get(fig,'Children'),'Type','axes');
    set(plothandle,'Tag','plot');
    fig;
    %if ~isempty(info_struct)
    %    tmptxt=[info_struct.Sample_Description,'; ',info_struct.Date];
    %else
    tmptxt=[S.raw_filename,'; ',S.raw_date];
    %end
    set(get(gca,'Title'),'String',tmptxt)
    if 1==1
        h=reduction_text(plothandle);
    end
    set(plothandle,'YScale','log')
end
delete(statusline)
if (1==1)%ap_struct.do_average==1 && ap_struct.do_output==1
    fig;
    savefile=[AP_batch_ouput_dir filesep AP_batch_filename];
    [dir file ext]=fileparts(savefile);
    outfile=[fullfile(AP_batch_ouput_dir,file),'_fh_'];
    if 0==1  %ap_struct.print==1
        print(g,'-dwinc')
    end
    if 1==1  %ap_struct.print2D2jpeg==1
        set(gui.figure,'PaperPositionMode','manual')
        print(gui.figure,'-djpeg',[outfile,'2D'])
    end
    if 1==1 %ap_struct.print1D2jpeg==1
        saveas(fig,[outfile,'1D'],'jpg')
    end
    if 0==1 %ap_struct.save2fig==1
        saveas(fig,outfile,'fig')
    end
    
    X = profile.xaxisfull;
    Y = profile.pixels/profile.livetime;
    E=profile.relerr.*Y;
    if 0==1 %ap_struct.save2columns==1
        data = [X(:), Y(:)];
        dlmwrite([outfile,'xy.csv'], data, ',')
    end
    if 1==1 %ap_struct.save3columns==1
        data = [X(:), Y(:), E(:)];
        dlmwrite([outfile,'xye.csv'], data, ',')
    end
end
AP_batch=0;
delete(fig)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MenuAP_FromHeader(menu,gui)
% This function handles multiple files with the same 
% auto processing configuration file  
%
% It is not a very intelligent handling, since it does not allow to
% do any data analysis on the actual series...maybe something for later
% so it really only sets up a loop over MenuAP_SingleFile
% using the users input from getfiles
% Mysa 16. 02. 2007 file *.mys is accepted as a lits of data files
% corrected 22. 02 deblank
global ap_file ap_struct info_struct
global remember_fdir
global AP_batch AP_batch_filename AP_batch_ouput_dir first_of_list
global multiple_file_run

multiple_file_run=1;
olddir=pwd;
if ~isempty(remember_fdir) && exist(remember_fdir)==7
    cd(remember_fdir)
end
[tmppath,tmpfiles]=getfiles('Choose files for Auto-Processing');
AP_batch_ouput_dir=uigetdir('Choose output directory');

for ii=1:length(tmpfiles)
    if ii==1, first_of_list=1;else; first_of_list=0; end
    AP_batch_filename=fullfile(tmppath,tmpfiles{ii});
    AP_batch=1;
    MenuAP_SingleFileFromHeader(menu,gui)
end
AP_batch=0;
if tmppath~=0
    remember_fdir=tmppath;
end
first_of_list=[];
cd(olddir)
multiple_file_run=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

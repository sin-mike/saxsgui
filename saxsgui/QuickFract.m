function Icalc=QuickFract(q,I,varargin)
% Description
% Fractal dimension determination
% Description end
% Number of Parameters: 2
% parameter 1: Fractamp : Fractal amplification
% parameter 2: FractD : Fractal Hausdorff Dimension D
% parameter 3: Backg : Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Quick Fractal determination does not work...


global fitpar
values=varargin{1};
Fractamp=values(1);
FractD=values(2);
if numel(values)==2
    Backg=0;
else
    Backg=values(3);
end
   

Icalc=Fractamp.*q.^-(6-FractD)+Backg;

%Icalc=0.5.*Fractamp.*sin(pi.*(FractD-1)./2).*q.^-(6-FractD);
%clear waste
%--------------------------------------------------------------------------

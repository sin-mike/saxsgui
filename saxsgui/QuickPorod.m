function Icalc=QuickPorod(q,I,varargin)
% Description
% This function calculates 1D data for a PearsonVII peak with a sloped
% background
% Description end
% Number of Parameters: 6
% parameter 1: Centre : Centre of PearsonVII
% parameter 2: Amp : Amplitude of PearsonVII
% parameter 3: Width : FWHM of PearsonVII
% parameter 4: Tailing : Tailing parameter of PearsonVII
% parameter 5: Backg : Background of PearsonVII
% parameter 6: Slope : Slope of the PearsonVII
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Quick Porod
% A fitting function that can determine the limits of the Porod region of a
% SAXS scattering plot. 


global fitpar
values=varargin{1};
Kp=values(1);
%Leftbound=values(2); %flexible bounds not possible!
%Rightbound=values(3);
%waste,lbindex=min(abs(q-leftbound));
%waste,ubindex=min(abs(q-rightbound));

Icalc=Kp./q.^4;
%clear waste
%--------------------------------------------------------------------------

function varargout = Fitting2D(varargin) 
%Fitting2D  Explore intensity averaging in a SAXS image.
%   Fitting2D(SAXSIM) starts the 2D Fitting window with SAXS image
%   SAXSIM.  
%
%   With the 2D fitting window you select regions of the SAXS image and
%   fitting functions to invoke fitting routines on 2D image data. 
%   Usually SAXSIM will be a an xy-SAXS image corrected according to the
%   corrections specified in the SAXS gui window.
%   
%   You can choose a particular fitting function from a list of available
%   fitting functions in directory, and you can then choose which
%   of the parameters you want to fix and which ones you want to let vary
%   i.e. fit.
%
%   Fitting2D(SAXSIM, CTOP, CMAP) displays SAXSIM using colormap CMAP (an
%   array of RGB values) and upper color scaling limit CTOP.  Leave off
%   CMAP to use the default colormap.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Fitting2D, by itself, creates a new Fitting2D or raises the existing
%   singleton*.
%
%   H = Fitting2D returns the handle to a new Fitting2D or the handle to
%   the existing singleton*.
%
%   Fitting2D('CALLBACK',hObject,eventData,handles,...) calls the local
%   function named CALLBACK in Fitting2D.M with the given input arguments.
%
%   Fitting2D('Property','Value',...) creates a new Fitting2D or raises the
%   existing singleton*.  Starting from the left, property value pairs are
%   applied to the GUI before Fitting2D_OpeningFunction gets called.  An
%   unrecognized property name or invalid value makes property application
%   stop.  All inputs are passed to Fitting2D_OpeningFcn via varargin.
%
%   *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%   instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Fitting2D

% Last Modified by GUIDE v2.5 11-Jul-2007 22:46:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Fitting2D_OpeningFcn, ...
                   'gui_OutputFcn',  @Fitting2D_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes just before Fitting2D is made visible.
function Fitting2D_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Fitting2D (see VARARGIN)

% Choose default command line output for Fitting2D
handles.output = hObject;

% Get the rth SAXS image.
if length(varargin) > 0 && isa(varargin{1}, 'saxs')
    handles.saxsin = varargin{1};
else
    handles.saxsin = saxs(ones(1024));  % temporary cheat to display empty figure
%     error('I need a SAXS argument.');
end

% Get the xy SAXS image.
if length(varargin) > 0 && isa(varargin{2}, 'saxs')
    handles.saxsdata = varargin{2};
else
    handles.saxsdata = saxs(ones(1024));  % temporary cheat to display empty figure
%     error('I need a SAXS argument.');
end

% Get optional CTOP, CMAP parameters.  
% No validation, because good programmers are lazy.
if length(varargin) > 2
    handles.ctop = varargin{3};
end
if length(varargin) > 3
    handles.cmap = varargin{4};
end
if length(varargin) > 4
    handles.displog = varargin{5};
end

% Record pixel spacing in the SAXS image.
handles.xspacing = (handles.saxsin.xaxis(2) - handles.saxsin.xaxis(1)) ...
    / (size(handles.saxsin.pixels, 2) - 1);
handles.yspacing = (handles.saxsin.yaxis(2) - handles.saxsin.yaxis(1)) ...
    / (size(handles.saxsin.pixels, 1) - 1);
    
% Display the SAXS image (in log if log in SAXSGUI window)
if handles.displog==1
    saxsin=log10(handles.saxsin);
else
    saxsin=handles.saxsin;
end
handles.image1 = image(saxsin, 'Parent', handles.axes1);

if isfield(handles, 'ctop')
    ctop_tmp=handles.ctop;
    ctop=max(max(handles.saxsin.pixels));
else
    ctop=max(max(handles.saxsin.pixels));
end
if ctop_tmp>ctop
    ctop_tmp=ctop;
end
set(handles.axes1, 'CLim', [0 ctop_tmp])
if isfield(handles, 'cmap')
    set(handles.figure1, 'Colormap', handles.cmap)
end

%Geet Slider Top Value
set(handles.top_text, 'String', num2str(ctop_tmp))
%Set Slider min max Value
set(handles.top_slider, 'Max', ctop)

%Set Slider Edit Value
set(handles.top_slider, 'Value', ctop_tmp)

% Adjust the title
tit = get(handles.axes1, 'Title'); 

if handles.displog==1
    set(handles.top_text,'Visible','off')
    set(handles.top_slider,'Visible','off')
    set(handles.text15,'Visible','off')
    set(tit, 'String', 'Logarithmic color scale')
else
    set(handles.top_text,'Visible','on')
    set(handles.top_slider,'Visible','on')
    set(handles.text15,'Visible','on')
    set(tit, 'String', 'Linear color scale')
end


% Insert the filename
filename=find_filename(handles.saxsin);
set(handles.Filename, 'String', filename);

% Set the bounds.
set(handles.ytop, 'String', num2str(handles.saxsin.yaxis(2)))
set(handles.ybottom, 'String', num2str(handles.saxsin.yaxis(1)))
set(handles.xleft, 'String', num2str(handles.saxsin.xaxis(1)))
set(handles.xright, 'String', num2str(handles.saxsin.xaxis(2)))

% Create the bounding rectangle.
handles.rect = rectangle('EraseMode', 'xor');
position_rectangle(handles)

% % Save the color order for plotting multiple lines.
% handles.colororder = get(handles.axes2, 'ColorOrder');

% Pick up mouse button down to drag out a rectangle in the left image.
set([handles.image1, handles.rect, handles.axes1, handles.figure1], ...
    'ButtonDownFcn', {@drag, handles})

% Now we need to load the available functions into the handles and
% put them into the pulldown menus and so first we find the functions
handles.fitfunctions.twoD=find_fitfunctions(2); %searching for 2d fitfunctions

% Update handles structure
guidata(hObject, handles);

% now load the first function
update_function_gui(handles,1);


% UIWAIT makes Fitting2D wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Outputs from this function are returned to the command line.
function varargout = Fitting2D_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function position_rectangle(handles)
% Set the bounding rectangle position.
xl = str2double(get(handles.xleft, 'String'));
xr = str2double(get(handles.xright, 'String'));
yb = str2double(get(handles.ybottom, 'String'));
yt = str2double(get(handles.ytop, 'String'));
set(handles.rect, 'Position', [xl, yb, xr - xl, yt - yb])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function argout = bounded(handles)
% Return the left image cropped by the bounding lines.
xl = str2double(get(handles.xleft, 'String'));
xr = str2double(get(handles.xright, 'String'));
yb = str2double(get(handles.ybottom, 'String'));
yt = str2double(get(handles.ytop, 'String'));
argout = crop(handles.saxsin, [xl, yb; xr, yt]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function drag(hobject, eventdata, handles)
% Called on mouse button down over the left image, bounding rectangle, axes
% object, or unoccupied figure space, we drag out a new area for the
% bounding rectangle.  Clip the rectangle to the axes.
point1 = get(handles.axes1, 'CurrentPoint');  % initial mouse position in axes
rbbox;
point2 = get(handles.axes1, 'CurrentPoint');  % final mouse position in axes
point1 = point1(1,1:2);  % extract just x and y
point2 = point2(1,1:2);
p1 = min(point1,point2);  % lower left corner
offset = abs(point1-point2);  % dimensions

if all(offset)  % rectangle has area (no dimension = 0)
    % Select entire image first so we don't run into old boundaries.
    selectall(handles)
    % Update text boxes.
    set(handles.xleft, 'String', num2str(p1(1)))
    xleft_validate(handles)
    set(handles.xright, 'String', num2str(p1(1) + offset(1)))
    xright_validate(handles)
    set(handles.ybottom, 'String', num2str(p1(2)))
    ybottom_validate(handles)
    set(handles.ytop, 'String', num2str(p1(2) + offset(2)))
    ytop_validate(handles)
    
    % Reposition bounding rectangle.
    position_rectangle(handles)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function ytop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ytop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ytop_Callback(hObject, eventdata, handles)
% Interpret new ytop string as a number.
% Keep the number within the limits of the SAXS image.
% Put the interpreted number back in ytop as a string, in case
% it differs from what the user intended.
% Redraw the corresponding bounding line in axes1.
% Replot the right-hand curve.
%
% hObject    handle to ytop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ytop_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ytop_validate(handles)
val = str2double(get(handles.ytop,'String'));  % interpret
val = min([val, handles.saxsin.yaxis(2)]);  % limit high value
val = max([val, str2double(get(handles.ybottom, 'String')) + handles.yspacing]);
    % at least one pixel above bottom bounding line
set(handles.ytop, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function ybottom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ybottom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ybottom_Callback(hObject, eventdata, handles)
% hObject    handle to ybottom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ybottom_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ybottom_validate(handles)
val = str2double(get(handles.ybottom,'String'));  % interpret
val = max([val, handles.saxsin.yaxis(1)]);  % limit low value
val = min([val, str2double(get(handles.ytop, 'String')) - handles.yspacing]);
    % at least one pixel below top bounding line
set(handles.ybottom, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function xleft_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xleft_Callback(hObject, eventdata, handles)
% hObject    handle to xleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xleft_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xleft_validate(handles)
val = str2double(get(handles.xleft,'String'));  % interpret
val = max([val, handles.saxsin.xaxis(1)]);  % limit low value
val = min([val, str2double(get(handles.xright, 'String')) - handles.xspacing]);
    % at least one pixel short of right bounding line
set(handles.xleft, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function xright_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xright_Callback(hObject, eventdata, handles)
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xright_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xright_validate(handles)
val = str2double(get(handles.xright,'String'));  % interpret
val = min([val, handles.saxsin.xaxis(2)]);  % limit high value
val = max([val, str2double(get(handles.xleft, 'String')) + handles.xspacing]);
    % at least one pixel right of left bounding line
set(handles.xright, 'String', num2str(val))  % display what we interpreted



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in newplot.
function newfit_Callback(hObject, eventdata, handles)
% hObject    handle to newplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hstatus=figstatus('Loading Parameters',get(hObject,'Parent'));
fitpar=read_gui_vals(handles);
bounds=get(handles.rect, 'Position');
clearstatus
hstatus=figstatus('Initializing Fit',get(hObject,'Parent'));

[Fitparam,Fitval,Info] = fitsaxs2D(handles.saxsdata,fitpar,bounds);

%update table
update_gui_fitted(handles,Fitparam,Fitval)
clearstatus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in newplot.
function calculate_Callback(hObject, eventdata, handles)
% hObject    handle to newplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hstatus=figstatus('Calculating function based on Start values',get(hObject,'Parent'));
fitpar=read_gui_vals(handles);
bounds=get(handles.rect, 'Position');
clearstatus
drawnow
fitpar.numiter=0;
fitpar.ul=fitpar.start;
fitpar.ll=fitpar.start;
[Fitparam,Fitval,Info] = fitsaxs2D(handles.saxsdata,fitpar,bounds);

set(handles.chisqval,'String',num2str(Fitval,3))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function argout = regionstr(handles)
% Return a string '(x1,y1) to (x2,y2)' for the bounded region.
argout = ['(', get(handles.xleft, 'String'), ...
        ',', get(handles.ybottom, 'String'), ...
        ') to (', get(handles.xright, 'String'), ...
        ',', get(handles.ytop, 'String'), ')'];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function selectall(handles)
% Set bounds to select all of image.  Note we don't position the bounding
% rectangle so if you want to, call position_rectangle after us.
set(handles.xleft, 'String', num2str(handles.saxsin.xaxis(1)));
set(handles.xright, 'String', num2str(handles.saxsin.xaxis(2)));
set(handles.ybottom, 'String', num2str(handles.saxsin.yaxis(1)));
set(handles.ytop, 'String', num2str(handles.saxsin.yaxis(2)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in BinningPopUp.
function BinningPopUp_Callback(hObject, eventdata, handles)
% hObject    handle to BinningPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns BinningPopUp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BinningPopUp
%BinningPopUpnum=get(handles.BinningPopUp,'Value');
%update_gui_new(handles,BinningPopUpnum); 

% --- Executes during object creation, after setting all properties.
function BinningPopUp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BinningPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in FittingFilenamePopup.
function FittingFilenamePopup_Callback(hObject, eventdata, handles)
% hObject    handle to FittingFilenamePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns FittingFilenamePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FittingFilenamePopup

fitfilenum=get(handles.FittingFilenamePopup,'Value');
update_function_gui(handles,fitfilenum);


% --- Executes during object creation, after setting all properties.
function FittingFilenamePopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FittingFilenamePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in fit1.
function fit1_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit2.
function fit2_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit3.
function fit3_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit4.
function fit4_Callback(hObject, eventdata, handles)


% --- Executes on button press in fit5.
function fit5_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit6.
function fit6_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit7.
function fit7_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit8.
function fit8_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit9.
function fit9_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function startval1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval1_Callback(hObject, eventdata, handles)
startval_input_check(handles,1)

% --- Executes during object creation, after setting all properties.
function startval2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval2_Callback(hObject, eventdata, handles)
startval_input_check(handles,2)

% --- Executes during object creation, after setting all properties.
function startval3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval3_Callback(hObject, eventdata, handles)
startval_input_check(handles,3)

% --- Executes during object creation, after setting all properties.
function startval4_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval4_Callback(hObject, eventdata, handles)
startval_input_check(handles,4)

% --- Executes during object creation, after setting all properties.
function startval5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval5_Callback(hObject, eventdata, handles)
startval_input_check(handles,5)

% --- Executes during object creation, after setting all properties.
function startval6_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval6_Callback(hObject, eventdata, handles)
startval_input_check(handles,6)

% --- Executes during object creation, after setting all properties.
function startval7_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval7_Callback(hObject, eventdata, handles)
startval_input_check(handles,7)

% --- Executes during object creation, after setting all properties.
function startval8_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval8_Callback(hObject, eventdata, handles)
startval_input_check(handles,8)

% --- Executes during object creation, after setting all properties.
function startval9_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval9_Callback(hObject, eventdata, handles)
startval_input_check(handles,9)


% --- Executes on button press in Graphics_on.
function Graphics_on_Callback(hObject, eventdata, handles)
% hObject    handle to Graphics_on (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Graphics_on



function num_of_iter_Callback(hObject, eventdata, handles)
% hObject    handle to num_of_iter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of num_of_iter as text
%        str2double(get(hObject,'String')) returns contents of num_of_iter as a double


% --- Executes during object creation, after setting all properties.
function num_of_iter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_of_iter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tolerance_Callback(hObject, eventdata, handles)
% hObject    handle to Tolerance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tolerance as text
%        str2double(get(hObject,'String')) returns contents of Tolerance as a double


% --- Executes during object creation, after setting all properties.
function Tolerance_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tolerance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in refinefit.
function refinefit_Callback(hObject, eventdata, handles)
% hObject    handle to refinefit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
update_gui_fit_to_start(handles)


%************************************************
function update_function_gui(handles,functionnum)

a=handles.fitfunctions.twoD;
%Insert the function name in the popup
set(handles.FittingFilenamePopup,'String',a.names)
%Set it to the one specific one indicated
set(handles.FittingFilenamePopup,'Value',functionnum)
%set(handles.FittingFilenamePopup,'TooltipString',a.name_help{functionnum}) 
set(handles.refinefit,'Enable','off')

%now set the fields according to the how many parameters in functionnum
for ii=1:a.numparams{functionnum}
    if (a.param_bool{functionnum}{ii}==1) % the parameter is boolean
        eval(['set(handles.par',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.par',num2str(ii),',''String'',[a.param_name{functionnum}{ii},'' bool'']);'])
        %    eval(['set(handles.par',num2str(ii),',''TooltipString'',a.param_help{functionnum}{ii});'])
        eval(['set(handles.fit',num2str(ii),',''Visible'',''off'');'])
        eval(['set(handles.fit',num2str(ii),',''Value'',0.0);'])
        eval(['set(handles.startval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.startval',num2str(ii),',''String'',''0'');'])
        eval(['set(handles.startval',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Enable'',''off'');'])
        eval(['set(handles.lowlim',num2str(ii),',''String'',''0'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.hilim',num2str(ii),',''Enable'',''off'');'])
        eval(['set(handles.hilim',num2str(ii),',''String'',''0'');'])
        eval(['set(handles.hilim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''String'','' '');'])
        eval(['set(handles.fitval',num2str(ii),',''Visible'',''on'');'])
    else %the parameter is not boolean
        eval(['set(handles.par',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.par',num2str(ii),',''String'',a.param_name{functionnum}{ii});'])
        %    eval(['set(handles.par',num2str(ii),',''TooltipString'',a.param_help{functionnum}{ii});'])
        eval(['set(handles.fit',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.fit',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.fit',num2str(ii),',''Value'',1.0);'])
        eval(['set(handles.startval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.startval',num2str(ii),',''String'','' '');'])
        eval(['set(handles.startval',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.lowlim',num2str(ii),',''String'',''-Inf'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.hilim',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.hilim',num2str(ii),',''String'',''Inf'');'])
        eval(['set(handles.hilim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''String'','' '');'])
        eval(['set(handles.fitval',num2str(ii),',''Visible'',''on'');'])
    end

end

for ii=a.numparams{functionnum}+1:9
    eval(['set(handles.par',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.par',num2str(ii),',''String'',[''#',num2str(ii),''']);'])
%    eval(['set(handles.par',num2str(ii),',''TooltipString'','' '';'])
    eval(['set(handles.fit',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.fit',num2str(ii),',''Value'',0);'])
    eval(['set(handles.startval',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.startval',num2str(ii),',''String'','' '');'])
    eval(['set(handles.startval',num2str(ii),',''Visible'',''off'');'])
    eval(['set(handles.lowlim',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.lowlim',num2str(ii),',''String'','' '');'])
    eval(['set(handles.lowlim',num2str(ii),',''Visible'',''off'');'])
    eval(['set(handles.hilim',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.hilim',num2str(ii),',''String'','' '');'])
    eval(['set(handles.hilim',num2str(ii),',''Visible'',''off'');'])
    eval(['set(handles.fitval',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.fitval',num2str(ii),',''String'','' '');'])
    eval(['set(handles.fitval',num2str(ii),',''Visible'',''off'');'])
end


function fitpar=read_gui_vals(handles)

fitpar.display_output_type=get(handles.DisplayType,'Value');

fitpar.fitfilenum=get(handles.FittingFilenamePopup,'Value');
fitpar.fitnamelist=get(handles.FittingFilenamePopup,'String');
fitpar.name=fitpar.fitnamelist{fitpar.fitfilenum};

fitpar.weightingnum=get(handles.weightingfunc,'Value');
fitpar.weightingfunclist=get(handles.weightingfunc,'String');
fitpar.weighting=fitpar.weightingfunclist{fitpar.weightingnum};

fitpar.binning=2^(get(handles.BinningPopUp','Value')-1);
fitpar.incl_resolution=get(handles.calcres,'Value'); 
for ii=1:9 
    eval(['if strcmp(get(handles.par',num2str(ii),',''Enable''),''on'');'...
            'fitpar.fit(ii)=get(handles.fit',num2str(ii),',''Value'');'...
            'if ~isempty(str2num(get(handles.startval',num2str(ii),',''String'')));'...
                'fitpar.start(ii)=str2num(get(handles.startval',num2str(ii),',''String''));'...
            'else;'...
                'errordlg(''Start values must be given'');'...
                'return;'...
            'end;'...
            'if ~isempty(get(handles.par',num2str(ii),',''String''));'...
                'fitpar.param_name{ii}=get(handles.par',num2str(ii),',''String'');'...
            'else;'...
                'fitpar.param_name{ii}=[''p'',num2str(ii)];'...
            'end;'...
            'if ~isempty(str2num(get(handles.lowlim',num2str(ii),',''String'')));'...
                'fitpar.ll(ii)=str2num(get(handles.lowlim',num2str(ii),',''String''));'...
            'else;'...
                'fitpar.ll(ii)=-Inf;'...
            'end;'...
            'if ~isempty(str2num(get(handles.hilim',num2str(ii),',''String'')));'...
                'fitpar.ul(ii)=str2num(get(handles.hilim',num2str(ii),',''String''));'...
            'else;'...
                'fitpar.ul(ii)=Inf;'...
            'end;'...
           'end;'])
end
fitpar.numiter=str2num(get(handles.num_of_iter,'String'));
if isempty(fitpar.numiter); fitpar.numiter=100; end
fitpar.tol=str2num(get(handles.Tolerance,'String'));
if isempty(fitpar.tol); fitpar.tol=1e-4; end
fitpar.graphics=get(handles.Graphics_on,'Value');


%************************************************
function update_gui_fitted(handles,Fitparam,Fitval)

drawnow
numparams=length(Fitparam);

%now set the fields according to the how many parameters in functionnum
for ii=1:numparams
    eval(['set(handles.fitval',num2str(ii),',''Enable'',''on'');'])
    eval(['set(handles.fitval',num2str(ii),',''String'',num2str(Fitparam(',num2str(ii),'),''%4.2e''));'])
    eval(['set(handles.fitval',num2str(ii),',''Visible'',''on'');'])
end

set(handles.chisqval,'String',num2str(Fitval,3))
set(handles.refinefit,'Enable','on')

%************************************************
function update_gui_fit_to_start(handles)

fitfilenum=get(handles.FittingFilenamePopup,'Value');
numparams=handles.fitfunctions.twoD.numparams{fitfilenum};

%now set the fields 
for ii=1:numparams
    eval(['a=get(handles.fitval',num2str(ii),',''String'');'])
    eval(['set(handles.startval',num2str(ii),',''String'',a);'])
end

function startval_input_check(handles,ii)
fitfilenum=get(handles.FittingFilenamePopup,'Value');
if handles.fitfunctions.twoD.param_bool{fitfilenum}{ii}
    txt=['if isempty(str2num(get(handles.startval',num2str(ii),',''String'')));'...
              'errordlg(''StartVal for Boolean Variable must be 0 or 1 !'',''Input Error'');'...
              'set(handles.startval',num2str(ii),',''String'',''0'');'...
              'return;'...
        'end;' ];
    eval(txt)
    txt=['if str2num(get(handles.startval',num2str(ii),',''String''))~=1 &&'...
              'str2num(get(handles.startval',num2str(ii),',''String''))~=0;'...
            'errordlg(''StartVal for Boolean Variable must be 0 or 1 !'',''Input Error'');'...
        'set(handles.startval',num2str(ii),',''String'',''0'');'...
        'return;'...
        'end;' ];
    eval(txt)
    txt=['set(handles.hilim',num2str(ii),',''String'',get(handles.startval',num2str(ii),',''String''));'];
    eval(txt)
    txt=['set(handles.lowlim',num2str(ii),',''String'',get(handles.startval',num2str(ii),',''String''));'];
    eval(txt)
else
    txt=['if isempty(str2num(get(handles.startval',num2str(ii),',''String'')));'...
        'errordlg(''StartVal must be a number!'',''Input Error'');'...
        'set(handles.startval',num2str(ii),',''String'','' '');'...
        'end;' ];
    eval(txt)
    return
end


function lowlim_input_check(handles,ii)

txt=['if isempty(str2num(get(handles.lowlim',num2str(ii),',''String'')));'...
         'errordlg(''LowLim must be real number or -Inf'',''Input Error'');'...
         'set(handles.lowlim',num2str(ii),',''String'',''-Inf'');'...
     'end;'...
     'if strcmp(get(handles.lowlim',num2str(ii),',''String''),''Inf'')'...
              ' || strcmp(get(handles.lowlim',num2str(ii),',''String''),''+Inf'');'... 
         'errordlg(''LowLim cannot be +Inf'',''Input Error'');'...
         'set(handles.lowlim',num2str(ii),',''String'',''-Inf'');'...
     'end;'];
eval(txt)

function hilim_input_check(handles,ii)

txt=['if isempty(str2num(get(handles.hilim',num2str(ii),',''String'')));'...
         'errordlg(''HiLim must be real number or +Inf'',''Input Error'');'...
         'set(handles.hilim',num2str(ii),',''String'',''+Inf'');'...
     'end;'...
     'if strcmp(get(handles.hilim',num2str(ii),',''String''),''-Inf'')'...
         'errordlg(''HiLim cannot be -Inf'',''Input Error'');'...
         'set(handles.hilim',num2str(ii),',''String'',''+Inf'');'...
     'end;'];
eval(txt)



function lowlim1_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,1)

function lowlim1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim2_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,2)

function lowlim2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim3_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,3)

function lowlim3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim4_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,4)

function lowlim4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim5_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,5)

function lowlim5_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim6_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,6)

function lowlim6_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim7_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,7)

function lowlim7_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim8_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,8)

function lowlim8_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim9_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,9)

function lowlim9_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim1_Callback(hObject, eventdata, handles)
hilim_input_check(handles,1)

function hilim1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function hilim2_Callback(hObject, eventdata, handles)
hilim_input_check(handles,2)

function hilim2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim3_Callback(hObject, eventdata, handles)
hilim_input_check(handles,3)

function hilim3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function hilim4_Callback(hObject, eventdata, handles)
hilim_input_check(handles,4)

function hilim4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim5_Callback(hObject, eventdata, handles)
hilim_input_check(handles,5)

function hilim5_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim6_Callback(hObject, eventdata, handles)
hilim_input_check(handles,6)

function hilim6_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim7_Callback(hObject, eventdata, handles)
hilim_input_check(handles,7)

function hilim7_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim8_Callback(hObject, eventdata, handles)
hilim_input_check(handles,8)

function hilim8_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim9_Callback(hObject, eventdata, handles)
hilim_input_check(handles,9)


function hilim9_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function top_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to top_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function top_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to top_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in calcres.
function calcres_Callback(hObject, eventdata, handles)
% hObject    handle to calcres (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of calcres
%calcresval=get(handles.calcresfunc,'Value');
%update_gui_new(handles,calcresval); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLOR SCALE CONTROL CALLBACKS AND UTILITIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_slider(slider, gui)
% Callback for slider controlling top of color scale.
% top_text calls this too.
top = max(1,fix(get(slider, 'Value')));
set(slider, 'Value', top)
set(gui.top_text, 'String', num2str(top))
set([gui.axes1], 'CLim', [0 top])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_text(edit, gui)
% Callback for text edit box controlling top of color scale.
num = str2double(get(edit, 'String'));
if ~ isnan(num)  % we have a number
    slider = gui.top_slider;
    if num >= get(slider, 'Min') && num <= get(slider, 'Max')
        set(slider, 'Value', num)
    end
    top_slider(slider, gui)  % update everything
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function lowlim82_CreateFcn(a,b,c)
function edit51_CreateFcn(a,b,c)
function edit53_CreateFcn(a,b,c)
function edit54_CreateFcn(a,b,c)
function edit55_CreateFcn(a,b,c)
function edit56_CreateFcn(a,b,c)
function edit57_CreateFcn(a,b,c)



% --- Executes on selection change in weightingfunc.
function weightingfunc_Callback(hObject, eventdata, handles)
% hObject    handle to weightingfunc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns weightingfunc contents as cell array
%        contents{get(hObject,'Value')} returns selected item from weightingfunc
%weightingfuncnum=get(handles.weightingfunc,'Value');
%update_gui_new(handles,weightingfuncnum); 



% --- Executes during object creation, after setting all properties.
function weightingfunc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to weightingfunc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in save2file.
function save2file_Callback(hObject, eventdata, handles)
% hObject    handle to save2file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hstatus=figstatus('Loading parameters',get(hObject,'Parent'));
fit.fitpar=read_gui_vals(handles);
fit.bounds=get(handles.rect, 'Position');
clearstatus
[filename, pathname] = uiputfile('*.mat','Save fitting session as file','fit.mat');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    save(fullfilename,'fit')
end

% --- Executes on button press in loadfromfile.
function loadfromfile_Callback(~, eventdata, handles, varargin)
% hObject    handle to loadfromfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if length(varargin)==1 
    fullfilename=varargin{1};
    load(fullfilename)
else
    [filename, pathname] = uigetfile('*.mat','Load fitting session file','*.*');
    if isstr(filename)
        fullfilename=fullfile(pathname,filename);
        load(fullfilename)
    end
end
set(handles.rect,'Position',fit.bounds);
handles=set_gui_vals(fit.fitpar,handles);
set(handles.xleft, 'String',num2str(fit.bounds(1)));
set(handles.xright, 'String',num2str(fit.bounds(1)+fit.bounds(3)));
set(handles.ybottom, 'String',num2str(fit.bounds(2)));
set(handles.ytop, 'String',num2str(fit.bounds(2)+fit.bounds(4)));
position_rectangle(handles)


function handles=set_gui_vals(fitpar,handles)

set(handles.FittingFilenamePopup,'Value',fitpar.fitfilenum);
set(handles.FittingFilenamePopup,'String',fitpar.fitnamelist);

try   % tests if the latest parameter is included (to make backward compatible)
    set(handles.DisplayType,'Value',fitpar.display_output_type);
catch
    set(handles.DisplayType,'Value',1);
end
update_function_gui(handles,fitpar.fitfilenum);

set(handles.weightingfunc,'Value',fitpar.weightingnum);
set(handles.weightingfunc,'String',fitpar.weightingfunclist);

set(handles.BinningPopUp','Value',log2(fitpar.binning)+1);
set(handles.calcres,'Value',fitpar.incl_resolution); 

for ii=1:length(fitpar.fit)
    eval(['set(handles.par',num2str(ii),',''Enable'',''on'');'...
            'set(handles.fit',num2str(ii),',''Value'',fitpar.fit(ii));'...
            'set(handles.startval',num2str(ii),',''String'',num2str(fitpar.start(ii)));'...
            'set(handles.lowlim',num2str(ii),',''String'',num2str(fitpar.ll(ii)));'...
            'set(handles.hilim',num2str(ii),',''String'',num2str(fitpar.ul(ii)));'])
end

set(handles.num_of_iter,'String',num2str(fitpar.numiter));
set(handles.Tolerance,'String',num2str(fitpar.tol));
set(handles.Graphics_on,'Value',fitpar.graphics);


function clearstatus
fig=findobj(get(0,'Children'),'Name','Fitting2D');
%delete any old status message.
hstatusold=findobj(fig,'Tag','statusline');
if ~isempty(hstatusold)
    delete(hstatusold)
end



% --- Executes on selection change in DisplayType.
function DisplayType_Callback(hObject, eventdata, handles)
% hObject    handle to DisplayType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns DisplayType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DisplayType

% This function defines which output plot functions is to be
% used...through 


% --- Executes during object creation, after setting all properties.
function DisplayType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DisplayType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



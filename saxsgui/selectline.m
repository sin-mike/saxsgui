function h_out = selectline(haxes)
%SELECTLINE Select from a set of line objects by appearance.
%   SELECTLINE(HAXES) asks you to choose a line from the axes by clicking
%   on the line.
%
%   SELECTLINE always returns a handle to line object you selected or [] if
%   you didn't select one.

hfig = get(haxes, 'Parent');  % parent figure window
hlines = findobj(haxes, 'Tag', 'Data');  % all lines in the axes
if isempty(hlines)  % no lines?
    h_out = [];  % return empty
    return
end
if length(hlines)==1  % one line
    h_out = hlines;  % return empty
    return
end
set(hlines, 'ButtonDownFcn', @click)  % install callbacks for line clicks
hstatus = figstatus(...
    'Click on a plotted line, or click on this message to cancel.', ...
    hfig);  % tell user what to do in a status bar
set(hstatus, ...
    'ButtonDownFcn', @cancel, 'Enable', 'inactive', ...  % capture status bar click
    'Tag', 'selectline_statusbar')  % tag our status bar
uiwait(hfig)  % wait for action

if ishandle(hstatus)  % status bar still here
    set(hlines, 'ButtonDownFcn', [])  % remove callbacks for line clicks
    h_out = get(hstatus, 'UserData');  % get our result
    delete(hstatus)  % delete status bar
else  % status bar gone; figure window probably closed
    h_out = [];  % return empty
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function click(hline, eventdata)
% User clicked line, store handle in status bar UserData.
hfig = get(get(hline, 'Parent'), 'Parent');
hbar = findobj(hfig, 'Tag', 'selectline_statusbar');  % find our status bar
set(hbar, 'UserData', hline)  % store the line handle there
uiresume  % continue main function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cancel(hbar, eventdata)
% User clicked status bar, cancel line selection.
set(hbar, 'UserData', [])  % no selection
uiresume  % continue main function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function    batch_xygrid_GUIupdate(handles)
global batch_xygrid_temp
% this routine updates the window where Batch parameters can be entered
% to reflect the changes that are made interactively

% hObject is the handle for the figure window
% handles are all the handles referred to by name f.eks. handle.sample
% reduc_par_temp is a structure that holds all the relevant values
% 
% this routine only changes the GUI it does not change any underlying
% parameter values in the structure
rdt=batch_xygrid_temp; %

set(handles.Show_Transm,'Value',rdt.Show_Transm)
set(handles.Show_SAXS_Int,'Value',rdt.Show_SAXS_Int)
set(handles.show_orientation,'Value',rdt.show_orientation)
set(handles.ShowQuiver,'Value',rdt.ShowQuiver)

set(handles.Xnum,'String',num2str(rdt.Xnum))
set(handles.Ynum,'String',num2str(rdt.Ynum))
set(handles.qmin,'String',num2str(rdt.qmin))
set(handles.qmax,'String',num2str(rdt.qmax))

set(handles.Save2DImages,'Value',rdt.Save2DImages)
set(handles.Save1DFigures,'Value',rdt.Save1DFigures)

set(handles.ReducedDataToSeparateFiles,'Value',rdt.ReducedDataToSeparateFiles)
set(handles.ReducedDataToSameFile,'Value',rdt.ReducedDataToSameFile)
set(handles.xyformat,'Value',rdt.xyformat)
set(handles.xydeltayformat,'Value',rdt.xydeltayformat)

set(handles.DescriptiveTagString,'String',rdt.DescriptiveTagString)

if (rdt.ReducedDataToSeparateFiles==0 && rdt.ReducedDataToSameFile==0)
    set(handles.xyformat,'Enable','Off')
    set(handles.xydeltayformat,'Enable','Off')
    set(handles.DescriptiveTagString,'Enable','Off')
    set(handles.text46,'Enable','Off')
else
    set(handles.xyformat,'Enable','On')
    set(handles.xydeltayformat,'Enable','On')
    set(handles.DescriptiveTagString,'Enable','On')
    set(handles.text46,'Enable','On')
end

files=get(handles.ChooseFiles,'UserData');
if isempty(files)
    set(handles.StartBatch,'Enable','Off')
    set(handles.Test_Files,'Enable','Off')
    set(handles.Show_Special,'Enable','Off')
    set(handles.Save_Special,'Enable','Off')
    set(handles.NumFilesChosen,'String','Number of files chosen: 0')
elseif ~exist(files.path)
    set(handles.StartBatch,'Enable','Off')
    set(handles.Test_Files,'Enable','Off')
    set(handles.Show_Special,'Enable','Off')
    set(handles.Save_Special,'Enable','Off')
    set(handles.NumFilesChosen,'String','Number of files chosen: 0')
else
    set(handles.StartBatch,'Enable','On')
    set(handles.Test_Files,'Enable','On')
    set(handles.Show_Special,'Enable','On')
    set(handles.Save_Special,'Enable','On')
    set(handles.NumFilesChosen,'String',['Number of files chosen: ',num2str(length(files.names))])
end



function varargout = saxsgui_export(varargin)
%SAXSGUI_EXPORT Used by SAXSGUI to export text files.
%   SAXSGUI_EXPORT(S) exports text data from SAXS image object S.

%      SAXSGUI_EXPORT, by itself, creates a new SAXSGUI_EXPORT or raises the existing
%      singleton*.
%
%      H = SAXSGUI_EXPORT returns the handle to a new SAXSGUI_EXPORT or the handle to
%      the existing singleton*.
%
%      SAXSGUI_EXPORT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SAXSGUI_EXPORT.M with the given input arguments.
%
%      SAXSGUI_EXPORT('Property','Value',...) creates a new SAXSGUI_EXPORT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before saxsgui_export_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to saxsgui_export_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Last Modified by GUIDE v2.5 23-May-2003 09:25:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @saxsgui_export_OpeningFcn, ...
                   'gui_OutputFcn',  @saxsgui_export_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before saxsgui_export is made visible.
function saxsgui_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to saxsgui_export (see VARARGIN)

% Find the SAXS input argument.
if (length(varargin) < 1) || (~ isa(varargin{1}, 'saxs'))
    delete(hObject)  % delete our figure
    error('I need a SAXS object.')
end
handles.saxs = varargin{1};

% Set x and y ranges.
xrange = xaxis(handles.saxs);
yrange = yaxis(handles.saxs);
set(handles.horizlow, 'String', num2str(xrange(1)));
set(handles.horizhigh, 'String', num2str(xrange(2)));
set(handles.vertlow, 'String', num2str(yrange(1)));
set(handles.verthigh, 'String', num2str(yrange(2)));

% Choose default command line output for saxsgui_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes saxsgui_export wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = saxsgui_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in triplets.
function triplets_Callback(hObject, eventdata, handles)
% hObject    handle to triplets (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
choose_format(hObject, handles)


% --- Executes on button press in xyztable.
function xyztable_Callback(hObject, eventdata, handles)
% hObject    handle to xyztable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
choose_format(hObject, handles)


% --- Executes on button press in params.
function params_Callback(hObject, eventdata, handles)
% hObject    handle to params (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of params
choose_format(hObject, handles)


function choose_format(button, handles)
buttons = [handles.triplets, handles.xyztable, handles.params];
choose_button(button, buttons)


function choose_button(button, buttons)
buttons = buttons(buttons ~= button);
set(buttons, 'Value', 0)
set(button, 'Value', 1)


% --- Executes during object creation, after setting all properties.
function horizlow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to horizlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


function horizlow_Callback(hObject, eventdata, handles)
% hObject    handle to horizlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of horizlow as text
%        str2double(get(hObject,'String')) returns contents of horizlow as a double
checknum(hObject)


% --- Executes during object creation, after setting all properties.
function horizhigh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to horizhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


function horizhigh_Callback(hObject, eventdata, handles)
% hObject    handle to horizhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of horizhigh as text
%        str2double(get(hObject,'String')) returns contents of horizhigh as a double
checknum(hObject)


% --- Executes during object creation, after setting all properties.
function vertlow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vertlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


function vertlow_Callback(hObject, eventdata, handles)
% hObject    handle to vertlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of vertlow as text
%        str2double(get(hObject,'String')) returns contents of vertlow as a double
checknum(hObject)


% --- Executes during object creation, after setting all properties.
function verthigh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to verthigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


function verthigh_Callback(hObject, eventdata, handles)
% hObject    handle to verthigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of verthigh as text
%        str2double(get(hObject,'String')) returns contents of verthigh as a double
checknum(hObject)


% --- Executes on button press in doexport.
function doexport_Callback(hObject, eventdata, handles)
% hObject    handle to doexport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
path = filefromuser;
if path
    write(crop(handles.saxs, cropbox(handles)), whattext(handles), ...
        numformat(handles), path)
end
close(handles.figure1)

% --- Executes on button press in docancel.
function docancel_Callback(hObject, eventdata, handles)
% hObject    handle to docancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)


% --- Executes on button press in dragrange.
function dragrange_Callback(hObject, eventdata, handles)
% hObject    handle to dragrange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function digits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to digits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


function digits_Callback(hObject, eventdata, handles)
% hObject    handle to digits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of digits as text
%        str2double(get(hObject,'String')) returns contents of digits as a double
ndigits = str2double(get(hObject,'String'));
if isnan(ndigits) || (ndigits < 0)
    errordlg('Enter a number greater than or equal to zero.', 'Number of digits')
    ndigits = 1;
else
    ndigits = fix(ndigits);
end
set(hObject, 'String', num2str(ndigits))


function numdigits = digits(handles)
% Return number of digits to the right of the decimal point, as specified
% in the 'digits' text edit box.  digits_Callback validates the text
% string, so we needn't here.
numdigits = str2double(get(handles.digits, 'String'));


function checknum(hEdit)
% Check that edit box contains a numeric representation.
num = str2double(get(hEdit, 'String'));
if isnan(num)
    errordlg('That is not a number.  Enter a number or trouble may ensue.', 'Not a number')
end


function path = filefromuser
% Return file path (directory and name) obtained from user, 
% or 0 if none to be had (like user cancelled).
[name dir] = uiputfile('*.txt', 'Export text file');
if name
    path = [dir name];
else
    path = 0;
end


function what = whattext(handles)
% Determine what text to output.
for button = [handles.triplets, handles.xyztable, handles.params]
    if get(button, 'Value')  % button selected
        what = get(button, 'Tag');  % 'triplets', 'xyztable', or 'params'
        break
    end
end


function fmt = numformat(handles)
% Create a numeric format string.
fmt = ['%.', get(handles.digits, 'String'), 'f'];


function box = cropbox(handles)
% Return [xmin, ymin; xmax, ymax] user crop coordinates.
box = [str2double(get(handles.horizlow, 'String')), ...
        str2double(get(handles.vertlow, 'String')); ...
        str2double(get(handles.horizhigh, 'String')), ...
        str2double(get(handles.verthigh, 'String'))];


function write(saxsobj, what, fmt, where)
% Write WHAT text from SAXOBJ image to file WHERE.  Use FMT for numeric
% output.
switch what
    case 'triplets'
        asciiwrite(saxsobj, where, fmt)
    case 'xyztable'
        xyzwrite(where, xyztable(saxsobj), fmt)
    case 'params'
        paramwrite(saxsobj, where)
end

function    ffm_par_out=ffm_par_init
global ffm_par_temp
% this routine provides initial parameters for the reduction parameter edit
% windows.
% Somewhere in the future it may be able to "know" what was previously
% entered in the last session...but for now it knows what was entered last
% in this session

if ~isempty(ffm_par_temp) 
    ffm_par_out=ffm_par_temp;
    ffm_par_temp=ffm_par_out;
else
    
    a.lowint_check=1;
    a.lowint_trans=1.0;
    a.lowintname='filename';
    
    a.mask_check=1;
    a.maskname='filename';
    
    a.highint_check=1;
    a.highintname='filename';
    a.highintcenname='filename';
    
    a.empty_check=0;
    a.empty_trans=1.0;
    a.emptyname='filename';
    
    a.dark_check=0;
    a.darkcurrentname='filename';
    
    ffm_par_out=a;
    ffm_par_temp=ffm_par_out;
end
    
function display(s)
%SAXS/DISPLAY Display characteristics of a saxs image object.

disp(' ');
disp([inputname(1),' = '])
disp(' ');
if length(s) == 1
    
    disp('    saxs image object')
    disp(' ')
    disp(paramstr(s))
else  % array size other than 1: rely on char method to display
    disp(['   ' char(s)])
end
disp(' ');

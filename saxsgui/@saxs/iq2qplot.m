function h = iq2qplot(s, varargin)
%%SAXS/IQ2QPLOT Plot a Iq^2 versus q plot of 1D SAXS object


x = s.xaxisfull;

% calibrated
if s.kcal
    distance_units = 'q (A^-^1)';   %was originally K, changed by KJ April 12, 2005
else
    distance_units = 'pixels';
end
%do we know the time? if not assume 1 second
if s.livetime
    if strcmp(s.reduction_state(1:2),'11') 
      ylabeltext='I*q^2 (Absolute intensity*q^2 (cm^-1 A^-^2)';
    else
      ylabeltext='I*q^2 (average intensity*q^2 (counts per mr^2 per second * A^-^2)';
    end
    livetime=s.livetime;
else
    ylabeltext='I*q^2';
    livetime=1;
end

 
type = s.type;
switch type
    case 'r-ave'
        hplot = semilogy(x, s.pixels/livetime, varargin{:});% added by KJ April 12, 2005 normalizes;

        lastline=findobj(gcf,'Type','line');  %This returns all the line in the plot but last is always element number 1
        set(lastline(1),'UserData',s); %saving saxsobj in data in the UserData variable
        set(lastline(1),'Tag','Data'); %so we can find it later when we want to change display
% this seems to work for both new and added lines.
        axes(get(hplot, 'Parent'));
        xlabel(['momentum transfer, ', distance_units])
        ylabel(ylabeltext)
        xdata=get(hplot,'Xdata');
        ydata=get(hplot,'Ydata');
        ydatanew=ydata.*xdata.^2;
        set(hplot,'Ydata',ydatanew)
    case 'th-ave' % should not happen
        errordlg('This plottype does not apply to Azimuthal averages')

    case 'x-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['x, ', distance_units])
        ylabel(ylabeltext)
    case 'y-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['y, ', distance_units])
        ylabel(ylabeltext)
    otherwise
        error('This is the wrong type of SAXS image for PLOT.  Try IMAGE.')
end




if nargout
    h = hplot;
end
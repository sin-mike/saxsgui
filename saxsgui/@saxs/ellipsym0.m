function objval = ellipsym0(sobj, bounds)
%SAXS/ELLIPSYM0 Compute elliptical symmetry of polar SAXS object.
%   ELLIPSYM0 does the computational work for ELLIPSYM (see that function).
%   A symmetry search might call ELLIPSYM0 directly.
%
%   VAL = ELLIPSYM0(SOBJ, BOUNDS) where SOBJ is SAXS object in polar
%   coordinates (type 'rth') and BOUNDS is a 2x2 array

% Hey wait.  Doing this in polar form samples the central image features
% more heavily than the outlying features.  As far as this objective value
% goes we could as easily use the image in rectangular coordinates.  It's
% the rotation that is easier in polar form.

if th <= 0
    th = th + 90;
end
if th > 90
    th = th - 90;
end

pix = S.pixels;
obs = 0;
for i = 1 : 2  % 1 -> x axis, 2 -> y axis
    th = th + (i - 1) * 90;  % rotate 90 degrees for y axis
    thpix = fix(th / thstep);
    th180pix = fix(180 / thstep);
    top = pix(thpix : thpix + th180pix - 1, :);  % top or left half of data
    if th == 1
        bot = flipud(pix(th180pix + 1 : end - 1, :));
    else
        bot = flipud([pix(thpix + th180pix : end - 1, :); pix(1 : thpix - 1, :)]);
    end  % bottom or right half of data at given rotation
    obs = obs + sum(sum((top - bot) .^ 2));
end

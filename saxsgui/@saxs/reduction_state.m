function Sred=reduction_state(S,handles);
%SAXS/REDUCTION_STATE Calculate reduction state based on gui values

Sred=S;
if nargin==1
    reduction_state=0;
else
    reduction_state=0;
    if get(handles.mask_cor,'Value')==1, reduction_state=reduction_state+8; end
    if get(handles.flatfield_cor_ave,'Value')==1, reduction_state=reduction_state+16; end
    if get(handles.flatfield_cor_raw,'Value')==1, reduction_state=reduction_state+32; end
    if get(handles.sampletrans_cor,'Value')==1, reduction_state=reduction_state+64; end
    if get(handles.empty_sub,'Value')==1, reduction_state=reduction_state+128; end
    if get(handles.darkcurrent_sub,'Value')==1, reduction_state=reduction_state+256; end
    if get(handles.absolute_cor,'Value')==1, reduction_state=reduction_state+512; end
    if get(handles.samplethick_cor,'Value')==1, reduction_state=reduction_state+1024; end
    if get(handles.readout_sub,'Value')==1, reduction_state=reduction_state+4; end
    if get(handles.zinger_cor,'Value')==1, reduction_state=reduction_state+2; end
    %if get(handles.solvent_sub,'Value')==1, reduction_state=reduction_state+1; end   
end

if reduction_state==0;
    Sred.reduction_state=[];
else
    Sred.reduction_state=num2str(dec2bin(reduction_state,11)); %reduction_state is a string
end
function S=strip_reduction_pixels(S);
% SAXS/STRIP_REDUCTION_PIXELS strips the 2D reduction data from the saxs-structure...
% This data was put on the the saxs structure allow averaging...
% now that averaging is done, it is no longer needed..and just fills up
% memeory

S.mask_pixels=[];
S.flatfield_pixels=[];
S.empty_pixels=[];
S.darkcurrent_pixels=[];
S.solvent_pixels=[];
function so = xsum(si)
%SAXS/XSUM Collapse a SAXS image to a summed intensity plot along the x axis.
%   XSUM(S) returns a new SAXS image, a collapsed version of S, plotting
%   the sum of intensities in each column of S along the x axis.

so = si;
so.pixels(isnan(so.pixels)) = 0;  % replace NaNs with zeros
so.pixels = sum(so.pixels);
if isempty(so.type)
    so.type = 'xsum';
else
    switch so.type
        case 'xy'
            so.type = 'xsum';
        case 'rth'
            so.type = 'rsum';
        otherwise
            so.type = 'xsum';  % ??
    end
end
event = so.type;
so.history = strvcat(so.history, event);

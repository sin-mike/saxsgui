function argout = empty_filename(s, name)
%SAXS/EMPTY_FILENAME Set or retrieve empty_filename for SAXS image.
%   EMPTY_FILENAME(S) returns the empty_filename for SAXS image S.
%
%   empty_FILENAME(S, NAME) returns a SAXS image object copied from S with
%   empty_filename name.

switch nargin
case 1  % return transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.empty_filename;
case 2  % set transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isstr(name)  == 1
        argout.empty_filename = name;
        if ~isempty(name)
            argout.history = strvcat(argout.history, ['empty_filename ', name]);
        end
    else
        error('The empty_filename is not a string.')
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end

function so=insert_saxs_pixelval(si,ypixel,xpixel,pixelval)
%SAXS/INSERT_SAXS_PIXELVAL this function simply inserts a pixel-value in the SAXS structure.
% so=insert_saxs_pixelval(si,si,ypixel,xpixel,pixelval))
so=si;
so.pixels(ypixel,xpixel)=pixelval;



function argout = livetime(s, live)
%SAXS/LIVETIME Retrieve live time for SAXS image.
%   LIVETIME(S) returns the live time recorded for SAXS image S.

switch nargin
case 1  % return livetime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.livetime;
case 2  % set livetime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isnumeric(live) & length(live) == 1
        argout.livetime = live;
    else
        error('I do not understand this LIVETIME argument.')
    end
    argout.history = strvcat(argout.history, ['livetime ', num2str(live)]);
otherwise  % we don't understand
    error('I need one or two arguments.')
end

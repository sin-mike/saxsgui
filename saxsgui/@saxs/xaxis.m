function argout = xaxis(varargin)
%SAXS/XAXIS Set or retrieve x axis scale for a SAXS image object.
%   XAXIS(S, X) returns a new SAXS object built from SAXS object S but with
%   a new x axis scale X.  X is a vector of two numbers in ascending order,
%   corresponding to the first and last column of the image in S.
%
%   XAXIS(S) returns the x axis vector from SAXS object S.

argout = saxis(1, varargin{:});

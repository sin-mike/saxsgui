function savecenter(s, name)
%SAXS/SAVECENTER Save center coordinates of SAXS image.
%   SAVECENTER(S, 'name') saves the center coordinates of SAXS image object
%   S to a file named name.mat.  Use SAVECENTER and LOADCENTER to reuse
%   image centers for multiple images.
%
%   See also SAXS/LOADCENTER.

if nargin ~= 2
    error('I need exactly two arguments.')
elseif ~ (isa(s, 'saxs') & ischar(name))
    error('I want a SAXS object and a file name for arguments.')
end

c = pixelfrac(s, [0 0]);
save(name, 'c')

function [s, c] = center(s0, varargin);
%SAXS/CENTER Calculates Center of a SAXS image object.
%   CENTER(S) searches SAXS image object S for a symmetric center,
%   returning the centered SAXS image object.  See SAXS/CENTER-NOTES for a
%   brief description of the search techniques.  CENTER changes xaxis and
%   yaxis values in the SAXS object so that the axes origin is at the
%   computed center of the image.
%
%   [SC, C] = CENTER(S) returns the centered SAXS object SC and the
%   computed center C in raw pixel coordinates. C is a two-element vector
%   [x, y].  You can also obtain the current center of an image in raw
%   pixel coordinates using PIXELCENTER(S).
%
%   Use MASK to change the area around a prospective center that CENTER
%   ignores while evaluating symmetry.  See SAXS/MASK.
%
%   CENTER(S, D) specifies the distance away from a prospective center
%   that CENTER will search for symmetry in x and y directions.  By default
%   CENTER tries to establish a reasonable value for D given the size of
%   the image.  Smaller D values reduce computation time.
%
%   CENTER(S, C) returns the SAXS image object centered at C. C is a
%   two-element vector [x, y] specifying the new center in raw pixel
%   coordinates.  CENTER does not perform a symmetry search. Use this form
%   if you know the image center or if you want to help a subsequent call
%   to CENTER by specifying the approximate center.
%
%   CENTER(S, ..., 'plot') plots the SAXS image along with the search
%   points evaluated.  Use this to analyze CENTER's performance.
%
%   CENTER(S, ..., 'nostatus') suppresses the normal status messages CENTER
%   displays in the command window while searching.
%
%   See also SAXS, SAXS/MASK, SAXS/XAXIS, SAXS/YAXIS, SAXS/CENTER-NOTES,
%   SAXS/PIXELCENTER.

% Sanity check.
if ~ isa(s0, 'saxs')
    error('The first argument must be a SAXS object.')
end

% Parameters.
show = 0;  % default don't show image and test points
status_update = 1;  % default print status updates as we go
search_dist = floor(min(300,min(size(s0.pixels)/4)));  % default 300-pixel quadrants for symmetry evaluations
center = [];  % default unknown center

% Note to self:  these belong in parameter file?

% Parse optional input arguments.
for arg = varargin
    arg = arg{1};  % strip off cell type
    if ischar(arg)
        switch arg
            case 'plot'
                show = 1;
            case 'nostatus'
                status_update = 0;
            otherwise
                error(['I don''t understand ''', arg, ''''])
        end
    elseif isnumeric(arg)
        if length(arg) == 1  % search distance
            search_dist = arg;
        elseif isequal(size(arg), [1, 2])  % center [x, y]
            center = arg;
        else
            error('I don''t understand those arguments.')
        end
    else
        error('I don''t understand those arguments.')
    end
end

if center  % center specified, not to be searched
    c = center;
else  % search for it
    %try
        c = findcenter(s0, search_dist, status_update, show);
    %catch
    %    warning('Auto-center routine failed')
    %end
end
s = setcenter(s0, c);

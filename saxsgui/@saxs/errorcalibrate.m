function S = errorcalibrate(Sin,error_norm_fact)
%SAXS/ERRORCALIBRATE Calibrate a SAXS image to obtain the error normalization
%factor.
%   SCAL = ERRORCALIBRATE(S,error_norm_fact) returns SAXS object S
%   with error normalization calibrated.  
%   
%   Error normalization factor is the factor that must be multiplied to the
%   a-priori calculated errors to obtain the proper error estimates.
%   For gas-detectors with pixel values following the normal distribution
%   the factor is equal to 1.
%   In for example CCD...a value of 100 does not correspond to 100 photons
%   and thus we would expect a different error than if it did correspond to
%   100 photons. Let's say a value of 100 corresponded to 10000 photons. In
%   this case we would "calculate" the error to be 10 out of 100 (or 10%) 
%   but in reality it would be 100 out of 10000 photons (or 1%).
%   So in this case the error normalizatio factor would be 0.1.
%   Equivalently one could say that if a pixel value of 1 equal N photons
%   then the error normalization factor would be 1/sqrt(N).

S=Sin;
S.error_norm_fact=error_norm_fact;



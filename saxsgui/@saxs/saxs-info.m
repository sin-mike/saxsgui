%SAXS Information
%   SAXS objects store small angle x-ray scattering images, various
%   attributes, and track the history of manipulations on the image.
%   Create SAXS objects from MPA disk files with GETSAXS or MPAREAD.
%   Create SAXS objects directly from a numeric array with SAXS itself.
%
%   SAXS objects display themselves on the command line by listing their
%   attributes, including their history of manipulations.
%
%   To extract the pixel array from a SAXS object, convert the object to
%   DOUBLE.  Pixel subsets can be read and written using index subscripts
%   with a SAXS object, such as S(:, 200:800).  If you write pixel values
%   this way, it is a very good idea to add a comment to the object with
%   HISTORY describing what you did.
%
%   If you want to group SAXS objects in arrays, use cell arrays.
%
%   Display SAXS images graphically with IMAGE.  This and other SAXS
%   methods extend standard MATLAB functions to work well with SAXS
%   objects.  To see the SAXS-specific help for a method, type HELP
%   SAXS/<METHOD>, such as HELP SAXS/IMAGE.
%
%   Some ordinary arithmetic operators can be used with SAXS objects.  They
%   operate on the pixels in the same way that the operators work with
%   numeric arrays in MATLAB, and they record their operations in the SAXS
%   object's history.  The operators are +, -, .*, and ./.  LOG and LOG10
%   are extended for SAXS objects to convert zero and NaN values to numbers
%   smaller than any other pixel values before taking the logarithm.
%
%   Type HELP SAXS-METHODS to see a list of public SAXS methods.
function sout = loadcenter(s, name)
%SAXS/LOADCENTER Load saved center coordinates into a SAXS image object.
%   S = LOADCENTER(S, 'name') loads saved center coordinates from file
%   name.mat and returns SAXS image object S centered at those coordinates.

if nargin ~= 2 || ~ isa(s, 'saxs') || ~ ischar(name)
    error('I need a SAXS object and a file name for arguments.')
end

try
    contents = whos('-file', name);
catch
    error([name, ' doesn''t seem to be the right kind of file'])
end

if length(contents) ~= 1 ...
        || ~ isequal(contents.name, 'c') ...
        || ~ isequal(contents.size, [1 2]) ...
        || ~ isequal(contents.class, 'double')
    error([name, ' doesn''t seem to be the right kind of file'])
end

load(name)
sout = center(s, c);

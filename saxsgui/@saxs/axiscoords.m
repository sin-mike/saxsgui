function coords = axiscoords(s, pixel)
%SAXS/AXISCOORDS Axis coordinates of a given pixel in a SAXS image.
%   COORDS = AXISCOORDS(S, PIXEL) returns coordinates [x, y] for a given
%   pixel [column, row] in SAXS image object S.
%   PIXEL may represent multiple pixels, one per row.
%   COORDS will become the same size.
%   Any pixel coordinates not in the image will return NaN values.

[nrows ncols] = size(s.pixels);
pixel(pixel < 1) = nan;
pixel(find(pixel(:, 1) > ncols), 1) = nan;
pixel(find(pixel(:, 2) > nrows), 2) = nan;

if isempty(s.xaxis)
    coords(:, 1) = pixel(:, 1);
else
    coords(:, 1) = axiscoord(s.xaxis, ncols, pixel(:, 1));
end
if isempty(s.yaxis)
    coords(:, 2) = pixel(:, 2);
else
    coords(:, 2) = axiscoord(s.yaxis, nrows, pixel(:, 2));
end


function value = axiscoord(axisrange, npixels, pixelnum)
step = (axisrange(2) - axisrange(1)) / (npixels - 1);
value = axisrange(1) + (pixelnum - 1) * step;

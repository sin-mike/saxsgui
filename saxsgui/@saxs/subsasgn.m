function a = subsasgn(s, subs, val)
%SAXS/SUBSASGN Handle indexed assignment to SAXS variables.

for sub = subs  % might be multiple levels of indexing
    switch sub.type
        case '()'
            s.pixels(sub.subs{:}) = val;
            event = 'pixels changed by direct assignment';
            s.history = strvcat(s.history, event);
        case '{}'
            s{sub.subs{:}} = val;
        case '.'
            error('You cannot access SAXS object fields directly.')
    end
end
a = s;
function s=get_reduction_file(s,field)
% SAXS/GET_REDUCTION_FILE Load data from a reduction file into
%s is a saxs image
%field indicated which type of  means there is only reporting when there is an error.

if strcmp(field,'mask')
    if ~isempty(s.mask_filename)
        if exist(s.mask_filename)
            a=load(s.mask_filename); %mask file contains 1's and 0's
            if ~isfield(a,'mask')
                errordlg(['mask file:', s.mask_filename,' does not contain variable "mask". Please make a new mask file'])
                return
            end
            if ~(size(a.mask)==size(s.raw_pixels)  )
                errordlg(['mask is the wrong size. Please choose a different file'])
                return
            end
            if ~islogical(a.mask)
                errordlg(['mask is not a logical file. Make new mask file '])
                return
            end
        else
            errordlg(['mask file:', s.mask_filename,' does not exist. Please correct'])
            return
        end
        warning off MATLAB:divideByZero
        s.mask_pixels=double(a.mask)./double(a.mask); % this transforms it into 1's and NaN's
        warning on MATLAB:divideByZero
    else
       s.mask_filename=[];
       
       s.mask_pixels=[];
    end
end

if strcmp(field,'flatfield')
    if ~isempty(s.flatfield_filename)
        if exist(s.flatfield_filename)
            a=getsaxs(s.flatfield_filename);
            if ~(size(a.pixels)==size(s.raw_pixels)  )
                errordlg(['flatfield is the wrong size. Please choose a different file'])
                return
            end
            s.flatfield_pixels=a.pixels;
        else
            errordlg(['flatfield file:', s.flatfield_filename,' does not exist. Please correct'])
            return
        end
    else
        s.flatfield_filename=[];
        s.flatfield_pixels=[];
    end
end

if strcmp(field,'empty')
    if ~isempty(s.empty_filename)
        if exist(s.empty_filename)
            a=getsaxs(s.empty_filename);
            if ~(size(a.pixels)==size(s.raw_pixels)  )
                errordlg(['empty holder file is the wrong size. Please choose a different file'])
                return
            end
            s.empty_pixels=a.pixels;
            s.empty_livetime = a.livetime;
        else
            errordlg(['empty file:', s.empty_filename,' does not exist. Please correct'])
            return
        end
    else
        s.empty_pixels=[];
        s.empty_livetime=[];
        s.empty_transfact=[];
        s.empty_filename=[];
    end
end

if strcmp(field,'solvent')
    if ~isempty(s.solvent_filename)
        if exist(s.solvent_filename)
            a=getsaxs(s.solvent_filename);
            if ~(size(a.pixels)==size(s.raw_pixels)  )
                errordlg(['solvent file is the wrong size. Please choose a different file'])
                return
            end
            s.solvent_pixels=a.pixels;
            s.solvent_livetime = a.livetime;
        else
            errordlg(['empty file:', s.empty_filename,' does not exist. Please correct'])
            return
        end
    else
        s.solvent_pixels=[];
        s.solvent_livetime=[];
        s.solvent_transfact=[];
        s.solvent_filename=[];
        s.solvent_thickness=[];
    end
end

if strcmp(field,'darkcurrent')
    if ~isempty(s.darkcurrent_filename)
        if exist(s.darkcurrent_filename)
            a=getsaxs(s.darkcurrent_filename);
            if ~(size(a.pixels)==size(s.raw_pixels)  )
                errordlg(['darkcurrent file is the wrong size. Please choose a different file'])
                return
            end
            s.darkcurrent_pixels=a.pixels;
            s.darkcurrent_livetime = a.livetime;
        else
            errordlg(['dark current file:', s.darkcurrent_filename,' does not exist. Please correct'])
            return
        end
    else
        s.darkcurrent_filename=[];
        s.darkcurrent_pixels=[];
        s.darkcurrent_livetime=[];
    end
end

if strcmp(field,'readoutnoise')
    if ~isempty(s.readoutnoise_filename)
        if exist(s.readoutnoise_filename)
            a=getsaxs(s.readoutnoise_filename);
            if ~(size(a.pixels)==size(s.raw_pixels)  )
                errordlg(['readoutnoise file is the wrong size. Please choose a different file'])
                return
            end
            s.readoutnoise=mean(mean(a.pixels));
        else
            errordlg(['readout noise file:', s.readoutnoise_filename,' does not exist. Please correct'])
            return
        end
    else
        s.readoutnoise_filename=[];
        s.readoutnoise=[];
    end
end
function Sred=reduction_state2D(S,handles);
%SAXS/REDUCTION_STATE Calculate reduction state based on saxs_object values
% handles    structure with handles and user data (see GUIDATA)

Sred=S;
reduction_state=0;
if ~isempty(S.mask_pixels), reduction_state=reduction_state+8; end
if ~isempty(S.flatfield_pixels), reduction_state=reduction_state+16+32; end
if ~isempty(S.transfact), reduction_state=reduction_state+64; end
if ~isempty(S.empty_pixels), reduction_state=reduction_state+128; end
if ~isempty(S.darkcurrent_pixels), reduction_state=reduction_state+256; end
if ~isempty(S.abs_int_fact), reduction_state=reduction_state+512; end
if ~isempty(S.sample_thickness), reduction_state=reduction_state+1024; end
if ~isempty(S.readoutnoise), reduction_state=reduction_state+4; end
if ~isempty(S.zinger_removal), reduction_state=reduction_state+2; end
if ~isempty(S.solvent_pixels), reduction_state=reduction_state+1; end

Sred.reduction_state=num2str(dec2bin(reduction_state,11)); %reduction_state is a string
%if increasing the number of reduction parameters...all of the above
%integers must be multiplied by 2...the new one given the value +1 and the 
%dec2bin...value increased by 1 (both above and below here
%if reduction_state==0; 
%    Sred.reduction_state=[];
%else
%    Sred.reduction_state=num2str(dec2bin(reduction_state,11)); %reduction_state is a string
%end
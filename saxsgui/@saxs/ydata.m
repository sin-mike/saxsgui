function Y = ydata(s)
%SAXS/YDATA Return vector of pixel y-coordinates.
%   YDATA(S) returns a vector of y coordinates for pixel rows in
%   SAXS object S.
nrows = size(s.pixels, 1);
ylimits = s.yaxis;
if isempty(ylimits)
    ylimits = [1 nrows];
end
Y = [ylimits(1) : (ylimits(2) - ylimits(1)) / (nrows - 1) : ylimits(2)];

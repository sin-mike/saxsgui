function symval = axsymp(sax)
%SAXS/AXSYMP Compute axial symmetry value for polar SAXS image.
%   AXSYMP(S) returns a scalar value representing the amount of axial
%   symmetry in polar SAXS image S.  Lower values indicate better symmetry.

% AXSYMP to compute axial symmetry value for image in polar coord's.
% Any rotating, masking, or weighting done prior to AXSYMP.
% Find rows for 0 <= theta < 180
% and rows for 180 <= theta < 360.
% Same number rows?
% Subtract and square.
% Now find rows for 90 <= theta < 270
% and rows for 270 <= theta < 360  plus  0 <= theta < 90.
% Same number rows?
% Subtract and square.
% Add squares and return.

if ~ isequal(type(sax), 'rth')
    error('The SAXS object argument must be in polar form.')
end

theta = ydata(sax);  % theta value for each row

top_row_nums = find(theta > 0 & theta < 180);
top_rows = sax.pixels(top_row_nums(1) : top_row_nums(end), :);

bot_row_nums = find(theta > 180 & theta < 360);
bot_rows = sax.pixels(bot_row_nums(1) : bot_row_nums(end), :);

ud_sym = sum(sum((top_rows - flipud(bot_rows)) .^ 2));

left_row_nums = find(theta > 90 & theta < 270);
left_rows = sax.pixels(left_row_nums(1) : left_row_nums(end), :);

right_row_nums1 = find(theta > 270);
right_rows1 = sax.pixels(right_row_nums1(1) : right_row_nums1(end), :);

right_row_nums2 = find(theta >= 0 & theta < 90);
right_rows2 = sax.pixels(right_row_nums2(1) : right_row_nums2(end), :);

right_rows = [right_rows1 ; right_rows2];

lr_sym = sum(sum((left_rows - flipud(right_rows)) .^ 2));

symval = ud_sym + lr_sym;

function so = ysum(si)
%SAXS/YSUM Collapse a SAXS image to a summed intensity plot along the y axis.
%   YSUM(S) returns a new SAXS image, a collapsed version of S, plotting
%   the sum of intensities in each row of S along the y axis.

so = si;
so.pixels(isnan(so.pixels)) = 0;  % replace NaNs with zeros
so.pixels = sum(so.pixels');
if isempty(so.type)
    so.type = 'ysum';
else
    switch so.type
        case 'xy'
            so.type = 'ysum';
        case 'rth'
            so.type = 'thsum';
        otherwise
            so.type = 'ysum';  % ??
    end
end
event = so.type;
so.history = strvcat(so.history, event);

function obj = cart(obj0)
%SAXS/CART Transform a SAXS object from polar to cartesian coordinates (not used)
%
%   WARNING:  This function does not yet work!
%
%   OBJ = CART(OBJ0) returns a cartesian, or xy, SAXS image object in OBJ
%   derived from the SAXS image in OBJ0.  If OBJ0 is in polar coordinates,
%   CART interpolates a regularly spaced set of cartesian coordinates.
%   This is the reverse of SAXS/POLAR, though we may not obtain the
%   original image, since we will probably sample points differently on the
%   return.
%
%   See also:  SAXS/POLAR; SAXS/CENTER.

% Find the minimum image radius, bounded by NaNs.
nancols = sum(isnan(obj0.pixels));
lastcol = min(find(nancols)) - 1;
% minrad = axiscoords(obj0, [lastcol 1]);
% minrad = lastx(1);

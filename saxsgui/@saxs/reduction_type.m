function so=reduction_type(si,rtype)
%SAXS/REDUCTION_TYPE store reduction type in SAXS object
%this function sets the reduction_type
% so=reduction_type(si,rtype)
% with rtype=1 being pixel-by-pixel reduction
% and       =2 being spectra-by-spectra reduction
%
so=si;
if rtype==1
    so.reduction_type='p';
else
    so.reduction_type='s';
end



function objval = ellipsym(sobj)
%SAXS/ELLIPSYM Compute an objective value for a SAXS object's elliptical symmetry.
%   ELLIPSYM(SAXSOBJ) returns a scalar objective value indicating the
%   amount of elliptical symmetry in SAXS image object SAXSOBJ.  SAXSOBJ
%   must already be centered and transformed to polar coordinates (see
%   SAXS/CENTER and SAXS/POLAR).  By elliptical symmetry we mean symmetry
%   about both major axes of the (cartesian) image.  Like an ellipse.

if isequal(type(sobj), 'rth')  % sobj is polar
    objval = ellipsym0(sobj);  % do real computation
else
    error('SAXS object must be in polar coordinates.')
end

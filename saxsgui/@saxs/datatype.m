function argout = datatype(s, dtype)
%SAXS/DATATYPE Set or return the data type from which the SAXS image object was created.
%   DATTYPE = DATATYPE(S) returns the image type, 'mpa','tiff','img', ', of
%   SAXS image S.
%
%   S2 = DATATYPE(S1, DATTYPE) sets the data type, returning the
%   SAXS image object.
%
% this function is called from files like mparead.m fujiimgread.m and
% tiffread.m

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.datatype;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    argout.datatype = dtype;
    argout.history = strvcat(argout.history, ['datatype ', dtype]);
otherwise
    error('I need one or two arguments.')
end

function snew=copy_reduction_parameters(snew,sold)
%SAXS/COPY_REDUCTION_PARAMETERS copy reduction parameters 
%   This function copies the reduction parameters from the previous saxs
%   object to the present one...

snew.transfact=             sold.transfact;
snew.sample_thickness=      sold.sample_thickness;
snew.reduction_state=       sold.reduction_state;
snew.mask_filename=         sold.mask_filename;
snew.mask_pixels=           sold.mask_pixels;
snew.flatfield_filename=    sold.flatfield_filename;
snew.flatfield_pixels=      sold.flatfield_pixels;                                                      
snew.flatfieldtype=         sold.flatfieldtype;                                                                               
snew.empty_filename=        sold.empty_filename;                             
snew.empty_pixels=          sold.empty_pixels;                                                                   
snew.empty_transfact=       sold.empty_transfact;                                                                              
snew.empty_livetime=        sold.empty_livetime;  
snew.solvent_filename=        sold.solvent_filename;                             
snew.solvent_pixels=          sold.solvent_pixels;                                                                   
snew.solvent_transfact=       sold.solvent_transfact;                                                                              
snew.solvent_livetime=        sold.solvent_livetime;  
snew.solvent_thickness=        sold.solvent_thickness;
snew.darkcurrent_filename=  sold.darkcurrent_filename;                                                                              
snew.darkcurrent_pixels=    sold.darkcurrent_pixels;                                                                      
snew.darkcurrent_livetime=  sold.darkcurrent_livetime;
snew.readoutnoise_filename= sold.readoutnoise_filename;                                                                              
snew.readoutnoise=          sold.readoutnoise;                                                                      
snew.zinger_removal=        sold.zinger_removal;
snew.abs_int_fact=          sold.abs_int_fact;


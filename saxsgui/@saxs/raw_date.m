function argout = raw_date(s, raw_date)
%SAXS/RAW_DATE Set or return the date of the data file from which the SAXS image object was created.
%   raw_date = raw_date(S) returns the filename,
%
% this function is called from files like mparead.m and
% tiffread.m

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.raw_date;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    argout.raw_date = raw_date;
otherwise
    error('I need one or two arguments.')
end

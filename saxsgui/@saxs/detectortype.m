function argout = detectortype(s, dettype)
%SAXS/DETECTORTYPE Set or return the data type from which the SAXS image object was created.
%   DTYPE = DETECTORTYPE(S) returns the detector type,
%   'MolMet','FujiIP','Hi-Star' of
%   SAXS image S.
%
%   S2 = DETECTORTYPE(S1, DETTYPE) sets the detector type, returning the
%   SAXS image object.
%
% this function is called from files like mparead.m fujiimgread.m and
% tiffread.m

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.detectortype;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    argout.detectortype = dettype;
    argout.history = strvcat(argout.history, ['detectortype ', dettype]);
otherwise
    error('I need one or two arguments.')
end

function S = uncalibrate(Sin)
%SAXS/UNCALIBRATE Remove K value calibration from a SAXS object.
%   S = UNCALIBRATE(S) returns SAXS object S without K value
%   calibration parameters.  S will be displayed with pixel offsets
%   rather than K values.
%
%   See also:  SAXS; SAXS/CALIBRATE; SAXS/IMAGE.

S = Sin;
if ~ isempty(S.kcal)  % image is calibrated
    % uncalibrate axis ranges
%     p2k = (S.kcal - S.koffset) ./ S.pixelcal;
%     S.xaxis = (S.xaxis - S.koffset) ./ p2k(1);
%     if isequal(S.type, 'xy')
%         S.yaxis = (S.yaxis - S.koffset) ./ p2k(2);
%     end
    [nrows ncols]=size(S.pixels);
    imgcen=pixel(S,[0 0]);
    S.xaxis=[1-imgcen(1),ncols-imgcen(1)];
    S.yaxis=[1-imgcen(2),nrows-imgcen(2)];
    % remove calibration data
    S.kcal = [];
    S.pixelcal = [];
    S.koffset = [];
    S.calibrationtype=[];
    S.wavelength=[];
    S.pixelsize=[];
    S.detector_dist=[];
    S.calibrationtype=[];
    % record history
    S = history(S, 'clear K calibration');
end

function s = minus(arg1, arg2)
%SAXS/MINUS Subtract saxs image objects.
%   One or both of the arguments is a saxs image object.
%   The result is a saxs image object after array subtraction
%   of the pixels.  One argument may be a scalar value.
%   Otherwise the dimensions of the (pixel or other) arrays
%   must match.

difference = double(arg1) - double(arg2);

if isa(arg1, 'saxs')
    s = arg1;
    s.history = strvcat(s.history, logarg('minus', arg2, inputname(2)));
else  % arg2 must be a saxs object
    s = arg2;
    s.history = strvcat(s.history, logarg('subtracted from', arg1, inputname(1)));
end
s.pixels = difference;

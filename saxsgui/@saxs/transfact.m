function argout = transfact(s, t)
%SAXS/TRANSFACT Set or retrieve transmission factor for SAXS image.
%   TRANSFACT(S) returns the transmission factor recorded for SAXS image S.
%
%   TRANSFACT(S, T) returns a SAXS image object copied from S with
%   transmission factor T.

switch nargin
case 1  % return transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.transfact;
case 2  % set transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isempty(t), t=1; end
    if isnumeric(t) && length(t) == 1
        argout.transfact = t;
    else
        error('I do not understand this TRANSFACT argument.')
    end
    argout.history = strvcat(argout.history, ['transfact ', num2str(t)]);
otherwise  % we don't understand
    error('I need one or two arguments.')
end

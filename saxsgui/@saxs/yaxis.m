function argout = yaxis(varargin)
%SAXS/YAXIS Set or retrieve y axis scale for a SAXS image object.
%   YAXIS(S, Y) returns a new SAXS object built from SAXS object S but with
%   a new y axis scale Y.  Y is a vector of two numbers in ascending order,
%   corresponding to the first and last row of the image in S.
%
%   YAXIS(S) returns the y axis vector from SAXS object S.

argout = saxis(2, varargin{:});

%SAXS/CENTER-NOTES Notes about the CENTER function.
%   CENTER tries to find the center of a SAXS image through a
%   combination of simplex search, grid search and interpolation.
%
%   If the axis coordinates 0,0 for the SAXS image are inside the
%   image the search begins at the nearest pixel to coordinates 0,0.
%   The axis coordinates of a raw image are the column and row
%   numbers which start at 1.
%
%   The objective function for the search is the squared  difference
%   between opposite quadrants around the proposed center. CENTER ignores
%   data near the proposed center in order to  eliminate effects of a
%   misaligned beamstop.  See SAXS/MASK.
%
%   Simplex optimization is used to find the approximate center starting
%   from the middle of the image or the specified starting point. A grid
%   search is then performed to verify that a minimum has been found.  A
%   quadratic is then fit to the objective function around the minimum
%   point to  estimate the exact center to a fraction of a pixel. 
%
%   To see the center of a displayed image more clearly, turn
%   the grid on with the command,  GRID ON.
%
%   See also SAXS/CENTER, SAXS/MASK.

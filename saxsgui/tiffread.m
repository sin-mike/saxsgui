function saxsobj = tiffread(pathname,silent) 
%TIFFREAD Read an image from a TIFF file, returning a SAXS image object.
%   TIFFREAD('file') reads the TIFF file 'file' and returns a SAXS image
%   object.
%
%
%   Future upgrades need to put in a warning that the process takes a long
%   time for large images.
%
%   KDJ January 5, 2006 Altered to be able to recognized a DESY mar CCD
%   image. Corresponding to what's done at Hasylab A2. 
%
%   KDJ January 30, 2005 Altered file to correct for "inverse coloring" and logarithmic tiff
%   files
%
%   KDJ January 16, 2005. Altered the tiff file to include also reading of
%   RGB color files.
%
%   Before 2005. The TIFF file must contain a grayscale image, not a color (RGB) image.
%   No longer correct!!!
%
%   KDJ February 21, 2010. Used imfinfo to allow the recognition of
%   different tiff files (for example pilatus)
%
%   See also SAXS/SAXS, GETSAXS.
[filepart1,filepart2,filepart3]=fileparts(pathname);


try  % reading an image from the file
    im = imread(pathname, 'tif')';
    %im = imread(pathname, filepart3(2:end))';
    dirinfo=dir(pathname);
    filedate=dirinfo.date;
    warning('off','MATLAB:tifftagsread:unknownTagPayloadState')
    imheader=imfinfo(pathname);
    warning('on','MATLAB:tifftagsread:unknownTagPayloadState')
catch
    caught = lasterr;  % error message
    error(['Error reading TIFF file', pathname, ':  ', caught])
end

if ndims(im) == 3  % this is an RGB image
    
    %error('TIFF file contains a color (RGB) image.  We will try to read it for SAXS.')
    
    %obtaining grayscale according to RGB-UYV model
    imgray=0.2990*double(im(:,:,1))+0.587*double(im(:,:,2))+0.1140*double(im(:,:,3));
    %rotating and making 0 intensity correspond to zero
    imgray=rot90(255-imgray',-1)*256; % don't know if this is necessary
    im=imgray';
%error('3')
end

% tiff files can come from a number of sources with Meta-information 
% provided in various ways.
% 
% 1) Hasylab/Desy has meta-information in special extra header files
% 2) Dectris/Pilatus has meta-information in the header file of tiff file
% 3) Others don't have any information. 

% Let's first look for signs of a header file (Desy/Hasylab style)
headerfile_exist=0;
%first look for headerfiles
[pathstr,name,ext] = fileparts(pathname);
%start with Desy single file format:
if exist([pathstr,filesep,name,'.dat'])
    headerfile_exist=1;
    headerfile_name=[pathstr,filesep,name,'.dat'];
end
%then Desy multiple file format:
multiple_file_name=name(1:end-5);
if exist([pathstr,filesep,multiple_file_name,'.dat'])
    headerfile_exist=1;
    headerfile_name=[pathstr,filesep,multiple_file_name,'.dat'];
end

if headerfile_exist
    file = fopen(headerfile_name, 'rt');
    if file == -1
        error('Could not open file.')
    end
    [realtime1,livetime1,detectortype1] = parse_desy_tiff_header(file);
    
else
    % OK..so it was not desy/hasylab...how about Pilatus
    ispilatus=0;
    if isfield(imheader,'Model')
        if strcmp('PILATUS',imheader.Model(1:7))
            %yes it is a Pilatus
            if isfield(imheader,'ImageDescription')
                % parse the imagedescription for relevant data
                [realtime1,livetime1] = parse_Pilatus_tiff_header(imheader.ImageDescription);
                detectortype1=imheader.Model(1:12);
                ispilatus=1;
                if isfield(imheader,'UnknownTags') % is there a TIFF tag 1334 with xmlformatted metadata
                    %how many Unknowntags
                    numtag=length(imheader.UnknownTags);
                    %are there any with 1334
                    for ii=1:numtag
                        if imheader.UnknownTags(ii).ID==1334
                            metadataheader=parse_saxslab_header(imheader.UnknownTags.Value);
                        end
                    end
                end
            end
        end
    end
    if ispilatus~=1  % some other format where we cannot get the exposure times
        if ~silent
            %input exposure time in dialogbox
            prompt={'Enter the normalization factor (time or monitor counter) for the tiff-file (in seconds):'};
            def={'900'};
            dlgTitle='Input for Exposure Time';
            lineNo=1;
            answer=inputdlg(prompt,dlgTitle,lineNo,def);

            livetime1=str2num(char(answer));
            if isempty(livetime1)
                livetime1=1;
            end
        else
            livetime1=1;
        end
        realtime1 = max(0,livetime1);
        detectortype1='Unknown';
    end
end
im=double(im);
if ispilatus
    if (strcmp(imheader.Model(1:12),'PILATUS 300K')) && sum(sum(im(:,196:212)))==0
        %it is a Pilatus 300K with stripes that should be masked away
        im(:,196:212)=im(:,196:212)+NaN;
        im(:,408:424)=im(:,408:424)+NaN;
    end
else
    im=rot90(flipud(im),-1);
end
% tiff data is taken to be linearly transform on a 1 to 1 basis so no need to make
% corrections

saxsobj = saxs(im, ['tiffread file ''', pathname, ''' ', datestr(now)]);


saxsobj = type(saxsobj, 'xy');
saxsobj = livetime(saxsobj, max(0,livetime1));
saxsobj = realtime(saxsobj, max(0,realtime1));
saxsobj = raw_filename(saxsobj, pathname);
saxsobj = raw_date(saxsobj,filedate);
saxsobj = datatype(saxsobj,'tiff');
saxsobj = detectortype(saxsobj,detectortype1);
try 
    saxsobj = metadata(saxsobj,metadataheader);
catch
end
fclose('all');

% ---------------------------------------------------------
function [realtime1,livetime1,detectortype1] = parse_desy_tiff_header(file)
% Extract array x dimension.

line = findline(file, ['!MarCCD file ',name,'.tif']);
nextline=fgetl(file);
monitors=str2num(nextline);

realtime1=monitors(1);
livetime1=monitors(2);
detectortype1='MarCCD';


function [realtime1,livetime1] = parse_Pilatus_tiff_header(ImDesc)

%find livetime
string_to_find='Exposure_time ';
a=strfind(ImDesc,string_to_find)+length(string_to_find);
livetime1=str2num(strtok(ImDesc(a:end)));
%find realtime
string_to_find='Exposure_period ';
a=strfind(ImDesc,string_to_find)+length(string_to_find);
realtime1=str2num(strtok(ImDesc(a:end)));
%find other not really used
string_to_find='Pixel_Size ';
a=strfind(ImDesc,string_to_find)+length(string_to_find);
tmptext=strtok(ImDesc(a:end),'#');
pixelsizetxt=tmptext(1:end-1);

function [saxslabheader] = parse_saxslab_header(xmlmetadata)

%first parse the xml
paramnamestart=strfind(xmlmetadata,'param name="');
paramnameend=strfind(xmlmetadata,'">');
paramend=strfind(xmlmetadata,'</param>');
saxslabheader='';
for ii=1:length(paramnamestart)
    paramnametmp=xmlmetadata(paramnamestart(ii)+12:paramnameend(ii)-1);
    paramvaluetmp=xmlmetadata(paramnameend(ii)+2:paramend(ii)-1);
    strtmp='';
    if ~strcmp(paramvaluetmp,'tiff')
        if isempty(str2num(paramvaluetmp))
            strtmp=['saxslabheader.',paramnametmp,'=''',paramvaluetmp,''';'];
        else
            strtmp=['saxslabheader.',paramnametmp,'=[',paramvaluetmp,'];'];
        end
    end
    try
        strtmp;
        eval(strtmp)
    catch
        strtmp=strrep(strtmp,strtmp(strfind(strtmp,':'):strfind(strtmp,'=')-1),'');
        eval(strtmp)
    end
end

% ---------------------------------------------------------
function line = findline(file, findstr, varargin)
%FINDLINE Look for line in file starting with string.
%  line = FINDLINE(file, findstr)
%    Look forward in file (handle) for a line starting with
%    the characters in findstr.
%    If not found before end of file, return an empty array.
%    Otherwise return the line found as a character array.
%
%  line = FINDLINE(file, findstr, stopstr)
%    Same but stop if we find a line starting with stopstr,
%    returning an empty array.  An enhancement
%    would be to begin the next search at that line.  But
%    for now a subsequent search would start at the line after
%    the stop line.  So far, we're going to bail out if one
%    of our FINDLINE's fails, so we don't need subsequent
%    FINDLINE's.

lenfind = length(findstr);
stopstr = [];
lenstop = 0;
if nargin == 3
    stopstr = varargin{1};
    lenstop = length(stopstr);
end

while ~ feof(file)
    line = fgetl(file);
    if strncmp(line, findstr, lenfind)
        break
    end
    if strncmp(line, stopstr, lenstop)  % returns false if lenstop is 0
        break
    end
end

if ~ strncmp(line, findstr, lenfind)
    line = [];  % string not found
end

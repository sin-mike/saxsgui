function inlist = propinlist(list, name)
%PROPINLIST Determine if property name is in a property name/value list.
%   PROPINLIST(LIST, 'name') returns true if 'name' is a name in the cell
%   array LIST, {'name1', VAL1, 'name2', VAL2, ...}.  LIST may be empty.
%
%   Feb 21, 2003:  Does this really work?  We don't use it anywhere.

if isempty(list)
    inlist = false;
    return
end

nprops = length(list) / 2;
if nprops ~= fix(nprops)  % odd number of list items
    nprops = fix(nprops);
    list = list{1 : end - 1};  % chop off the last one
end

list = reshape(list, 2, nprops);
list = list(1, :);
inlist = any(list == name);

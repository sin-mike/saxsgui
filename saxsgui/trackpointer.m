function trackpointer(fig)
%TRACKPOINTER Display coordinates of pointer within axes.
%   Noto bene!!  This don' work yet!  Maybe never!
%
%   TRACKPOINTER toggles pointer tracking in the current figure, displaying
%   two-dimensional coordinates in at the top right corner of any axes.
%
%   TRACKPOINTER(FIG) tracks the pointer in the specified figure.

if ~ nargin
    fig = gcf;
end

label = getappdata(fig, 'trackpointer_label');
if label  % we are tracking, so stop
    set(fig, 'WindowButtonMotionFcn', '')
    delete(label)
    rmappdata(fig, 'trackpointer_label')
else  % start tracking
    
end
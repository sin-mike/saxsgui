%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ap_struct=autoprocfileread(ap_file)
file = fopen(ap_file, 'rt');
ap_struct=[];
if file == -1
    error('Could not open autoprocessing file.')
end
while ~feof(file)
    line=fgetl(file);
    eq_split=findstr(line,'=');
    comment_split=findstr(line,'%');
    if isempty(comment_split), comment_split=length(line)+1;end
    if ~isempty(eq_split)
        if eq_split(1)<comment_split
            param=strtrim(line(1:eq_split-1));
            pval=strtrim(line(eq_split+1:comment_split-1));
            if ~isempty(str2num(pval))
                pval=str2num(pval);
            else
                pval=strrep(pval,'''','');
            end
            if ~isempty(param)
                ap_struct=assign_struct(ap_struct,param,pval);
            end
        end
    end
end
fclose(file);
if ~isfield(ap_struct,'matlab_center_file') || ~isfield(ap_struct,'matlab_calib_file')
    errordlg('Both the matlab center and calibration files must be given in the setup configuration file')
    ap_struct= [];
else
    if (~exist([ap_struct.matlab_center_file,'.mat']) || ~exist([ap_struct.matlab_calib_file,'.mat'])) &&...
       (~exist(ap_struct.matlab_center_file) || ~exist(ap_struct.matlab_calib_file))     
        errordlg('Either the center or the calibration file could not be found')
        ap_struct= [];
    end
end
%determining if averaging is required
if isfield(ap_struct,'do_average') %averaging is desired
    if ap_struct.do_average==1
        if ~isfield(ap_struct,'I_vs_q'), ap_struct.I_vs_q=1; end
        if ~isfield(ap_struct,'phistart'), ap_struct.phistart=0; end
        if ~isfield(ap_struct,'phiend'), ap_struct.phiend=360; end
        if ~isfield(ap_struct,'qstart'), ap_struct.qstart=1e-3; end
        if ~isfield(ap_struct,'qend'), ap_struct.qend=0.5; end
        if ~isfield(ap_struct,'xaxis_type'), ap_struct.xaxis_type='lin';end
        if ~isfield(ap_struct,'num_xpoints'), ap_struct.num_xpoints=200;end
        if ~isfield(ap_struct,'reduction_type'), ap_struct.reduction_type='s';end
        if ~isfield(ap_struct,'reduction_text'), ap_struct.reduction_text=1;end
    else
        ap_struct.do_average=0;
    end
else
    ap_struct.do_average=0;
end
%determining if 1D-output is required
if isfield(ap_struct,'do_output')
    if (ap_struct.do_output==1)
        if ~isfield(ap_struct,'output_dir'), ap_struct.output_dir='';end
        if ~exist(ap_struct.output_dir),ap_struct.output_dir='';end
        if ~isfield(ap_struct,'addon_name'), ap_struct.addon_name='';end
        if ~isfield(ap_struct,'print'), ap_struct.print=0; end
        if ~isfield(ap_struct,'print1D2jpeg'), ap_struct.print1D2jpeg=0; end
        if ~isfield(ap_struct,'print2D2jpeg'), ap_struct.print2D2jpeg=0; end
        if ~isfield(ap_struct,'save2fig'), ap_struct.save2fig=0; end
        if ~isfield(ap_struct,'save2columns'),ap_struct.save2columns=0;end
        if ~isfield(ap_struct,'save3columns'),ap_struct.save3columns=0;end
    else
        ap_struct.do_output=0;
    end
else
    ap_struct.do_output=0;
end
%determining if 2D-output is required
if isfield(ap_struct,'doxy_output')
    if (ap_struct.doxy_output==1)
        if ~isfield(ap_struct,'doxy_save'), ap_struct.doxy_save=0;end
        if ~isfield(ap_struct,'xy_output_dir'), ap_struct.xy_output_dir='';end
        if ~exist(ap_struct.xy_output_dir),ap_struct.xy_output_dir='';end
        if ~isfield(ap_struct,'xy_numx')
            error('AutoProcessing file does not contain information on number of X-points. Please correct AutoProcessing file')
            return
        end
        if ~isfield(ap_struct,'xy_numy')
            error('AutoProcessing file does not contain information on number of Y-points. Please correct AutoProcessing file')
            return
        end
        if ~isfield(ap_struct,'xy_stepx'), ap_struct.stepx=1; end
        if ~isfield(ap_struct,'xy_stepy'), ap_struct.stepy=1; end
        if ~isfield(ap_struct,'xy_unit'), ap_struct.stepy=''; end
        if ~isfield(ap_struct,'doxy_transmap'), ap_struct.doxy_transmap=1; end
        if ~isfield(ap_struct,'doxy_absmap'), ap_struct.doxy_absmap=1; end
        if ~isfield(ap_struct,'doxy_intmap'), ap_struct.doxy_intmap=1; end
        if (ap_struct.doxy_arrow_output==1)
            if ~isfield(ap_struct,'xy_arrow_background'), ap_struct.doxy_arrow_background=2; end
            if ~isfield(ap_struct,'xy_arrow_direction_expr'), ap_struct.doxy_arrow_direction_expr='0'; end
            if ~isfield(ap_struct,'xy_arrow_size_expr'), ap_struct.doxy_arrow_size_expr='1'; end
        end
    else
        ap_struct.doxy_output=0;
    end
else
    ap_struct.doxy_output=0;
end
%also save the date that the autoprocessing file was saved
D=dir(ap_file);
ap_struct.modifdate=D.date;

            
    
function ap=assign_struct(ap,parameter,pval)
switch parameter
    % These pertain to sample - should really be in info file
    case 'filename'
        ap.filename=pval;
    case 'realtime'
        ap.realtime=pval;
    case 'livetime'
        ap.livetime=pval;
    case 'transmission'
        ap.transmission=pval;
    case 'sample_thickness'
        ap.sample_thickness=pval;
    case 'comment'
        ap.comment=pval;
    case 'comment2'
        ap.comment2=pval;
    % These pertain to sample - should really be in info file
    case 'empty_filename'
        ap.empty_filename=pval;
    case 'empty_transfact'
        ap.empty_transfact=pval;
    case 'solvent_filename'
        ap.solvent_filename=pval;
    case 'solvent_transfact'
        ap.solvent_transfact=pval;
    case 'solvent_thickness'
        ap.solvent_thickness=pval;
    %These pertain to the specfic setup
    case 'matlab_center_file'
        ap.matlab_center_file=pval;
    case 'matlab_calib_file'
        ap.matlab_calib_file=pval;
    case 'matlab_reduction_file'
        ap.matlab_reduction_file=pval;
    case 'mask_filename'
        ap.mask_filename=pval;
    case 'flatfield_filename'
        ap.flatfield_filename=pval;
    case 'abs_int_fact'
        ap.abs_int_fact=pval;
    % These pertain to the generic system
    case 'darkcurrent_filename'
        ap.darkcurrent_filename=pval;
    case 'readoutnoise_filename'
        ap.readoutnoise_filename=pval;
    case 'zinger_removal'
        ap.zinger_removal=pval;
    case 'error_calib'
        ap.error_calib=pval;
    %these pertain to averaging instructions
    case 'do_average'
        ap.do_average=pval;
    case 'I_vs_q'
        ap.I_vs_q=pval;
    case 'phistart'
        ap.phistart=pval;
    case 'phiend'
        ap.phiend=pval;
    case 'qstart'
        ap.qstart=pval;
    case 'qend'
        ap.qend=pval;
    case 'xaxis_type'
        ap.xaxis_type=pval;
    case 'num_xpoints'
        ap.num_xpoints=pval;
    case 'reduction_type'
        ap.reduction_type=pval;
    case 'reduction_text'
        ap.reduction_text=pval;
    % These pertain to output instructions
    case 'do_output'
        ap.do_output=pval;
    case 'new'
        ap.new=pval;
    case 'plot'
        ap.plot=pval;
    case 'print'
        ap.print=pval;
    case 'print1D2jpeg'
        ap.print1D2jpeg=pval;
    case 'print2D2jpeg'
        ap.print2D2jpeg=pval;
    case 'save2fig'
        ap.save2fig=pval;
    case 'save2columns'
        ap.save2columns=pval;
    case 'save3columns'
        ap.save3columns=pval;
    case 'addon_name'
        ap.addon_name=pval;
    case 'output_dir'
        ap.output_dir=pval;
    % These pertain to XY scans instructions
    case 'doxy_output'
        ap.doxy_output=pval;
    case 'doxy_save'
        ap.doxy_save=pval; 
    case 'xy_output_dir'
        ap.xy_output_dir=pval;
    case 'xy_name'
        ap.xy_name=pval;
    case 'xy_numx'
        ap.xy_numx=pval;
    case 'xy_numy'
        ap.xy_numy=pval;
    case 'xy_stepx'
        ap.xy_stepx=pval;
    case 'xy_stepy'
        ap.xy_stepy=pval;
    case 'xy_unit'
        ap.xy_unit=pval;
    case 'doxy_transmap'
        ap.doxy_transpmap=pval;
    case 'doxy_absmap'
        ap.doxy_absmap=pval;
    case 'doxy_intmap'
        ap.doxy_intmap=pval;
    case 'doxy_arrow_output'
        ap.doxy_arrow_output=pval;
    case 'xy_arrow_background'
        ap.xy_arrow_background=pval; 
    case 'xy_arrow_direction_expr'
        ap.xy_arrow_direction_expr=pval; 
    case 'xy_arrow_size_expr'
        ap.xy_arrow_size_expr=pval;  
end
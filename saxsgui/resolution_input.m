function [sigma_d1,sigma_d2,sigma_w,sigma_c1,sigma_c2,q_limit,npixels]=resolution_input()

%first ask for the system parameters.
prompt={'Enter the FWHM value for the detector smearing',...
    'Enter the FWHM value for the source wavelength distribution',...
    'Enter the FWHM value for the collimation smearing, if unknown (0), fill in system parameters',...
    'Enter the radius of pinhole 1 in m:',...
    'Enter the radius of pinhole 2 in m:',...
    'Enter the radius of pinhole 3 in m:',...
    'Enter the length between the first and second pinhole in m',...
    'Enter the length between the second pinhole and the sample in m',...
    'Enter the length between the sample and the detector in m',...
    'Enter the radius of the beamstop in m',...
    'Enter the wavelength of the radiation in m',...
    'number of pixels to use for smearing: 4, 8, 12, 20 or 24.',...
    'Do you have loads of memory available? (approx. 250 Mb) (Yes/No)'};
%        'Enter the length between the sample and detector in mm',... -->
%        not used yet, the derivation by Pedersen does not take this into
%        account!!!
name='Input for instrumental resolution limit calculation';
numlines=1;
defaultanswer={'0.01e-10','0.01e-10','0.01e-10','0.0005','0.0002','0.0005','1','1','4','0.0025','1.8415e-10','24','Yes'};
options.Resize='on';
options.Windowstyle='normal';
options.Interpreter='tex';
parameters=inputdlg(prompt,name,numlines,defaultanswer,options);

%convert the parameters for clarity
n=1;
%finite width at half max. related to sigma as sigma=fwhm/(2(2ln2)^0.5)
fwhm_d1=str2double(parameters(n));n=n+1; %detector smearing in direction 1
fwhm_d2=fwhm_d1;%for the purpose of this exercise...
%fwhm_dv=str2double(parameters(2)); %detector smearing in direction 2
fwhm_w=str2double(parameters(n));n=n+1; %wavelength distribution (only influences direction 1, parallel to q)
fwhm_c1=str2double(parameters(n));n=n+1; %collimation smearing
% fwhm_c2=str2double(parameters(5)); %collimation smearing in direction 2
    r1=str2double(parameters(n));n=n+1;
    r2=str2double(parameters(n));n=n+1;
    r3=str2double(parameters(n));n=n+1;
    l1=str2double(parameters(n));n=n+1;
    l2=str2double(parameters(n));n=n+1;
    ld=str2double(parameters(n));n=n+1;
    rbs=str2double(parameters(n));n=n+1;%beamstop radius
    lambda=str2double(parameters(n));n=n+1; %this is the wavelength in meters.
    npixels=str2double(parameters(n));n=n+1;
    loadsofmemory=parameters(n);n=n+1;
    
    %let's make sure loads of comments in the "loadsof=memory" variable is
    %fixed
    if strcmp(loadsofmemory,'Yes')==1 || strcmp(loadsofmemory,'yes')==1 || strcmp(loadsofmemory,'Y')==1 || strcmp(loadsofmemory,'y')==1
        loadsofmemory='Yes';
    elseif strcmp(loadsofmemory,'No')==1 || strcmp(loadsofmemory,'no')==1 || strcmp(loadsofmemory,'N')==1 || strcmp(loadsofmemory,'n')==1 || strcmp(loadsofmemory,'YeeaaNo')==1
        loadsofmemory='No';
    end
    
if fwhm_c1==0; %then determine the sigma by means of the system parameters

    %ls=str2double(parameters(10)); see note above.

    
    r2f=r2+r2*l2/(l1+(r1*l1/(r2-r1))); %this calculates a fictional second 
    %pinhole size, given that we have a three-pinhole collimation system, 
    %and not a two pinhole system, as stated in the article. however, the
    %function can result in negative values for the third pinhole, which is
    %obviously mistaken. What would be a better solution?
    
    %The better solution, dictated by the focusing optics, is to input the
    %beam divergence and size at the sample position.
    
    L=l2+l1;

    %twotheta=2.*arcsin (q./(2.*(2.*pi./lambda))); %p.325, 2nd column)

    a1=r1/(l1+ld);
    a2=r2/(ld);

    %here we have a choice. Either we use uber accurate smearing functions,
    %that give us an angle-depent sigma matrix, or we use the little less
    %accurate function that does not contain the cosine(twotheta) relationship
    %(in effect, sets it to a value of one). for a maximum angle of 10 degrees,
    %the absence of cos(10)^4 will result in a deviation of a little over 5%
    %Those uber-accurate functions are given below, but are not completed for a
    %three-pinhole system. Instead, a little less accurate version is used for
    %clarity.
    % if a1>=a2 %these are the uber-accurate functions, for a two pinhole collimation system.
    %     Dbeta1=2.*r1./l1 - r2.^2.*(cos(twotheta)).^4./(2.*r1.*ld.^2.*l1).*(l1+ld/((cos(twotheta)).^2)).^2; %I am quite confident the matrices are multiplied correctly here.
    %     Dbeta2=2.*r1./l1 - r2.^2.*(cos(twotheta)).^2./(2.*r1.*ld.^2.*l1).*(l1+ld/((cos(twotheta)))).^2; %I am quite confident the matrices are multiplied correctly here.
    % else
    %     Dbeta1=r.*r2.*(1./l1+(cos(twotheta)).^2./ld)-r1.^2.*ld./(2.*r2.*l1).*1/((cos(twotheta)).^2.*(l1+ld/(cos(twotheta)).^2)); %
    %     Dbeta2=r.*r2.*(1./l1+(cos(twotheta))./ld)-r1.^2.*ld./(2.*r2.*l1).*1/((cos(twotheta)).*(l1+ld/(cos(twotheta)))); %
    % end

    %here is the simplified variant. beta1=beta2=beta
    if a1>=a2
        Dbeta=2*r1/L-1/2*r2f^2/r1*(ld+L)^2/ld^2*1/L;
    else
        Dbeta=2*r2f*(1/L+1/ld)-1/2*r1^2/r2f*ld/L*1/(ld+L);
    end
    %direction 1 is the direction parallel to the vector q, direction 2 is
    %perpendicular to that. This implementation assumes they don't differ.

    sigma_c1=2.*pi./(lambda*1e10).*Dbeta./2.*(2.*log(2)).^(1/2); %collimation smearing in direction 1 %argh.. of course lambda in angstroms...
    sigma_c2=2.*pi./(lambda*1e10).*Dbeta./2.*(2.*log(2)).^(1/2); %collimation smearing in direction 2

else
    sigma_c1=2.*pi./lambda.*fwhm_c1./2.*(2.*log(2)).^(1/2);
    sigma_c2=sigma_c1;
end
sigma_d1=2.*pi./lambda*fwhm_d1/(2*(2*log(2))^0.5); %detector smearing in direction 1
sigma_d2=2.*pi./lambda*fwhm_d2/((2*log(2))^0.5); %detector smearing in direction 2
sigma_w=fwhm_w/(2*(2*log(2))^0.5); %wavelength distribution (only influences direction 1, parallel to q)

%now determine the radius (in q) of uncertain data through parasitic scattering
%from the second pinhole. the q limit is in angstroms, naturally. These are the
%limits of the data to which the fitting function can be applied.

q_ps_limit=4*pi/(1e10*lambda)*sin(atan((r3+r2)/(l2*ld)*(l2+ld)-r2/ld));
%originally, this function took the distance between the third pinhole and
%sample into account (see below).... no longer, to avoid complications.
%q_ps_limit=4*pi/(1e10*lambda)*sin(atan((r3+r2)/(l2*ls)*(l2+ld)-r2/ls));

%and determine the q limit as originating from the blocked beamstop:

q_bs_limit=4*pi/(1e10*lambda)*sin(atan(rbs/ld));

%and set the q-limit to be the biggest of the two.
if q_ps_limit>=q_bs_limit
    q_limit=q_ps_limit;
else
    q_limit=q_bs_limit;
end

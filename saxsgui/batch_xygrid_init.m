function    batch_xygrid_out=batch_xygrid_init
global batch_xygrid_temp
% this routine provides initial parameters for the reduction parameter edit
% windows.
% Somewhere in the future it may be able to "know" what was previously
% entered in the last session...but for now it knows what was entered last
% in this session

if ~isempty(batch_xygrid_temp) 
    batch_xygrid_out=batch_xygrid_temp;
else  
    a.Show_Transm=1; 
    a.Show_SAXS_Int=0;
    a.show_orientation=0;
    a.ShowQuiver=0;

    a.Xnum=5;
    a.Ynum=5;
    a.NumFilesChosen=0;
    a.qmin=0.01;
    a.qmax=0.3;

    a.Save2DImages=0;
    a.Save1DFigures=0;

    a.xyformat=0;
    a.xydeltayformat=1;
    a.ReducedDataToSameFile=0;
    a.ReducedDataToSeparateFiles=1;
    
    a.DescriptiveTagString='Reduced';
    
    batch_xygrid_out=a;
    batch_xygrid_temp=batch_xygrid_out;
end
    
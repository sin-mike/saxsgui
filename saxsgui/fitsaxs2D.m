function [Fitparam,Fitval,Info] = fitsaxs2D(si,fitpar,bounds)
%FITSAXS2D Fits SAXS image intensities either in 2 D.
%   FITSAXS2D(S,fitpar,bounds) returns the fit-parameters in a vector,
%   the Fit in a structure and any additional information
%
%   The fitpar should have a structure of the form:
%fitpar =
%
%        name: 'orientationdistribution.m'
%     binning: 1
%         fit: [1 1 1 1 1]
%       start: [1 2 3 4 5]
%          ll: [0 1 2 3 4]
%          ul: [2 3 4 5 6]
%     numiter: 100
%         tol: 1.0000e-003
%    graphics: 1
%  param_name: alpha beta gamma chi omega
%
%   This routine uses an approach provided by Brian R. Pauw October 2006
%   and has as it's core the bounded minimum search mechanism
%   (fminsearchbnd version 4) publicly
%   available at the Matlab Central and written by Author: John D'Errico
%   E-mail: woodchips@rochester.rr.com
%
%   Note: binning is now implemented by Brian
%

%MOD1: get the smearing parameters and set them global so every function
%can use them.
global sigma_d1 sigma_d2 sigma_w sigma_c1 sigma_c2 q_limit npixels smear_shape A q_use
global error_norm_fact
error_norm_fact=si.error_norm_fact;
%ENDMOD

% Let's give some name we can understand
func=fitpar.name(1:end-2); %removing the .m
x0=fitpar.start;
for ii=1:length(fitpar.fit)
    if fitpar.fit(ii)==0  %values are completely constrained
        xl(ii)=x0(ii); %lower bounds
        xu(ii)=x0(ii); %higher bounds
    else
        xl(ii)=max(-Inf,fitpar.ll(ii)); %lower bounds
        xu(ii)=min(Inf,fitpar.ul(ii));  %higher bounds
    end
end
qmin=bounds(1);
qmax=bounds(1)+bounds(3);
phimin=bounds(2);
phimax=bounds(2)+bounds(4);

%This hack is included to fix a bug when the user has not entered any calibrations.

%convert image pixels locations to angle phi and angle q
[q,phi]=create_qphi_gen(si.pixels,si.raw_center(2),si.raw_center(1),si.kcal/si.pixelcal(2),si.kcal/si.pixelcal(1),si.wavelength);
%MOD5: linear binning integrated. Great..thanks Brian!
if fitpar.binning~=1
    [Iobs,qs,phis]=linearbinning2(si,fitpar.binning);
else
    Iobs=si.pixels;
    qs=q;
    phis=phi;
end

%now confine the data to be only what we are interested in
q_use=zeros(size(qs,1),size(qs,2));
%MOD4: this keeps the shape of the matrices:  OK is this necessary? Yes,
%otherwise the smearing function needs to figure out which pixels lie
%closest to the to-be-smeared pixel.
for i=1:size(qs,1)
    for j=1:size(qs,2)
        if (qmin < qs(i,j) && qs(i,j) < qmax && phimin < phis(i,j) && phis(i,j) < phimax)
            q_use(i,j)=1;
        else
            q_use(i,j)=NaN;
        end
    end
end

% Iobs=si.pixels(relevant_data_indices_qphix,relevant_data_indices_qphiy);
% qs=q(relevant_data_indices_qphix,relevant_data_indices_qphiy);
% phis=phi(relevant_data_indices_qphix,relevant_data_indices_qphiy);
%MOD4: also reconstruct the matrix.... time consuming...
%if Iobs is projected onto q_use, as well as Icalc, the merit function
%could still be calculated without problems. It is better, however, to crop
%the functions.
%         npixels=24;%this defines the number of pixels around all other pixels that contribute to the smearing. This is used instead of the cutoff value, to speed up the algorithm. to keep it square, use 8 or 24 pixels. values of 4, 8, 12, 20 and 24 are allowed.
%Iobs=I;
%qs=q;
%phis=phi;

%MOD3: there is now enough information to calculate the smearing matrix if
%requested
if fitpar.incl_resolution==0
    smears=0;
    % we can now also crop our data the regular way.
    cropindices=find(q_use==1);
    Itoobs=Iobs(cropindices);
    qtos=qs(cropindices);
    phitos=phis(cropindices);
    clear Iobs qs phis
    Iobs=Itoobs;
    qs=qtos;
    phis=phitos;
    clear Itoobs qtos phitos
else
    %determine fitting relevant data
    %relevant_data_indices_qphi=find(qmin < q & q < qmax && phimin < phi && phi < phimax);;

    [sigma_d1,sigma_d2,sigma_w,sigma_c1,sigma_c2,q_limit,npixels]=resolution_input();
    [smear_shape,q_use,A]=resolution_v3(qs,phis,sigma_d1,sigma_d2,sigma_w,sigma_c1,sigma_c2,q_limit,npixels,q_use);
    smears.sigma_d1=sigma_d1;
    smears.sigma_d2=sigma_d2;
    smears.sigma_w=sigma_w;
    smears.sigma_c1=sigma_c1;
    smears.sigma_c2=sigma_c2;
    smears.q_limit=q_limit;
    smears.npixels=npixels;
    smears.q_use=q_use;
    smears.smear_shape=smear_shape;
    smears.A=A;
end
%ENDMOD
%first create the appropriate handle definition of the Merit-function
%for now it is the unweighted chi-square merit

meritfunc=@(x) chisqr(func,Iobs,qs,phis,smears,fitpar,x) ; %MOD smears added


[Fitparam,Fitval,exitflag,output]=...
    fminsearchbnd(meritfunc,x0,xl,xu,optimset('MaxFunEvals',fitpar.numiter,'MaxIter',fitpar.numiter,'Tolx',fitpar.tol));


fitfun=str2func(func);
Ifit=fitfun(qs,phis,Fitparam);
Info=[];
%if it's no good...
if exitflag==0
    %uiwait(warndlg('maximum number of iterations reached'));
    display(output.message);
end
clearstatus
hstatus=figstatus('Preparing to plot results....please be patient');

if fitpar.numiter==0
    calcfig=plot2D_calcresults(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,qmin,qmax,phimin,phimax,fitpar);
else 
    fitfig=plot2D_fitresults(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,qmin,qmax,phimin,phimax,fitpar);
end
kill_speedhandle



function fitfig=plot2D_calcresults(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,qmin,qmax,phimin,phimax,fitpar)

fitfig=findobj(get(0,'Children'),'Name','Results of 2D Calculation');
if isempty(fitfig)
    fitfig=figure;
    set(gcf,'Name','Results of 2D Calculation')
end

if fitpar.display_output_type==1
    fitplotcontent1(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end
if fitpar.display_output_type==2
    fitplotcontent2(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end
if fitpar.display_output_type==3
    fitplotcontent3(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end
if fitpar.display_output_type==4
    fitplotcontent4(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end

function fitfig=plot2D_fitresults(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,qmin,qmax,phimin,phimax,fitpar)

fitfig=findobj(get(0,'Children'),'Name','Results of 2D fitting');
if isempty(fitfig)
    fitfig=figure;
    set(gcf,'Name','Results of 2D fitting')
end
% outputmenu = uimenu('Label', 'Save...');
% figmenu=uimenu(outputmenu, 'Label', 'As Matlab figure', 'Callback', ...
%     {@SaveFig, fitfig});
% mfilemenu=uimenu(outputmenu, 'Label', 'As M-file', 'Callback', ...
%     {@SaveMfile, fitfig});
% mfilemenu=uimenu(outputmenu, 'Label', 'As Ascii-file', 'Callback', ...
%     {@SaveAscii, fitfig});
if fitpar.display_output_type==1
    fitplotcontent1(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end
if fitpar.display_output_type==2
    fitplotcontent2(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end
if fitpar.display_output_type==3
    fitplotcontent3(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end
if fitpar.display_output_type==4
    fitplotcontent4(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
end


% This function creates the plot window and plots the result AS REQUESTED
% IN THE FITTING WINDOW.
function hfig=fitplotcontent4(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
% put your 4th display output here
errordlg('There is no output defined for this display output')
    
function hfig=fitplotcontent3(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax,fitpar)
%report generation
% for speed we interpolate the data and fit on a smaller grid
% 50x50 rather than the full grid (often 1k x 1k)
%griddata may produce NAN on the edges.. .therefore we start by making
% the matrixes larger by 1 value
qstep=(qmax-qmin)/50;
x=max(0,qmin-qstep):qstep:qmax+qstep;
phistep=(phimax-phimin)/50;
y=max(0,phimin-phistep):phistep:min(360,phimax+phistep);

%griddata has been replaced by gridfit because griddata messes up and
%gridfit has improved functionality
[ZIobs,XI,YI]=gridfit(qs,phis,Iobs,x,y);
ZIfit=gridfit(qs,phis,Ifit,x,y);
ZIres=gridfit(qs,phis,(Iobs-Ifit),x,y);

Zmax=max(max(ZIobs(:)),max(ZIfit(:)));
Zmin=min(min(ZIobs(:)),min(ZIfit(:)));
Zmin=max(Zmax/10000,Zmin);
Zresmin=min(ZIres(:));
Zresmax=max(ZIres(:));

%get some more information:
moreinfo=guidata(gcbo);
if ~ isfield(moreinfo,'saxsdata');
    fithandles=findobj('Name','Fitting2D','-and','Type','figure'); 
%this would go SO wrong if more Fitting2D figures were open. make sure  
% to close them!
    moreinfo=guidata(fithandles(1));
    if ~ isfield(moreinfo,'saxsdata')
        sample_name='-';
    else
        [sample_dirname,sample_name,sample_ext]=fileparts(moreinfo.saxsdata.raw_filename);
    end
else
[sample_dirname,sample_name,sample_ext]=fileparts; 
(moreinfo.saxsdata.raw_filename);
end

figure(fitfig)
set(fitfig,'Units','normalized')
set(fitfig,'Position',[.1 0 .6 .8]);
[discard,parnames]=findparam(fitpar.name);
subplot(3,2,[1 3])
text_misc_label={'sample name:' 'fitfunction name:' 'q range:' '\phi range:' 'merit'};
text_misc_ans{1}=sample_name;
text_misc_ans{2}=fitpar.name;
text_misc_ans{3}=sprintf('%s - %s',num2str(qmin),num2str(qmax));
text_misc_ans{4}=sprintf('%s - %s',num2str(phimin),num2str(phimax));
text_misc_ans{5}=num2str(Fitval);
for paramnum=1:size(Fitparam,2);
    text_param_label{paramnum}=sprintf('%s:',parnames.name{paramnum});
    text_param_ans{paramnum}=num2str(Fitparam(paramnum),'%1.3G');
    text_param_combol1{paramnum}=sprintf('%s &',parnames.name{paramnum});
    text_param_combol2{paramnum}=sprintf('%s &',num2str(Fitparam(paramnum),'%1.3G'));
end
axis off
text(.45,.9,text_misc_label,'VerticalAlignment','Top','HorizontalAlignment','Right');
text(.45,.45,text_param_label,'VerticalAlignment','Top','HorizontalAlignment','Right');
text(.55,.9,text_misc_ans,'VerticalAlignment','Top','HorizontalAlignment','Left');
text(.55,.45,text_param_ans,'VerticalAlignment','Top','HorizontalAlignment','Left');
drawnow

%store the text results:
datevector=datevec(now);
storefilename=sprintf('%s%s%i%s%i%s%i%s%i%s%i%s% s.tex',sample_dirname,...
    filesep,datevector(1),'_',datevector(2),'_',datevector(3),'_',...
    datevector(4),'_',datevector(5),'_',sample_name);
storedatahandle=fopen(storefilename,'wt');
fprintf(storedatahandle,'%s &%s &%s &%s &%s &%s &%s &%s &%s &%s \\\\ \n',...
    text_misc_label{1},text_misc_ans{1},text_misc_label{2},...
    text_misc_ans{2},text_misc_label{3},text_misc_ans{3},...
    text_misc_label{4},text_misc_ans{4},text_misc_label{5},...
    text_misc_ans{5})
for paramnum=1:(size(Fitparam,2)-1);
    fprintf(storedatahandle,'%s',text_param_combol1{paramnum});
end
fprintf(storedatahandle,'%s \\\\ \n',text_param_combol1{paramnum+1});
for paramnum=1:(size(Fitparam,2)-1);
    fprintf(storedatahandle,'%s',text_param_combol2{paramnum});
end
fprintf(storedatahandle,'%s \\\\ \n',text_param_combol2{paramnum+1});

subplot(3,2,5)
contour(XI,YI,ZIres)
set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (linear scale) ')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow

subplot(3,2,2)
mesh(XI,YI,ZIobs)
set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Data')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow

subplot(3,2,4)
mesh(XI,YI,ZIfit)
set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Modelled data')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow

subplot(3,2,6)
mesh(XI,YI,ZIres)
set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (linear scale) ')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow

set(fitfig,'PaperPositionMode','auto');
% [file,path]=uiputfile('*.eps','Save the sheet to what file?');
% if file~=0
figfilename=sprintf('%s%s%i%s%i%s%i%s%i%s%i%s% s.eps',sample_dirname,...
    filesep,datevector(1),'_',datevector(2),'_',datevector(3),'_',...
    datevector(4),'_',datevector(5),'_',sample_name);
    print('-depsc2',sprintf('%s%s','-f',num2str(fitfig)),'-noui','- r150',figfilename);
% end

close(fitfig)

function hfig=fitplotcontent2(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
%a more convenient version of type 1
% for speed we interpolate the data and fit on a smaller grid
% 50x50 rather than the full grid (often 1k x 1k)
%griddata may produce NAN on the edges.. .therefore we start by making
% the matrixes larger by 1 value
qstep=(qmax-qmin)/50;
x=max(0,qmin-qstep):qstep:qmax+qstep;
phistep=(phimax-phimin)/50;
y=max(0,phimin-phistep):phistep:min(360,phimax+phistep);

%griddata has been replaced by gridfit because griddata messes up and
%gridfit has improved functionality
[ZIobs,XI,YI]=gridfit(qs,phis,Iobs,x,y);
ZIfit=gridfit(qs,phis,Ifit,x,y);
ZIres=gridfit(qs,phis,(Iobs-Ifit),x,y);

Zmax=max(max(ZIobs(:)),max(ZIfit(:)));
Zmin=min(min(ZIobs(:)),min(ZIfit(:)));
Zmin=max(Zmax/10000,Zmin);
Zresmin=min(ZIres(:));
Zresmax=max(ZIres(:));

subplot(2,2,1)
contour(XI,YI,ZIres)
set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (linear scale) ')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow

figure(fitfig)
subplot(2,2,2)
mesh(XI,YI,ZIobs)
set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Data')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow

subplot(2,2,4)
mesh(XI,YI,ZIfit)
set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Modelled data')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow


subplot(2,2,3)
%mesh(XI,YI,sign(ZIres).*log10(abs(ZIres)))
mesh(XI,YI,ZIres)
set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (logarithmic scale) ')
xlabel('q, (1/A)')
ylabel('Phi (degrees)')
colorbar
drawnow


function hfig=fitplotcontent1(Fitparam,Fitval,exitflag,output,Ifit,Iobs,qs,phis,fitfig,qmin,qmax,phimin,phimax)
% for speed we interpolate the data and fit on a smaller grid
% 50x50 rather than the full grid (often 1k x 1k)
%griddata may produce NAN on the edges.. .therefore we start by making
% the matrixes larger by 1 value
qstep=(qmax-qmin)/50;
x=max(0,qmin-qstep):qstep:qmax+qstep;
phistep=(phimax-phimin)/50;
y=max(0,phimin-phistep):phistep:min(360,phimax+phistep);

%griddata has been replaced by gridfit because griddata messes up and
%gridfit has improved functionality
[ZIobs,XI,YI]=gridfit(qs,phis,Iobs,x,y);
ZIfit=gridfit(qs,phis,Ifit,x,y);
ZIres=gridfit(qs,phis,(Iobs-Ifit),x,y);

Zmax=max(max(ZIobs(:)),max(ZIfit(:)));
Zmin=min(min(ZIobs(:)),min(ZIfit(:)));
Zmin=max(Zmax/10000,Zmin);
Zresmin=min(ZIres(:));
Zresmax=max(ZIres(:));

figure(fitfig)
subplot(2,2,1)
contour(XI,YI,ZIobs)
%set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Data')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')
colorbar
drawnow

subplot(2,2,3)
contour(XI,YI,ZIfit)
%set(gca,'Zlim',[Zmin Zmax])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Modelled data')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')
colorbar
drawnow

subplot(2,2,2)
contour(XI,YI,ZIres)
%set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (linear scale) ')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')
colorbar
drawnow

subplot(2,2,4)
mesh(XI,YI,ZIres)
set(gca,'Zlim',[Zresmin Zresmin+Zmax-Zmin])
set(gca,'Xlim',[XI(1,1) XI(end,end)])
set(gca,'Ylim',[YI(1,1) YI(end,end)])
title('Residuals (linear scale) ')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Azimuthal Angle (degrees)')
colorbar
drawnow

%and here we define the function that needs to be minimized, basically
%differently weighted residuals
function cs=chisqr(func,I,qs,phis,smears,fitpar,varargin)
global error_norm_fact

warning off all;
func=str2func(func);
warning on all;
fitvalues=varargin{1};
if iscell(fitvalues)
    fitvalues=fitvalues{1};
end
Icalc=func(qs,phis,fitvalues); %must be of similar shape as the original...

%MOD2: the Icalc will be smeared using the input parameters as obtained
%before. ----> SLOOOOOW!
if fitpar.incl_resolution==1
    Icalc=smear_v2(qs,phis, smears.sigma_d1, smears.sigma_d2, smears.sigma_w, smears.sigma_c1, smears.sigma_c2, smears.q_use, smears.smear_shape,Icalc, smears.A);
end

warning off all;
%MOD6: this will prevent too much from going wrong here. remember that
%"smear" actually NaN's all values of non-interest.
indextogo=find(isfinite(Icalc)==1);

if isfield(fitpar,'weighting')==0
    fitparam.weighting='Weighted \Chi.';
end
if strcmp(fitpar.weighting,'Ave. Res.')
    cs=sqrt(sum((Icalc(indextogo)-I(indextogo)).^2)/(length(indextogo)-length(fitvalues)));
elseif strcmp(fitpar.weighting,'Rel. Ave. Res.');
    cs=sum(abs(Icalc(indextogo)-I(indextogo))./max(Icalc(indextogo),1/(fitpar.binning.^2)))/(length(indextogo)-length(fitvalues));
elseif strcmp(fitpar.weighting,'Weighted \Chi' );
    cs=fitpar.binning*sum(abs(Icalc(indextogo)-I(indextogo))./(sqrt(max(Icalc(indextogo),1/fitpar.binning.^2))*error_norm_fact))...
        /(length(indextogo)-length(fitvalues));
end
countstatus(fitvalues,cs)
warning on all;

function countstatus(fitvalues,cs)
persistent ii oldtic
%this is a hack that will keep check on when the last calculation
% was performed. If it is roughly more than 5 seconds ago we will assume ti
% was a previous optimization


newtic=tic;
if double(newtic)-double(oldtic)< 18000000
    ii=ii+1;
else
    ii=1;
end
oldtic=newtic;

speedhandle=findobj(get(0,'Children'),'Name','Fitting Counter');
if isempty(speedhandle)
   speedhandle=figure();%speed! speeeeed! ... if figstatus works in this window, matlab will not redraw all of the graphs. thus, much speed is gained.
   set(speedhandle,'Name','Fitting Counter');
else
   clearstatus_speedhandle
end
hstatus=figstatus(['Function calculation #',num2str(ii),':  ',...
    num2str(fitvalues,4),'  ','Merit:',num2str(cs,3)],speedhandle);
drawnow


function clearstatus
fig=findobj(get(0,'Children'),'Name','Fitting2D');
%delete any old status message.
hstatusold=findobj(fig,'Tag','statusline');
if ~isempty(hstatusold)
    delete(hstatusold)
end

function clearstatus_speedhandle
fig=findobj(get(0,'Children'),'Name','Fitting Counter');
%delete any old status message.
hstatusold=findobj(fig,'Tag','statusline');
if ~isempty(hstatusold)
    delete(hstatusold)
end

function kill_speedhandle
speedhandle=findobj(get(0,'Children'),'Name','Fitting Counter');
if ~isempty(speedhandle)
    delete(speedhandle)
end


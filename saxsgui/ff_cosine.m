function y=ff_cosine(x,I,varargin)
% Description
% This function calculates 1D data for a Cosine function
% Description end
% Number of Parameters: 2
% parameter 1: Amp : Amplitude
% parameter 2: Den : 1/2pi of the Period
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Cosing
% A fitting function that can determine the shape of a flatfield for exampl

values=varargin{1};
amp=values(1);
den=values(2);
y=amp*cos(x/den);
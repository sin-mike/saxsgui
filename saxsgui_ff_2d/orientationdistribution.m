function Icalc=orientationdistribution(q,phi,varargin)
% Description
% This function calculates 2D data for a model system
% of holes, descripbed Wang,Cohen and Ruland 1993 and 
% elaborated by B Pauw, 2006
% Description end
% Number of Parameters: 5
% parameter 1: mu : Description of mu
% parameter 2: Bphi : Description Bphi
% parameter 3: L3 : Description L3
% parameter 4: k : Description of k
% parameter 5: lp : Description of lp
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
% 
% Orientationdistribution
% This function takes that evaluates function based on the input values as
% parameters. The sequence of values is very important, and should match
% that of the sequence of identifiers described by the variables cell array.
% I do not know yet how to convert the names in variables to proper
% variables for use in the function.
% this orientation distribution is described by Wang, Cohen and Ruland 1993, Tillinger
% and Ruland, and elaborated by Brian R. Pauw, 2006.
%function written by Brian R. Pauw, 12th of October, 2006.
values=varargin{1};
mu=values(1);
Bphi=values(2);
L3=values(3);
k=values(4);
lp=values(5);


Bobs=sqrt(4*pi^2./(q.^2.*L3^2)+Bphi^2); %this is given by Ruland et al.
%/Iphi=1./(Bobs.*sqrt(2*pi)).*exp(-(phi-mu).^2./(2.*Bobs.^2)); %and this is the gaussian distribution function
Iphi=exp(-(phi-mu).^2./(2.*Bobs.^2)); %and this is the gaussian distribution function
%this is the porod fitting function, that describes the decay at higher
%angles
Icalc=Iphi.*(k./(2.*pi.*(q./(2.*pi)).^4.*lp));

%
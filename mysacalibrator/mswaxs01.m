% mswaxs01 calculates waxs calibration R=R(q)
% parameters can be fixed using globals mask and parx
% parx contains fixed parameters or zeroes
% mask contains zeros on fixed and ones on free positions
% qe ... q values;
% il ... Rs (intensities) to fit; it ... Rs (intensities) calculated
% par(1)=aaa, par(2)=lam 
function delta=mswaxs01(par)
%%global alam imx qe il it ql yl ee 
global qe il it %% ee 
global mask parx
par=par.*mask+parx;
aaa=par(1); lam=par(2);
it=aaa*tan(2*asin(qe*lam/4/pi));
delta= sum(abs(it-il));
%err=delta'*delta;

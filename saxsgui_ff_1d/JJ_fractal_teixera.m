function Icalc=JJ_fractal_teixera(q,I,varargin)
% Description
% This function calculates 1D SAXS data for a mass fractal consisting of 
% spheres 
% Description end
% Number of Parameters: 5
% parameter 1: Amp : Amplitude of function
% parameter 2: R : Radius of the disk
% parameter 3: D : Fractal Dimension
% parameter 4: eta : Cutoff length for fractal correlation
% parameter 5: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of 
% infinitely disks rods
% 
% Taken from J.S.Pedersen, ch 16. case 17, eq. 37
%

values=varargin{1};
Amp=values(1);
R=values(2);
D=values(3);
eta=values(4);
Backg=values(5);

A=1./(q*R).^D;
B=D*gamma(D-1)./(1+1./(q.^2*eta^2).^((D-1)/2));
C=sin((D-1)*atan(q*eta));
Icalc=Amp*A.*B.*C.*abs(F1(q,R).^2)+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);




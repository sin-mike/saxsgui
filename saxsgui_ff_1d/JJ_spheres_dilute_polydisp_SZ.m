function Icalc=JJ_spheres_dilute_polydisp_SZ(q,I,varargin)
% Description
% This function calculates 1D SAXS data for a Schulz-Zimmerman
% size distribution of spheres
% Description end
% Number of Parameters: 4
% parameter 1: Amp : Amplitude of function 
% parameter 2: Ravg : Average Hard-sphere Radius 
% parameter 3: Rsigma : Standard Deviation of Radius Value 
% parameter 4: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute polydisperse dispersion of hard
% spheres according to the Schulz-Zimmerman distributiom
% 
% Eventhough analytical expressions are possible, we perform the calculation 
% numerically 
%Taken from J.S.Pedersen, ch 16. case 11, eq. 9 and 21

values=varargin{1};
Amp=values(1);
Ravg=values(2);
Rsigma=values(3);
Backg=values(4);

numRval=101; %number of values of R to use
Rsigmarange=5; % the R-range will be evaluated from +- Rsigmarange standard deviations

rarray=max(0.1,Ravg-Rsigmarange*Rsigma):(2*Rsigmarange*Rsigma)/(numRval-1):Ravg+Rsigmarange*Rsigma;
normarray=D(rarray,Ravg,Rsigma);

imax=max(size(rarray));
ii=1;
Fsqsum=q*0;
wsum=0;
while ii<=imax,
    Fsqsum=abs(F1(q,rarray(ii)).^2)*normarray(ii)*(4*pi/3*rarray(ii)^3)^2+Fsqsum;
    wsum=wsum+normarray(ii);
    ii=ii+1;
end
Fsqavg=Fsqsum/wsum;

Icalc=Amp*Fsqavg+Backg;



function F=F1(q,R)
F=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);

function SZout=D(r,Ravg,Rsigma)
z=Ravg^2/Rsigma^2-1;
SZout=((z+1)/Ravg)^(z+1)*r.^2/gamma(z+1).*exp(-(z+1).*r./Ravg);

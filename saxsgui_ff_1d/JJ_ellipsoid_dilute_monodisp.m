function Icalc=JJ_ellipsoid_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for dilute monodisperse ellipsoids
% �with axes a,b,c 
% Description end
% Number of Parameters: 5
% parameter 1: Amp : Amplitude of function 
% parameter 2: a : a-Axis semi-length 
% parameter 3: b : b-Axis semi-length
% parameter 4: c : c-Axis semi-length
% parameter 5: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of generalized 
% ellipsoids (tri-axial)
% - The averaging over orientations is performed numerically (2 integrals)
% Taken from J.S.Pedersen, ch 16. case 5, eq. 28
%

values=varargin{1};
Amp=values(1);
a=values(2);
b=values(3);
c=values(4);
Backg=values(5);

alphastep=0.01;
betastep=0.01;
alpha=[0:alphastep:pi/2];
beta=[0:betastep:pi/2];
sumoveralpha=0;

for ii=1:length(alpha)
    sumoverbeta=0;
    for jj=1:length(beta)
        F=F1(q,Rel(a,b,c,alpha(ii),beta(jj)));
        sumoverbeta=sumoverbeta+sin(alpha(ii))*abs(F.^2);
    end
    sumoveralpha=sumoveralpha+2/pi*sumoverbeta*betastep*alphastep;
end

Icalc=Amp*sumoveralpha+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);

function Relout=Rel(a,b,c,alpha,beta)
Relout=sqrt((a^2*sin(beta)^2+b^2*cos(beta)^2)*sin(alpha)^2+c^2*cos(alpha)^2);

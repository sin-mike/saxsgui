function Icalc=JJ_spherical_3shell_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for monodisperse hard spherical 3-shell
% Description end
% Number of Parameters: 8
% parameter 1: Amp : Amplitude of function 
% parameter 2: R1 : Outer Radius of Outer Shell (1)
% parameter 3: R2 : Outer Radius of Shell 2
% parameter 4: R2 : Outer Radius of Shell 3
% parameter 5: rho1 : Relative Scattering Length of Shell 1
% parameter 6: rho2 : Relative Scattering Length of Shell 2 
% parameter 7: rho3 : Relative Scattering Length of Shell 3 
% parameter 8: Backg: Constant Background of data
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of hard
% triple shells with different relative scttering Lengths. 
% The 2 shells are immediately next to each other
% Actually this system has 4 regions-
% Region 0: Outside the particle with scattering length 0
% Region 1: Outer shell of the particle with scattering length rho1
% Region 2: Central of the particle (shell2) with scattering length rho2
% Region 3: Inner part of the particle (shell3) with scattering length rho3
% Taken from J.S.Pedersen, ch 16. case 3, eq. 23-24
%  

values=varargin{1};
Amp=values(1);
R1=values(2);
R2=values(3);
R3=values(4);
rho1=values(5);
rho2=values(6);
rho3=values(7);
Backg=values(8);

M3=rho1*V(R1)+(rho2-rho1)*V(R2)+(rho3-rho2)*V(R3);
F3=(rho1*V(R1).*F1(q,R1)+(rho2-rho1)*V(R2).*F1(q,R2)+(rho3-rho2)*V(R3).*F1(q,R3))/M3;
Icalc=Amp*abs(F3.^2)+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);

function Vout=V(R)
Vout=4*pi*R^3/3;

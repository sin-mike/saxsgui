function Icalc=JJ_spheres_hard_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for monodisperse hard spheres
% including structure factor
% Description end
% Number of Parameters: 4
% parameter 1: Amp : Amplitude of function 
% parameter 2: Rhs : Hard-sphere Radius 
% parameter 3: eta : Hard-sphere volume fraction 
% parameter 4: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a denser monodisperse dispersion of hard
% spheres, Including both structure factor and form factor 
% 
% Taken from J.S.Pedersen, ch 16. case 3.2-1, eq. 78-80
% 

values=varargin{1};
Amp=values(1);
Rhs=values(2);
eta=values(3);
Backg=values(4);

F=F1(q,Rhs);
S=S1(q,Rhs,eta);
Icalc=Amp*abs(F.^2).*S+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);

function Sout=S1(q,Rhs,eta)
alpha=(1+2*eta)^2/(1-eta)^4;
beta=-6*eta*(1+eta/2)^2/(1-eta)^4;
gam=eta*alpha/2;
A=2*Rhs*q;
G=alpha*(sin(A)-A.*cos(A))./A.^2+...
    beta*(2*A.*sin(A)+(2-A.^2).*cos(A)-2)./A.^3+...
    gam*(-A.^4.*cos(A)+4*((3*A.^2-6).*cos(A)+(A.^3-6*A).*sin(A)+6))./A.^5;
Sout=(1+24*eta*G./(2*Rhs*q)).^(-1);


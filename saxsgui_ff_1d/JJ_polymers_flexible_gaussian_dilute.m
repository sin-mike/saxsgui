function Icalc=JJ_polymers_flexible_gaussian_dilute(q,I,varargin)
% Description
% This function calculates 1D SAXS data for flexible polymers chains which
% are not selfavoiding and obey gaussian statistics
% Description end
% Number of Parameters: 3
% parameter 1: Amp : Amplitude of function
% parameter 2: Rg : Ensemble average of Radius of Gyration
% parameter 3: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion flexible 
% polymers chains which are not selfavoiding and obey gaussian statistics
% 
% Taken from J.S.Pedersen, ch 16. case 18, eq. 38
%

values=varargin{1};
Amp=values(1);
Rg=values(2);
Backg=values(3);

u=Rg^2*q.^2;

Icalc=Amp*2*(exp(-u)+u-1)./u.^2+Backg;




function Icalc=JJ_spheres_stickyhard_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for monodisperse hard spheres
% including structure factor corresponding to sticky hardspheres
% Description end
% Number of Parameters: 5
% parameter 1: Amp : Amplitude of function 
% parameter 2: Rhs : Hard-sphere Radius 
% parameter 3: eta : Hard-sphere volume fraction 
% parameter 4: tau : Stickyness parameter
% parameter 5: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a denser monodisperse dispersion of sticky hard
% spheres, Including both structure factor and form factor 
% 
% Taken from J.S.Pedersen, ch 16. case 3.2-2, eq. 81-82
% 

values=varargin{1};
Amp=values(1);
Rhs=values(2);
eta=values(3);
tau=values(4);
Backg=values(5);

F=F1(q,Rhs);
S=S1(q,Rhs,eta,tau);
Icalc=Amp*abs(F.^2).*S+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);

function Sout=S1(q,Rhs,eta,tau)
temp1=tau/eta+1/(1-eta);
lambda=min(6*temp1+sqrt(36*temp1),6*temp1-sqrt(36*temp1));
mu=lambda*eta*(1-eta);
alpha=(1+2*eta-mu);
beta=(-3*eta+mu)/(2*(1-eta)^2);
kappa=2*Rhs*q;
A=1+12*eta*(alpha.*(sin(kappa)-kappa.*cos(kappa))./kappa.^3+...
    beta*(1-cos(kappa))./kappa.^2-...
    lambda/12*sin(kappa)./kappa);
B=12*eta*(alpha.*(kappa.^2/2-kappa.*sin(kappa)+1-cos(kappa))./kappa.^3+...
    beta*(kappa-sin(kappa))./kappa.^2-...
    lambda/12*(1-cos(kappa))./kappa);
Sout=1./(A.^2+B.^2);


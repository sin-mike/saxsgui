function Icalc=JJ_inf_thin_disk_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for dilute monodisperse dispersion
% of infinitely thin disks
% Description end
% Number of Parameters: 3
% parameter 1: Amp : Amplitude of function
% parameter 2: R : Radius of the disk
% parameter 3: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of 
% infinitely disks rods
% 
% Taken from J.S.Pedersen, ch 16. case 16, eq. 36
%

values=varargin{1};
Amp=values(1);
R=values(2);
Backg=values(3);

Icalc=Amp*(2./(q*R).^2).*(1-besselj(1,(2*q*R))./(q*R))+Backg;





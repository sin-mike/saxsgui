 function [dmaxcen,alphacen]=guess_fine_bift_params(dmax,alphacen,ntot0,searchparam,x,y,sd);
 
 
 % This routine tries to find the best values for dmax and alpha based on
 % variations on a simplex routine as executed in matlabs fminssearch.
 % Since the alpha parameter is somewhat insensitive we optimize the
 % logarithm of alpha.
 
 [p1,evi1]=fminsearch(@bift_simplex,[dmax,log(alphacen)],...
    optimset('MaxIter',100),ntot0,searchparam,x,y,sd) ;
% %in some cases we find a lower maximum while the data indicates there may
% [p2,evi2]=fminsearch(@bift_simplex,[2*p1(1),p1(2)],...
%     optimset('MaxIter',40),ntot0,searchparam,x,y,sd) ;
% %choosing most promising area
% if evi2<evi1
%     p=p2;
% else
%     p=p1;
% end
p=p1;
dmaxcen=p(1);
alphacen=exp(p(2));
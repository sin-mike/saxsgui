
%**************************************************************************
%*     Definition of the prior
%************************************************************************
function [M,xf]=bift_prior(ntot,y,dfx)
xf=dfx*[1:ntot];
diam=dfx*(ntot);
[M]=bift_distphere(ntot,dfx,y(1),diam);


%********************************************************************* 
%*     Distance distribution for Sphere
%*********************************************************************
function [M]=bift_distphere(ntot,dr,rx,d)
% sum1=0;
% sum2=0;
% sum3=0;
pmin=0.002; 
av=rx/ntot;
psum=d.^3/24.;          
fact=rx/psum*dr;
dmax=(ntot)*dr;
R=dr*(1:ntot);
M=R.^2.*(1-1.5*(R/d)+.5*(R/d).^3).*fact;
M=M.*(R<=d);
sum1=sum(M);

avm=pmin*max(M);
M=M.*(M>avm)+avm.*(M<=avm);
sum2=sum(M);

M=M*sum1/sum2;

%  
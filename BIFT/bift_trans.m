
% C*********************************************************************
% C     SETTING UP OF TRANSFORMATION MATRIX A    ( A=Smear*FT )
% C     (Calculation of smearing and/or Fourier transformation)
% C*********************************************************************
%function [A,ft,smear]=trans(x,xf,ntot,mtot,dfx,nmax)
function A=bift_trans(x,xf)

%only implemented with no smearing so far.
%
%expressed in matlab language
warning off MATLAB:divideByZero
A=sin(x'*xf)./(x'*xf);
warning on MATLAB:divideByZero

%for xf=0 we actually get 0/0 which returns NAN when it should be 1.
% we therefore assume that NaN are always created this way
I=find(isnan(A));
A(I)=1;

%later on we may introduce a smearing such as the one shown below adapted
%by S. Hansen from J.S. Pedersen



%                             SUBROUTINE TRANS(A,FT,SMEAR,x,xf,NTOT,MTOT,DFX,NMAX)
%                             REAL A(NMAX,NMAX),SMEAR(NMAX,NMAX),FT(NMAX,NMAX)
%                             REAL r(NMAX),q(NMAX),x(nmax),xf(nmax),xj(nmax)
%                             common r11,r12,r21,r22,rl11,rl12,rl21,rl22
%                             common rlam1,rlam2,dlam1,dlam2,ntot1,ntot2
%                             
%                             c***** No smearing:
%                             if(r11.eq.0) then
%                                 DO 4 I=1,mtot
%                                 DO 3 J=1,NTOT
%                                 A(I,J)=SIN(X(I)*XF(J))/(X(I)*XF(J))
%                                 3  CONTINUE
%                                 4  CONTINUE
%                                 goto 999
%                             end
%                             
%                             C***** Smearing:***************************************************
%                             c     For each data set an extra - intermediate - array xj is made 
%                             c     from the array x by adding points at the start of x (low-q) - 
%                             c     this allows for correct calculation of smearing matrix.
%                             c            p(r) ->  Ij(q) ->  I(q)
%                             c     Index   K         J        I
%                             c     No     NTOT    NTOT1+N    MTOT
%                             c
%                             c                    NTOT2+N <-new N
%                             c                     etc.  for more datasets
%                             c******************************************************************
%                             
%                             c********  data set 1
%                             
%                             dq=x(2)-x(1)
%                             n=x(1)/dq
%                             do 10 j=1,n
%                             xj(j)=x(1)-(n-(j-1))*dq
%                             10 continue
%                             if(xj(1).le.1.e-6) xj(1)=1.e-6
%                                 do 11 j=1+n,ntot1+n
%                                 jj=j-n
%                                 xj(j)=x(jj)
%                                 11 continue
%                                 
%                                 DO 2 I=1,ntot1
%                                 sm=0                         
%                                 DO 1 J=1,ntot1+n
%                                 SMEAR(I,J)=SMEARx(R11,R12,RL11,RL12,RLAM1,DLAM1,x(i),xJ(J))*dq
%                                 sm=sm+smear(i,j)
%                                 1 CONTINUE
%                                 do 100 j=1,ntot1+n
%                                 smear(i,j)=smear(i,j)/sm
%                                 100 continue
%                                 2 CONTINUE
%                                 
%                                 DO 40 J=1,ntot1+n
%                                 DO 30 K=1,NTOT
%                                 FT(J,K)=SIN(XJ(J)*XF(K))/(XJ(J)*XF(K))
%                                 30 CONTINUE
%                                 40 CONTINUE
%                                 
%                                 DO 70 I=1,ntot1
%                                 DO 60 K=1,ntot                    
%                                 A(I,K)=0
%                                 DO 50 J=1,ntot1+n
%                                 A(I,K)=SMEAR(I,J)*FT(J,K)+A(I,K)
%                                 50 CONTINUE
%                                 60 CONTINUE
%                                 70 CONTINUE
%                                 if(ntot2.eq.0) goto 999
%                                     
%                                     c********   data set 2
%                                     
%                                     dq=x(mtot)-x(mtot-1)
%                                     n=x(ntot1+1)/dq
%                                     do 105 j=1,n
%                                     xj(j)=x(ntot1+1)-(n-(j-1))*dq
%                                     105 continue
%                                     if(xj(1).le.1.e-6) xj(1)=1.e-6
%                                         do 111 j=1+n,ntot2+n
%                                         jj=j-n+ntot1
%                                         xj(j)=x(jj)
%                                         111 continue
%                                         
%                                         DO 201 I=ntot1+1,mtot 
%                                         sm=0                        
%                                         DO 101 J=1,ntot2+n
%                                         SMEAR(I,J)=SMEARx(R21,R22,RL21,RL22,RLAM2,DLAM2,x(i),xj(J))*dq
%                                         sm=sm+smear(i,j)
%                                         101 CONTINUE
%                                         do 1011 j=1,ntot2+n
%                                         smear(i,j)=smear(i,j)/sm
%                                         1011 continue
%                                         201 CONTINUE
%                                         
%                                         DO 401 J=1,ntot2+n
%                                         DO 301 K=1,NTOT
%                                         FT(J,K)=SIN(XJ(J)*XF(K))/(XJ(J)*XF(K))
%                                         301 CONTINUE
%                                         401 CONTINUE
%                                         
%                                         DO 701 I=ntot1+1,mtot
%                                         DO 601 K=1,ntot                    
%                                         A(I,K)=0
%                                         DO 501 J=1,ntot2+n
%                                         A(I,K)=SMEAR(I,J)*FT(J,K)+A(I,K)
%                                         501 CONTINUE
%                                         601 CONTINUE
%                                         701 CONTINUE
%                                         
%                                         999 return
%                                     end
%                                     
%                                     c------------------------------------------------------------------
%                                     
%                                     *
%                                     FUNCTION SMEARx(R1,R2,RL1,RL2,RLAM,DLAM,QCEN,Q)
%                                     *       Returns R(<q>,q) for QCEN = <q>
%                                         
%                                         * r1,r2,rl1,rlw in cm.
%                                         *
%                                         PI=3.1415927
%                                         WK0=2.*PI/RLAM
%                                         SIGDET=1.3*WK0/RL2*0.4246609
%                                         *     SIGRAV=0.00065*0.4246609
%                                         *     SIGRAV=0.00137
%                                         SIGRAV=0.001
%                                         THET0=ASIN(QCEN/2./WK0)
%                                         AA1=R1/(RL1+RL2/COS(2.*THET0)**2)
%                                         A2=R2*COS(2.*THET0)**2./RL2
%                                         IF(A2. GT. AA1) GOTO 125
%                                         BET1=2.*R1/RL1-0.5*R2*R2*(COS(2.*THET0))**4.*(RL1
%                                         -+RL2/COS(2.*THET0)**2)**2./RL1/RL2/RL2/R1
%                                         GOTO 127
%                                         125  BET1=2.*R2*(1./RL1+(COS(2.*THET0)**2)/RL2)
%                                         --0.5*R1*R1*RL2/(R2*COS(2.*THET0)**2.*RL1
%                                         -*(RL1+RL2/COS(2.*THET0)**2))
%                                         127  DELQ1=SQRT((WK0*COS(THET0)*BET1)**2.+(QCEN*DLAM)**2.)
%                                         SIGQ=(0.4246609*DELQ1)**2.+SIGDET**2.+SIGRAV**2.
%                                         SMEARx=FRES(SIGQ,QCEN,Q)
%                                         RETURN
%                                         END
%                                         *......................................................................
%                                             FUNCTION FRES(SIGQ,QCEN,Q)
%                                         X=QCEN*Q/SIGQ
%                                         T=X/3.75
%                                         IF(ABS(X).GT. 3.75) GOTO 100
%                                         BESS=1.+3.5156229*T*T+3.0899424*T**4.+1.2067492*T**6.
%                                         -+0.2659732*T**8.+0.0360768*T**10.+0.0045813*T**12.
%                                         FRESX=Q/SIGQ*EXP(-0.5*(QCEN*QCEN+Q*Q)/SIGQ)*BESS
%                                         GOTO 200
%                                         100 BESSX=(0.39894228+0.01328592/T+0.00225319/T**2.
%                                         --0.00157565/T**3.+0.00916281/T**4.-0.02057706/T**5.
%                                         -+0.02635537/T**6.-0.01647633/T**7.+0.00392377/T**8.)
%                                         FRESX=Q/SIGQ*EXP(-0.5*(QCEN*QCEN+Q*Q)/SIGQ+X)*BESSX/SQRT(X)
%                                         200 FRES=FRESX
%                                         RETURN
%                                         END
%                                         c------------------------------------------------------------------------
%                                         
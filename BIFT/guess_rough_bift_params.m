 function [alphacen,dmax]= guess_rough_bift_params(rg,alphaband,ntot0,searchparam,x,y,sd)

 %now evaluate and find best alpha for given rg 
dmax=3*rg;
for ii=1:alphaband*2+1
    alpha=10^(ii-(alphaband+1));
    eval(ii)=-bift_simplex([dmax,log(alpha)],ntot0,searchparam,x,y,sd) ;
    if ~isreal(eval(ii))
        eval(ii)=-inf;
    end
    xeval(ii)=alpha;
end

alphacen=xeval(find((eval-max(eval))==0.0)); %finding alpha value giving best evidence

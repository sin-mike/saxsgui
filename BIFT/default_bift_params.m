nmax=500; %maximum number of 
ndist=1000;
ncalcmax=1000; %maximum number of alpha-dmax combinations allowed
alphastart=100; %this is the center of the first rough optimization 
alphaband=12; %range of initial values is alphastartE-alphaband to alphastartE+Alphaband
alpharange=30; %range of refined alpha values suggested is alpharange below and above best values
dmaxrange=1.5; % relative range of refined dmax suggested (dmaxmin=dmaxopt/dmaxrange, dmaxmax=dmaxopt*dmaxrange*dmaxrange)
ndiam=21; %default number of dmax
nalpha=11; %default number of alphas calculated

%for each alpha-dmax combination, the program will try to find an
%optimum solution (using a simplex type algorithm). This algorithm needs
%parameters values to make it run reasonably.
searchparam.maxit=1000; %default maximum number of iterations per optimization
searchparam.minit=100;  %default minimum number of iterations per optimization
searchparam.omegaorig=.5; % default maximum step of solution per optimizationstep..
% for some values of alpha this parameter will be
% too big.
searchparam.omegamin=0.001; % this omega is the minimum omega will ever be allowed...if lower simply return
%infinite value of evidence
searchparam.omegareduction=2.; %if omega-orig parameter too big reduce it by this amount
searchparam.dotsptol=0.001; %the optimization stops when dotsp=1-dotsptol 

searchparam.errorcorr=1; % the errorbars may be changed by the program 1=yes  0=No
searchparam.targeterr=0.8; %the standard deviation of (y-yfit)/ysigma should be 1....however
                           % here is the opportunity to make it otherwise
                           % (works best with 0.9)
searchparam.errorcorrthresh=3; %if the standard deviation is above this value then we will not use it 
                                % since it indicate a bad preliminary fit

                           
%Normally this optimization will run without reporting. However for some
%purposes you may want to follow along in the optimization.
verboseparam.on=0;  % should be 1 if you want to have result plotted during the optimization
% (Note this will slow down the calculation tremendously
verboseparam.viewstep=50; %if plotting iterations, how many iterations between each plot
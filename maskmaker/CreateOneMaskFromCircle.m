function binimgout=CreateOneMaskFromCircle(imgin,linepoints)
% this routine creates an 0/1-image the size of imgin with pointvalues
% equal to 1 for pixels that have a position on the inside of the
% circle given by vertices in linepoints (actually the first one)
%
% The technique is very basic and essentially consist of
% 1) locating the center of the figure 
% 2) locating the radius of the figure
% 3) creating an x y image using meshgrid
% 4) calculating a matrix with values from logical (radius<radius of the
% circle)
%
% so here goes


%First we want to pull out the information on the center and the radius of
%the circle
cen=linepoints(1,:);
radius=linepoints(2,:);
%radius=sqrt(sum((linepoints(:,2)-linepoints(:,1)).^2));

% at this we generate some necessary imagedata

[ymax,xmax]=size(imgin);
[x,y]=meshgrid(1:xmax,1:ymax);

%now we associate with each pixel a distance to the center
R=sqrt((x-cen(1)).^2+(y-cen(2)).^2);
binimgout=(R<=radius(1));

    